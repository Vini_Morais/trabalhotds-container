��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �  	   �  1   �       	        '  
   3     >  
   V     a     w     �  	   �     �  
   �     �     �     �  
   �  	   �     �                 
         '     H     P  *   f      �  $   �     �     �     	            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-18 19:30+0000
Last-Translator: Ernestas Karalius <Unknown>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Apie Veiksmai Leisti adresus Programos Sritis, kurioje atsiras XApp būsenos piktogramos Naršyti Atšaukti Kategorijos Kategorija Pasirinkite piktogramą Numatytoji Numatytoji piktograma Įrenginiai Emblemos Jaustukai Mėgstamiausi Piktograma Piktogramų dydis Paveikslėlis Įkeliama... Mime tipai Atidaryti Operacija nepalaikoma Kita Vietos Ieškoti Pasirinkti Pasirinkite paveikslėlio failą Būsena Numatytoji kategorija Piktograma, kurią naudoti pagal numatymą Pageidaujamas piktogramų dydis. Eilutė, atvaizduojanti piktogramą. Ar leisti adresus XApp būsenos įskiepis XApp būsenos įskiepio gamykla 