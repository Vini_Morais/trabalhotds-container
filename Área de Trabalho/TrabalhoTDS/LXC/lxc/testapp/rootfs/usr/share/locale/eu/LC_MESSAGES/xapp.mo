��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     
   �     �     �     �  (   �            
   "  	   -     7  
   J     U     f     n     v     ~     �     �     �     �  
   �     �     �     �     �     �     �                   %   5     [     u  !   �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-28 12:46+0000
Last-Translator: Asier Iturralde Sarasola <Unknown>
Language-Team: Basque <eu@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Honi buruz Ekintzak Onartu bideak Aplikazioak XApp egoera ikonoak agertuko diren tokia Arakatu Utzi Kategoriak Kategoria Aukeratu ikono bat Lehenetsia Ikono lehenetsia Gailuak Ikurrak Emojiak Gogokoak Ikonoa Ikonoaren tamaina Irudia Kargatzen... Mime motak Ireki Eragiketa ez da onartzen Bestelakoak Lekuak Bilatu Hautatu Hautatu irudi-fitxategia Egoera Kategoria lehenetsia Modu lehenetsian erabiliko den ikonoa Hobetsitako ikono tamaina Ikonoa adierazten duen katea. Bideak onartu behar diren ala ez. XApp Egoera applet-a XApp Egoera applet faktoria 