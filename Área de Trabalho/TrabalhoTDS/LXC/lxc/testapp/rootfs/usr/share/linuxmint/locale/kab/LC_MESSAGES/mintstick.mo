��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  &   ,  +   S          �     �  J  �     
          #     >  !   W  
   y     �  
   �  $   �     �  D   �     (     ;     H     ^     l     {                     
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-08-10 12:42+0000
Last-Translator: Yacine Bouklif <yacinebouklif@gmail.com>
Language-Team: Kabyle <kab@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Teḍra-d tuccḍa deg unɣal n tugna. Teḍra tuccḍa di tmerna n uḥric di %s. Teḍra-d tuccḍa. Tuccḍa n usesteb Fren isem i tsarutt inek FAT32
   + Yemṣada di yal amkan.
   - Ur izmir ara ad isefrek ifuyla iɛeddan Go.

exFAT
   + Nezmer ad d-nini yemṣada di yal amkan.
   + Yezmer ad isefrek ifuyla iɛeddan 4 Go.
   - Ur yemṣada ara ṭas am FAT32.

NTFS
   + Yemṣada akked Windows.
   - Ur yemṣada ara akked Mac neɣ akked tegti n yibenkan.
   - Tikwal iseɛɛu ugur n umṣada akked Linux (NTFS mačči d ilelli yerna d tijenyert tamedeffirt).

EXT4
   + D atrar, yerked, d arurad, yesɛa aɣmis.
   + Iteddu akked isirigen n ifuyla n Linux.
   - Ur imṣda ara akked Windows, Mac neɣ akked tuget n ibenkan.
 Amasal Msel tasarutt USB Eg tasarutt USB n  usenker Eg tasarutt USB n tnekra Ulac deqs n umkan di tsarutt USB. Fren Tugna Fren tasarutt USB: Fren tugna Tasarutt USB tettwamsel akken iwata. Tugna tettwaru akken iwata. Ayagi ad yekkes akk isefka ɣef tsarutt USB, tebɣiḍ ad tkemleḍ? Amyaru n tugna USB Tasarutt USB Amessal n tsarutt USB Tasarutt USB: Isem n ubleɣ: Aru arussin 