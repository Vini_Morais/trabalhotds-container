��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  X   �  M   �     -      M  *   n     �     �  !   �  (   �  :     ;   J  t   �  %   �    !      7     X  3   s     �     �  $   �  %   �  
     '        A     Q     b  e   g  4   �       /        7     U  >   Z  B   �     �  5   �  V   %     |     �     �  
   �     �  2   �          5     L     Z  2   _  r   �  1     E   7  ,   }  k   �  7     h   N  0   �  E   �  v   .  Y   �  ]   �  =   ]   -   �   |   �      F!  7   W!  !   �!  >   �!     �!  %   
"  '   0"     X"  N   k"     �"     �"  N   �"     &#     7#     @#     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-28 11:30+0000
Last-Translator: Dmitriy Kulikov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 В процессе копирования образа произошла ошибка. При создании раздела на %s возникла ошибка. Произошла ошибка Произошла ошибка. Ошибка аутентификации. Вычисление... Проверить Контрольная сумма Файл контрольных сумм Несовпадение контрольной суммы Выберите метку для USB-накопителя Загрузите ISO-образ повторно. Его контрольная сумма не корректна Все выглядит хорошо! FAT32 
  + Совместима со всеми ОС и аппаратными устройствами.
  -  Нельзя хранить файлы более 4Гб.

exFAT
  + Совместима почти со всеми ОС и аппаратными устройствами.
  + Возможно хранить файлы более 4Гб.
  -  Не такая широко поддерживаемая, как FAT32.

NTFS 
  + Совместима с Windows.
  -  Не совместима с Mac и большинством аппаратных устройств.
  -  Незначительные проблемы совместимости с Linux (NTFS является проприетарной файловой системой).

EXT4 
  + Современная, стабильная, быстрая, журнализируемая.
  + Поддерживает файловые разрешения Linux.
  -  Не совместима с Windows, Mac и большинством аппаратных устройств.
 Файловая система: Форматировать Форматировать USB-накопитель ГБ GPG сигнатуры Файл, подписанный GPG Файл, подписанный GPG: Назад Верификация ISO-образа ISO-образ: ISO-образы ISO: Если вы доверяете подписи, вы можете доверять ISO-образу. Ошибка проверки целостности КБ Ключ не найден на сервере. Локальные файлы МБ Создание загрузочного USB-носителя Создание загрузочного USB-накопителя Подробнее Идентификатор тома не найден Недостаточно свободного места на USB-накопителе Сумма SHA256 Файл сумм SHA256 Файл сумм SHA256: SHA256sum: Выбрать образ Выберите USB-флеш-накопитель Выберите образ Подписано: %s Размер: ТБ Сумма SHA256 ISO-образа неверна. Файл сумм SHA256 не содержит контрольных сумм для этого ISO-образа. Файл суммы SHA256 не подписан. USB-накопитель успешно отформатирован. Контрольная сумма верна Контрольная сумма верна, но ее подлинность не проверялась. Файл gpg не может быть проверен. Файл gpg не может быть загружен. Проверьте правильность URL. Образ был успешно записан. Файл с суммами не может быть проверен. Файл с суммами не может быть загружен. Проверьте правильность URL. Данный ISO-образ подтвержден доверенной подписью. Данный ISO-образ подтвержден недоверенной подписью. Даннй ISO-образ похож на образ Windows. Это официальный ISO-образ. Вся информмация  на USB-флеш-накопителе будет уничтожена. Продолжить? Адреса URL Запись образа на USB-накопитель USB-флеш-накопитель Форматирование USB-флеш-накопителя USB-накопитель: Неизвестная подпись Недоверенная подпись Проверить Проверка подлинности и целостности образа Метка тома: Том: Образы Windows требуют специальной обработки. Записать байт неизвестно 