��    U      �  q   l      0  ?   1  B   q  !   �  $   �      �               %     1     5  5   M     �     �  _   �     �     	  3   	  +   E	     q	     y	     �	  	   �	     �	     �	  	   �	  
   �	     �	     �	  Z   �	  C   6
     z
     �
     �
     �
     �
  	   �
     �
  
   �
  F   �
     #     ,     1     8     X     ^     p     �  L   �     �                          !  -   -  +   [     �  
   �     �     �     �     �     �     �  D   �     2     :     C     K  3   a  *   �  +   �     �               /     5     E  6   R  E   �     �     �     �     �  �  �  A   �  >   
  $   I  !   n     �     �     �     �     �     �  -   �  	          R        p     }  5   �     �     �     �     �     �     �       	             &     3  Q   F  :   �     �     �     �     �     �  	   �     	       B   )  	   l     v     {  !   �     �     �  $   �     �  R     	   X  	   b     l     y  	   }     �  0   �  0   �     �     �                         5     <  B   I     �     �     �     �  )   �  )   �  $        *     F     \     l     s     z  -   �  7   �     �     �  	                 K   '         M            +       $   &      B                     2   ,   3   0   =      8          O       	   /          Q       G   5   S          >               L   @   A       <      T   .             #   I   J       9   U      R   1   6          E   F   :       *   7              -   4      ?   )   "          C   !           (                         D       H       P   ;              
   %          N    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-27 14:15+0000
Last-Translator: AlephAlpha <alephalpha911@gmail.com>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 需下载 %(downloadSize)s，释放了 %(localSize)s 磁盘空间 需下载 %(downloadSize)s，需要 %(localSize)s 磁盘空间 释放了 %(localSize)s 磁盘空间 需要 %(localSize)s 磁盘空间 正在运行 %d 项任务 3D 关于 附件 全部 已完成所有操作 试图添加 Flatpak 仓库时出现错误。 棋牌类 无法安装 无法在有活动的操作时处理此文件。
请在其完成后再次尝试。 无法移除 聊天 点击<a href='%s'>此处</a>来添加您的评价。 正在处理以下的软件包 细节 绘图 编辑精选 教育 电子 电子邮件 模拟器 必需组件 文件共享 第一人称视角 Flatpak 支持目前不可用。请尝试安装 flatpak 和 gir1.2-flatpak-1.0。 Flatpak 支持目前不可用。请尝试安装 flatpak。 字体 游戏 图像 安装 安装新程序 已安装 已安装的应用 正在安装 安装此软件包将对您的系统造成不可修复的损害。 互联网 Java 启动 将搜索限制在当前的列表 数学 多媒体编码译码器 用于KDE的多媒体编码解码器 找不到匹配的软件包 没有软件包可展示。
这可能表示存在问题。请尝试刷新缓存。 不可用 未安装 办公软件 PHP 软件包 摄影 请耐心等待。这可能需要一些时间... 请使用 apt-get 命令来安装这个软件包 程序设计 出版 Python 即时战略 刷新 刷新软件包列表 移除 正在移除 移除此软件包将对您的系统造成不可修复的损害。 评论 扫描 科学 科学及教育 在软件包描述中搜索(搜索更慢) 在软件包摘要中搜索(搜索较慢) 正在搜索软件仓库，请稍候 显示已安装应用程序 飞行模拟与赛车 软件管理器 音频 影音 系统工具 您要添加的 Flatpak 仓库已经存在。 有正在进行中的操作。
您确定要退出吗？ 回合制战略 视频 查看器 网络 