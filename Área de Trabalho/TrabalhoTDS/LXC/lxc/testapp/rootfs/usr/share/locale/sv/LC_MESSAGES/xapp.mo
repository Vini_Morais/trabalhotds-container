��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  
   �     �     �  )   �          %  
   ,     7     @     N     ]     k     s     z  	   �     �     �     �  
   �  	   �     �      �     �     �     �     �     �            "        B  #   [          �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-26 00:03+0000
Last-Translator: Jan-Olof Svensson <jan-olof.svensson@abc.se>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Om Åtgärder Tillåt sökvägar Program Det område där XApp visar status-ikoner Bläddra Avbryt Kategorier Kategori Välj en ikon Standardvärde Förvald ikon Enheter Emblem Emojier Favoriter Ikon Ikonstorlek Bild Hämtar... Mimetyper Öppna Stöd saknas för denna åtgärd Annan Platser Sök Välj Välj bildfil Status Förvald kategori Den ikon som används som standard Föredragen ikonstorlek. Strängen som representerar ikonen. Om du vill tillåta sökvägar. Status-appen XApp Verktyg för status-appen XApp 