��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  ?     =   S      �     �  (   �     �     �  	         
          "  @   >  
        �  `   �     �       A     %   Z     �     �     �  	   �     �     �  
   �  
   �  	   �     �  q   �  X   `     �     �     �  	   �     �  
   �            Q   '  	   y     �     �      �     �     �     �     �  X        `     r     �     �     �     �  )   �  ,   �     �                     1     :     V     \  P   d     �     �  	   �     �  ,   �  '     %   ?     e     �     �     �     �     �  9   �  G        O     d  
   j     u         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-07-09 12:40+0000
Last-Translator: André Savik <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s å laste ned, %(localSize)s diskplass frigjort %(downloadSize)s å laste ned, %(localSize)s diskplass kreves %(localSize)s diskplass frigjort %(localSize)s diskplass kreves %d oppgave på gang %d oppgaver på gang 3D Om Tilbehør Alle Alle applikasjoner Alle operasjoner er ferdige Det oppstod en feil under forsøk på å legge til Flatpak-repo. Brettspill Kan ikke installere Kan ikke behandle denne filen mens det er aktive operasjoner.
Prøv igjen etter at de er ferdig. Kan ikke fjerne Nettprat Klikk <a href='%s'>here</a> for å legge til din egen anmeldelse. jobber for tiden med følgende pakker Detaljer Tegning Redaktørens valg Utdanning Elektronikk E-post Emulatorer Nødvendig Fildeling Førsteperson Støtte for Flatpak er ikke tilgjengelig for øyeblikket. Forsøk å installere flatpak og gir1.2-flatpak-1.0.™ Støtte for Flatpak er ikke tilgjengelig for øyeblikket. Forsøk å installere flatpak. Skrifttyper Spill Grafikk Installer Installer nye programmer Installert Installerte applikasjoner Installerer Installering av denne pakken kan forårsake uopprettelig skade på systemet ditt. Internett Java Start Begrens søk til gjeldende liste Mattematikk Multimedia-kodeker Multimedia-kodeker for KDE Ingen matchende pakker funnet Ingen pakker å vise.
Dette kan indikere et problem - prøv å oppdatere hurtigbufferen. Ikke tilgjengelig Ikke installert Kontor PHP Programpakke Fotografering Vær tålmodig. Dette kan ta litt tid ... Bruk apt-get for å installere denne pakken. Programmering Publisering Python Sanntidsstrategi Oppdater Oppdater listen over pakker Fjern Fjerner Hvis du fjerner denne pakken, kan det føre til uopprettelig skade på systemet. Vurderinger Skanner Vitenskap Vitenskap og Utdanning Søk i pakkebeskrivelser (enda tregere søk) Søk i pakkesammendraget (tregere søk) Søker programvarelager, et øyeblikk Vis installerte applikasjoner Simulering og racing Programvarebehandler Lyd Lyd og video Systemverktøy Flatpak-repo du prøver å legge til eksisterer allerede. Det er for tiden aktive operasjoner.
Er du sikker på at du vil slutte? Rundebasert strategi Video Framvisere Nett 