��    ,      |  ;   �      �  ?   �  B   	  !   L  $   n     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �       	        &     /     =     K     R     Z  +   f     �  
   �     �     �     �     �     �  3   �  *        I     _     p     �     �     �     �  �  �  ^   �  U   	  9   ]	  0   �	     �	     �	     �	     �	  	   �	      
     
     
     "
     0
     K
  	   X
     b
  	   o
     y
     �
     �
     �
     �
     �
     �
     �
  .   �
     -     <  #   J     n     ~     �     �  J   �  H   �     >     Y     u     �  ,   �     �     �            #         "                     )          !               ,   (                &       +                               $       '                 	          %                *   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-30 06:03+0000
Last-Translator: GunChleoc <Unknown>
Language-Team: Akerbeltz
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: gd
 %(downloadSize)s ri luchdadh a-nuas, thèid %(localSize)s a dh’àite a shaoradh air an diosg %(downloadSize)s ri luchdadh a-nuas, feumaidh e %(localSize)s a dh'àite air an diosg Thèid %(localSize)s a dh’àite a shaoradh air an diosg Feumaidh e %(localSize)s a dh'àite air an diosg 3D Mu dhèidhinn Co-innealan Geamannan-bùird Cabadaich Mion-fhiosrachadh Peantadh Post-d Aithrisichean Co-roinneadh fhaidhlichean Cruthan-clò Geamannan Grafaigeachd Stàlaich Stàlaich aplacaidean ùra Stàlaichte An t-eadar-lìon Chan eil seo ri fhaighinn Cha deach a stàladh Oifis Pacaid Togail dhealbhan Cleachd apt-get gus a' phacaid seo a stàladh. Prògramachadh Foillseachadh Geamannan ro-innleachdach fìor-ama Thoir air falbh Lèirmheasan Sganadh Saidheans agus Foghlam Lorg ann an tuairisgeulan nam pacaidean (bidh an lorg fiù nas slaodaiche) Lorg ann an gearr-chunntasan nam pacaidean (bidh an lorg nas slaodaiche) Samhlachadh agus rèiseadh Manaidsear a' bhathair-bhog Fuaim agus video Innealan an t-siostaim Geamannan ro-innleachdach cuairt air chuairt Sealladairean An lìon 