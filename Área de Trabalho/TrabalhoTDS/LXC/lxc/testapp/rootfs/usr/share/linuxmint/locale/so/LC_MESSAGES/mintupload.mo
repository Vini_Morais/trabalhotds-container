��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  <   Q  <   �  3   �  !   �  P   !     r  6     	   �     �     �     �     �     �     �          -  �   H  )   �  3     *   @     k  %   �     �     �     �  +   �     �     �     �     �  �   �     �     �     �  G   �               )  <   5  3   r  )   �  (   �  7   �     1     C     J     [     t     �     �  #   �  *   �  !        *     ?  Q   M     �  
   �  1   �                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-06-05 09:38+0000
Last-Translator: Ismael Omar <ismaelhssn@gmail.com>
Language-Team: Somali <so@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s laguma xadidin feelka sifadda ee yaala %(2)s ama %(3)s %(percentage)s oo %(number)d feel - U daabulayaa %(service)s %(percentage)s oo 1 feel - U daabulayaa %(service)s %(size_so_far)s oo %(total_size)s %(size_so_far)s oo %(total_size)s - %(time_remaining)s ka harsan (%(speed)s/sec) %s Astaamaha <b>Fadlan u geli magac khidmadda daabulidda cusub:</b> Kusaabsan B Kansal Kansal daabulidda? Hubi iskuxirnaanshaha Xir Ma helin masaaxo ku filan Ma keydin bedelada rogrogida Qeex khidmadaha daabulidda Majaladda lagu daabulayo. <TIMESTAMP> waxaa lagu bedelay shabadda waqtiga ee hada ah, kan soo socda waa shabadda waqtiga oo la siiyay. Sida caadiga ah: . Ma dooneysaa inaad kansashid daabulidaan? Soojiid &amp; Halkaan kusii daa si loogu daabulo %s Feel ka weyn masaaxadda ka banaan khidmada Feel ka weyn xadiga khidmada Daabulidda feelka waa lagu guleystay. GB GiB Sooraha: Magaca sooraha ama cinwaan IP, caadiga ah:  KB KiB MB MiB Baasweer, caadiga ah: aan baasweer laheyn isku xirnaanshaha SCP-da, xuruufo banaa isku xirnaanshaha FTP-da, furayaasha ~/.ssh loo isticmaala iskuxirnaanshaha SFTP-da Baasweerka: Dhabiga: Godka: Godka fog, caadiga ah waxa waaye 21 oo FTP-da, 22 oo SFTP-da iyo SCP-da Hoos ka wad shaqadda Magaca khidmadda: Khidmadaha: Lagu guuleystay daabulidda %(number)d feel loo '%(service)s' Lagu guuleystay daabulidda 1 heel loo '%(service)s' U daabulidda '%(service)s' waa la kasalay Khidmadaan waxey u baahantahay baasweer. Shabadda waqtiga qaabkeeda (strftime). Sida caadiga ah: Shabadda waqtiga: Nooca: Raabidadda(URL): Khidmad aan la garan: %s Agaasimaha daabulidda Agaasimaha daabulidda... Khidmadaha daabulidda U daabulidda '%s' wey fashilmatay:  Daabulayaa %(number)d feel loo %(service)s Daabulayaa 1 feel loo %(service)s Daabulayaa feelka... Isticmaalaha: Magaca isticmaalaha, caadiga ah ka dhigaya magaca aad isticmaashid ee maxaliga ah _Feel _Caawinaad iskuxirnaanshaha si guul ah ayaa loo meel mariyey 