��    o     �  �        �  o   �     )  �   A  Y   �  1   ,   %   ^   !   �   %   �      �   ;   �      "!     8!     V!  @   u!  =   �!  !   �!  C   "  D   Z"  @   �"  A   �"  c   "#  `   �#  `   �#  ]   H$  '   �$  $   �$  F   �$  ?  :%     z&     �&  =   �&  :   �&     *'  @   I'  A   �'  =   �'  >   
(  `   I(  ]   �(  ]   )  Z   f)  K   �)  #   *  )   1*  -   [*     �*  #   �*     �*  I   �*      +     3+  	   @+     J+  *   R+     }+     �+  	   �+  �   �+     �,  C   �,  G   �,  5   0-     f-  !   y-      �-  �  �-  V  q/     �0  7   �0  $    1  2   E1  6   x1  M   �1     �1  <   2  J   V2  #   �2  5   �2  =   �2  <   93  *   v3     �3     �3  �   �3  )   �4  $   �4  �   �4  P   j5     �5  -   �5  V   �5     L6  /   k6  )   �6  u   �6     ;7  5   Z7     �7     �7     �7  +   �7     8  $   #8  E   H8  4   �8  -   �8  @   �8  4   29     g9  o   �9  Z   W:  ^   �:     ;  �   .;     �;     <  .   %<  0   T<  A   �<  '   �<     �<  ;   �<  E   4=  E   z=  �   �=    �>     �?     �?  $   �?  4   	@     >@     S@  J   o@  =   �@  ]   �@  P   VA     �A     �A  $   �A  0   �A     B  *   >B  �   iB  <   �B  =   4C  r   rC  `   �C     FD  %   \D     �D  L   �D  (   �D  6   E  4   NE  5   �E      �E     �E     �E     
F     &F  "   CF  %   fF  s   �F  !    G  +   "G  4   NG  +   �G  4   �G  (   �G  *   H  (   8H  4   aH  5   �H  7   �H  7   I  6   <I     sI  -   �I  4   �I  -   �I  0   $J  #   UJ     yJ  $   �J  4   �J  *   �J     K  �   %K  ,   �K  0   �K  6   L  ;   >L  9   zL  ]   �L  �   M  �   �M  	   cN  {   mN  B   �N  7   ,O  +   dO  -   �O  }  �O  )   <Q  $   fQ  k  �Q    �V  &   Y     3Y     ;Y     BY  /   `Y  #   �Y  w   �Y  E   ,Z  /   rZ     �Z     �Z  (   �Z  �  �Z  (   �]     ^     4^  .   P^  /   ^  4   �^     �^  .   �^  )    _  >   J_     �_     �_     �_  >   �_  �   �_  8   �`  %   �`  9   �`      -a  Q   Na  X   �a  S   �a      Mb  �   nb  Y   �b  2   Lc  ;   c  m   �c  7   )d  2   ad     �d  +   �d  _   �d     >e  o   We     �e  &   �e  Z   f     gf  1   yf  4   �f  e   �f  0   Fg  7   wg     �g  ;   �g  (   h  o   ,h  #   �h     �h  #   �h  :   i     ?i     Wi  &   gi  <   �i  +   �i     �i     j  =   ,j  A   jj     �j  G   �j  	    k  �   
k  �   �k  �   �l  �   m  v   �m  )   n  2   <n  <   on  Z   �n     o  4   o     Qo      Zo  .   {o     �o     �o  ,   �o  1   p  +   8p  =   dp  &   �p  F   �p  )   q  5   :q     pq  H   yq  4   �q  >   �q  #   6r  4   Zr     �r  	   �r  *   �r  3   �r  R    s  .   Ss  /   �s     �s     �s  3   �s     �s     	t  3   t  	   Et  &   Ot  3   vt  /   �t     �t  1   �t  �   #u     �u  E   �u  D   v  C   Kv  "   �v  ,   �v  K   �v     +w     3w     7w  �   Ww  �   +x  �   �x  u   �y  k   z     wz  �   �z      {     @{  [   U{  x   �{  V   *|  r   �|  k   �|  �   `}  �   �}  G   �~  9   �~  "     %   =     c     z  ~  �  �   	�     ��  �   ��  o   Z�  .   ʂ  '   ��  !   !�  "   C�     f�  9   ��     ��  ,   σ  -   ��  \   *�  Y   ��  '   �  X   	�  W   b�  S   ��  T   �  �   c�  �   �  �   l�     �  /   n�  (   ��  E   ǈ  J  �  )   X�  *   ��  Y   ��  V   �  $   ^�  S   ��  T   ׋  P   ,�  Q   }�  �   ό     Q�     э  |   Q�  R   Ύ  0   !�  >   R�  K   ��  &   ݏ  !   �     &�  [   9�     ��     ��     ��     Ð  5   ɐ     ��     �  
   �    *�     6�  K   O�  O   ��  ?   �     +�  !   A�  *   c�  �  ��    x�  %   ��  P   �  <   o�  @   ��  5   �  U   #�  "   y�  [   ��  i   ��  9   b�  8   ��  m   ՙ  G   C�  2   ��  '   ��  $   �  �   �  ;   ��  7   ��  �   -�  Q   ޜ     0�  5   <�  Q   r�  %   ĝ  5   �  /    �  �   P�     ؞  I   ��  	   @�      J�  !   k�  ,   ��     ��  +   ֟  S   �  6   V�  7   ��  8   Š  :   ��  �   9�  �   ݡ  n   g�  n   ֢  !   E�  �   g�     M�     d�  6   ��  8   ��  D   �  +   6�     b�  X   r�  I   ˥  S   �  �   i�  +  _�     ��     ��     ��  F   ݨ     $�     A�  \   W�  O   ��  y   �  U   ~�     Ԫ     �  5   �  5   ;�  )   q�  (   ��  �   ī  ;   �  ;   ��  �   ��  e   ��     ��  &   �  &   5�  N   \�  *   ��  A   ֮  >   �  :   W�  #   ��     ��     ԯ     �  (   �  %   1�  :   W�  �   ��  #   �  8   9�  A   r�  8   ��  A   ��  *   /�  ,   Z�  *   ��  3   ��  4   �  A   �  9   ]�  >   ��  '   ֳ  ;   ��  =   :�  ,   x�  /   ��  3   մ  	   	�  3   �  >   G�  +   ��  $   ��  �   ׵  %   v�  2   ��  9   ϶  D   	�  H   N�  �   ��  �   �  �   ��     ��  q   ��  O   �  F   Q�  0   ��  2   ɺ  �  ��  4   ��  $   .�  %  S�  d  y�  ,   ��     �     �     �  :   7�  5   r�  �   ��  M   /�  0   }�     ��     ��  5   ��    "�  &   8�  7   _�  1   ��  7   ��  7   �  F   9�  
   ��  4   ��  *   ��  U   ��  	   A�     K�     b�  -   }�  �   ��  4   ^�  '   ��  B   ��  $   ��  c   #�  c   ��  `   ��  *   L�  �   w�  p   ��  A   m�  B   ��  w   ��  =   j�  8   ��      ��  (   �  m   +�     ��  �   ��     6�  +   N�  h   z�     ��  C   ��  B   >�  s   ��  2   ��  C   (�      l�  J   ��  4   ��  �   �  #   ��      ��  !   ��  H   �     W�     p�  *   ��  M   ��  3   ��     3�  $   P�  C   u�  E   ��     ��  T   �  
   a�  �   l�  �   a�  �   ?�  �   ��  ~   ��  *   	�  /   4�  E   d�  j   ��     �  I   )�  	   s�  ,   }�  2   ��     ��     ��  1   �  1   >�  @   p�  D   ��  +   ��  F   "�  0   i�  3   ��     ��  Q   ��  9   -�  8   g�  #   ��  .   ��  
   ��  	   ��  7   �  ?   @�  ]   ��  >   ��  4   �     R�     V�  3   [�  #   ��     ��  5   ��     ��  2   �  @   4�  F   u�     ��  5   ��  �   �     ��  G   ��  H   �  E   L�  0   ��  .   ��  p   ��  	   c�     m�  "   q�  �   ��  �   y�  �   E�  |   #�  m   ��     �  �   *�  "   ��     ��  ^   �  |   b�  H   ��  w   (�  {   ��  �   �  �   ��  Q   ��  O   ��  ,   ?�  *   l�     ��     ��                �   R   V          t   l      Y       �   T   C   �   �   <  �   s   �       I   �   W   �   9   d           �   �       �   i          �     0       h                     
   c   ^   V   �   A  k   �   *  ,             &   H  �   �       m  �   )   g  �   q   f   �   A   .       �   |   8      �   >       �          "   �   r      =                    �      �                M   P  �   0          �   �       j             )  �       �   h  �   [   �   Y  �       ?     e  +     �         _  �               ]   E      �       �   B   [     ,  �   O           �   l       <   o  	           G   >  U  �   O  �   N        �     {   �           T      �   �   ;  M  �   `  \                 �   p   '  \      C          �               :    8      Z  -  �       �   �   �       F   Q   1   �   �         �   �   �   K  �   a       �   �       ;              w   �   5          �   %   .  D   7             
             S   $  e   v       @      �       ^  N       "        �   4         i  �       �       �   b  /                                  (   R      �   �   J   �   d      �   j            P     =  L  Q  U   9      @   z       3   �   :   #   y   �          4   �           �   �   �               !   �   �      L   �   �       +   F          �          2   �   3          o   2  �       B  �            1  �   �   H   E       X  �   k  }       �   G  �   �   �       �       X   a  `     �           ?   Z   �   I  &  -   6   D      n       7  ]  �   �   f  6  x       �   !  �   �   �       �           �     n          �       �   �   _      *   �     S     �   �         K   �   	   J  b   �   �   ~   5   �         �   m      �   u   �   �   �               '       �   �      �             $   W    �   �         %  �   �   (      �      g   c  #  �   �   /   
Error: Setting global apt proxy and pro scoped apt proxy at the same time is unsupported. No apt proxy is set.  * Service has variants  A new version is available: {version}
Please run:
    sudo apt-get install ubuntu-advantage-tools
to get the latest bug fixes and new features.  Make sure to run
    sudo apt-get update
to get the latest package information from apt.  {issue} [{context}] does not affect your system.  {issue} [{context}] is not resolved.  {issue} [{context}] is resolved.  {issue} does not affect your system.  {issue} is not resolved.  {issue} is resolved by livepatch patch version: {version}.  {issue} is resolved. %lu esm-apps security updates %lu esm-infra security updates %lu esm-infra security updates and %lu esm-apps security updates %lu esm-infra security updates and 1 esm-apps security update %lu standard LTS security updates %lu standard LTS security updates and %lu esm-apps security updates %lu standard LTS security updates and %lu esm-infra security updates %lu standard LTS security updates and 1 esm-apps security update %lu standard LTS security updates and 1 esm-infra security update %lu standard LTS security updates, %lu esm-infra security updates and %lu esm-apps security updates %lu standard LTS security updates, %lu esm-infra security updates and 1 esm-apps security update %lu standard LTS security updates, 1 esm-infra security update and %lu esm-apps security updates %lu standard LTS security updates, 1 esm-infra security update and 1 esm-apps security update '{arg}' is not formatted as 'key=value' '{endpoint}' is not a valid endpoint *Your Ubuntu Pro subscription has EXPIRED*
Renew your service at {url} *Your Ubuntu Pro subscription has EXPIRED*
{{pkg_num}} additional security update(s) require Ubuntu Pro with '{{service}}' enabled.
Renew your service at {url} *Your Ubuntu Pro subscription has EXPIRED*
{{pkg_num}} additional security update(s) require Ubuntu Pro with '{{service}}' enabled.
Renew your service at {url} 1 esm-apps security update 1 esm-infra security update 1 esm-infra security update and %lu esm-apps security updates 1 esm-infra security update and 1 esm-apps security update 1 standard LTS security update 1 standard LTS security update and %lu esm-apps security updates 1 standard LTS security update and %lu esm-infra security updates 1 standard LTS security update and 1 esm-apps security update 1 standard LTS security update and 1 esm-infra security update 1 standard LTS security update, %lu esm-infra security updates and %lu esm-apps security updates 1 standard LTS security update, %lu esm-infra security updates and 1 esm-apps security update 1 standard LTS security update, 1 esm-infra security update and %lu esm-apps security updates 1 standard LTS security update, 1 esm-infra security update and 1 esm-apps security update A change has been detected in your contract.
Please run `sudo pro refresh`. A fix is available in {fix_stream}. A fix is coming soon. Try again tomorrow. A reboot is required to complete {operation}. API endpoint to call APT failed to install the package.
 APT install failed. APT lock is held. Ubuntu Pro configuration will wait until it is released APT update failed. AUTO_ENABLED AVAILABLE Account All Updates for the Robot Operating System Anbox Cloud Are you sure? (y/N)  Arguments Attach this machine to Ubuntu Pro with a token obtained from:
{url}

When running this command without a token, it will generate a short code
and prompt you to attach the machine to your Ubuntu Pro account using
a web browser. Attaching the machine... Auto-attach image support is not available on this image
See: {url} Auto-attach image support is not available on {{cloud_type}}
See: {url} Automatically attach on an Ubuntu Pro cloud instance. Available Commands Backing up {original} as {backup} Block waiting on pro to complete CAUTION: Your Ubuntu Pro subscription expired on {{expired_date}}.
Renew your subscription at {url} to ensure
continued security coverage for your applications.
Your grace period will expire in {{remaining_days}} day. CAUTION: Your Ubuntu Pro subscription expired on {{expired_date}}.
Renew your subscription at {url} to ensure
continued security coverage for your applications.
Your grace period will expire in {{remaining_days}} days. CAUTION: Your Ubuntu Pro subscription will expire in {{remaining_days}} day.
Renew your subscription at {url} to ensure
continued security coverage for your applications. CAUTION: Your Ubuntu Pro subscription will expire in {{remaining_days}} days.
Renew your subscription at {url} to ensure
continued security coverage for your applications. Calls the Client API endpoints. Can't find series {series} in the distro-info database. Can't load the distro-info database. Cannot install package {package} version {version} Cannot provide both --args and --data at the same time Cannot set {key} to {value}: <value> for interval must be a positive integer. Canonical Livepatch service Canonical servers did not recognize this image as Ubuntu Pro Canonical servers did not recognize this machine as Ubuntu Pro: "{detail}" Choose: [E]nable {service} [C]ancel Choose: [R]enew your subscription (at {url}) [C]ancel Choose: [S]ubscribe at {url} [A]ttach existing token [C]ancel Collect logs and relevant system information into a tarball. Common Criteria EAL2 Provisioning Packages Could not disable {title}. Could not enable {title}. Could not extract series information from /etc/os-release.
The VERSION filed does not have version information: {version}
and the VERSION_CODENAME information is not present Could not find past release for {release} Could not find yaml file: {filepath} Couldn't import the YAML module.
Make sure the 'python3-yaml' package is installed correctly
and /usr/lib/python3/dist-packages is in your PYTHONPATH. Currently attempting to automatically attach this machine to Ubuntu Pro services DESCRIPTION Detach this machine from Ubuntu Pro services. Detach will disable the following service: Detach will disable the following services: Disable an Ubuntu Pro service. Disabling dependent service: {required_service} Disabling incompatible service: {service} Do not pass the TOKEN arg if you are using --attach-config.
Include the token in the attach-config file instead.
     Do you want to proceed? (y/N)  Due to contract refresh, '{service}' is now disabled. ENTITLED Enable an Ubuntu Pro service. Enable services with: {command} Enable {service} with: pro enable {service} Enabling default service {name} Enabling required service: {service} Enter your new token to renew Ubuntu Pro subscription on this system: Enter your token (from {url}) to attach this system: Error parsing API json data parameter:
{data} Error while trying to parse a yaml file using 'yaml' from {path} Error: Cannot use {option1} together with {option2}. Error: Setting global apt proxy and pro scoped apt proxy
at the same time is unsupported.
Cancelling config process operation.
 Error: The current Ubuntu Pro subscription is not entitled to: {service}.
Without it, we cannot fix the system. Error: Ubuntu Pro service: {service} is not enabled.
Without it, we cannot fix the system. Error: issue "{issue}" is not recognized.
Usage: "pro fix CVE-yyyy-nnnn" or "pro fix USN-nnnn" Error: {issue_id} not found. Even though a related USN failed to be fixed, note
that {{issue_id}} was fixed. Related USNs do not
affect the original USN. Learn more about the related
USNs, please refer to this page:

{url}
 Executing `{command}` Executing `{command}` failed. Expanded Security Maintenance for Applications Expanded Security Maintenance for Infrastructure Expected value with type {expected_type} but got type: {got_type} Expected {expected} but found: {actual} FEATURES FIPS compliant crypto packages with stable security updates Failed running command '{cmd}' [exit({exit_code})]. Message: {stderr} Failed running reboot_cmds script. See: /var/log/ubuntu-advantage.log Failed to automatically attach to Ubuntu Pro services {num_attempts} time(s).
The failure was due to: {reason}.
The next attempt is scheduled for {next_run_datestring}.
You can try manually with `sudo pro auto-attach`. Failed to automatically attach to Ubuntu Pro services {num_attempts} time(s).
The most recent failure was due to: {reason}.
Try re-launching the instance or report this issue by running `ubuntu-bug ubuntu-advantage-tools`
You can try manually with `sudo pro auto-attach`. Failed to perform attach... Failure checking APT policy. Failure when uninstalling {packages} Finished upgrade of Ubuntu Pro service configuration Fixing related USNs: Fixing requested {issue_id} For a list of all Ubuntu Pro services and variants, run 'pro status --all' For a list of all Ubuntu Pro services, run 'pro status --all' For easiest security on {title}, use Ubuntu Pro instances.
Learn more at {cloud_specific_url} For example, run:
    apt-cache show {package}
to learn more about that package. Found CVEs: Found Launchpad bugs: Found related USNs:
- {related_usns} Further installed packages covered by {service}: GPG key '{keyfile}' not found. Generic version of the RT kernel (default) Get another security update through Ubuntu Pro with 'esm-apps' enabled: Get more security updates through Ubuntu Pro with 'esm-apps' enabled: Got value with incorrect type at index {index}:
{nested_msg} Got value with incorrect type for field "{key}":
{nested_msg} If used, fix will not actually run but will display everything that will happen on the machine during the command. If used, when fixing a USN, the command will not try to also fix related USNs to the target USN. Include beta services Include unavailable and beta services Initiating attach operation... Inspect and resolve CVEs and USNs (Ubuntu Security Notices) on this machine. Installed packages covered by {service}: Installed packages with an available {service} update: Installed packages with an {service} update applied: Installing packages on changed directives: {packages} Installing required snap: {snap} Installing required snaps Installing {packages} Installing {title} packages Interrupt received; exiting. Invalid command specified '{cmd}'. Invalid url in config. {key}: {value} Invalid value for {path_to_value} in /etc/ubuntu-advantage/uaclient.conf. Expected {expected_value}, found {value}. Learn more about Ubuntu Pro at %s Learn more about Ubuntu Pro for 16.04 at %s Learn more about Ubuntu Pro for 16.04 on Azure at %s Learn more about Ubuntu Pro for 18.04 at %s Learn more about Ubuntu Pro for 18.04 on Azure at %s Learn more about Ubuntu Pro on AWS at %s Learn more about Ubuntu Pro on Azure at %s Learn more about Ubuntu Pro on GCP at %s List and present information about esm-apps packages List and present information about esm-infra packages List and present information about third-party packages List and present information about unavailable packages Main/Restricted packages receive updates until {date}. Manage Ubuntu Pro configuration Management and administration tool for Ubuntu Metadata for {issue} is invalid. Error: {error_msg}. Migrating /etc/ubuntu-advantage/uaclient.conf Missing argument '{arg}' for endpoint {endpoint} NIST-certified FIPS crypto packages NOTICES No Ubuntu Pro operations are running No Ubuntu Pro services are available to this system. No affected source packages are installed. No help available for '{name}' No proxy set in config; however, proxy is configured for: {{services}}.
See {url} for more information on pro proxy configuration.
 One moment, checking your subscription first Operation in progress: {lock_holder} (pid:{pid}) Optional key or key(s) to show configuration settings. Options to pass to the API endpoint, formatted as key=value Output system related information related to Pro services Package fixes cannot be installed.
To install them, run this command as root (try using sudo) Packages from third parties are not provided by the official Ubuntu
archive, for example packages from Personal Package Archives in Launchpad. Packages that are not available for download may be left over from a
previous release of Ubuntu, may have been installed directly from a
.deb file, or are from a source which has been disabled. Packages: Please sign in to your Ubuntu Pro account at this link:
{url}
And provide the following code: {bold}{{user_code}}{end_bold} Preview of FIPS crypto packages undergoing certification with NIST Provide detailed information about Ubuntu Pro services. RT kernel optimized for Intel IOTG platform RT kernel optimized for NVIDIA Tegra platform Refresh three distinct Ubuntu Pro related artifacts in the system:

* contract: Update contract details from the server.
* config:   Reload the config file.
* messages: Update APT and MOTD messages related to UA.

You can individually target any of the three specific actions,
by passing it's target to nome to the command.  If no `target`
is specified, all targets are refreshed.
 Removing apt preferences file: {filename} Removing apt source file: {filename} Report current status of Ubuntu Pro services on system.

This shows whether this machine is attached to an Ubuntu Advantage
support contract. When attached, the report includes the specific
support contract details including contract name, expiry dates, and the
status of each service on this system.

The attached status output has four columns:

* SERVICE: name of the service
* ENTITLED: whether the contract to which this machine is attached
  entitles use of this service. Possible values are: yes or no
* STATUS: whether the service is enabled on this machine. Possible
  values are: enabled, disabled, n/a (if your contract entitles
  you to the service, but it isn't available for this machine) or — (if
  you aren't entitled to this service)
* DESCRIPTION: a brief description of the service

The unattached status output instead has three columns. SERVICE
and DESCRIPTION are the same as above, and there is the addition
of:

* AVAILABLE: whether this service would be available if this machine
  were attached. The possible values are yes or no.

If --simulate-with-token is used, then the output has five
columns. SERVICE, AVAILABLE, ENTITLED and DESCRIPTION are the same
as mentioned above, and AUTO_ENABLED shows whether the service is set
to be enabled when that token is attached.

If the --all flag is set, beta and unavailable services are also
listed in the output.
 Report the current reboot-required status for the machine.

This command will output one of the three following states
for the machine regarding reboot:

* no: The machine doesn't require a reboot
* yes: The machine requires a reboot
* yes-kernel-livepatches-applied: There are only kernel related
  packages that require a reboot, but Livepatch has already provided
  patches for the current running kernel. The machine still needs a
  reboot, but you can assess if the reboot can be performed in the
  nearest maintenance window.
 Run 'pro help {service}' to learn more SERVICE STATUS Scalable Android in the cloud Security Updates for the Robot Operating System Security compliance and audit tools Security vulnerability ID to inspect and resolve on this system. Format: CVE-yyyy-nnnn, CVE-yyyy-nnnnnnn or USN-nnnn-dd Service {name} is recommended by default. Run: sudo pro enable {name} Set and apply Ubuntu Pro configuration settings Setting {scope} APT proxy Setting {service} proxy Show customisable configuration settings Show security updates for packages in the system, including all
available Expanded Security Maintenance (ESM) related content.

Shows counts of how many packages are supported for security updates
in the system.

If called with --format json|yaml it shows a summary of the
installed packages based on the origin:
- main/restricted/universe/multiverse: packages from the Ubuntu archive
- esm-infra/esm-apps: packages from the ESM archive
- third-party: packages installed from non-Ubuntu sources
- unknown: packages which don't have an installation source (like local
  deb packages or packages for which the source was removed)

The output contains basic information about Ubuntu Pro. For a
complete status on Ubuntu Pro services, run 'pro status'.
 Skipping installing packages: {packages} Sorry, no fix is available yet. Sorry, no fix is available. Source package does not exist on this release. Source package is not affected on this release. Starting upgrade of Ubuntu Pro service configuration Subscription Successfully processed your pro configuration. Successfully refreshed your subscription. Successfully updated Ubuntu Pro related APT and MOTD messages. Summary: Target to refresh. Technical support level The following package(s) will be reinstalled from the archive: The following security update requires Ubuntu Pro with 'esm-infra' enabled: The following security updates require Ubuntu Pro with 'esm-infra' enabled: The name of the variant to use when enabling the service The system apt cache may be outdated. The system apt information was updated {days} day(s) ago. The update is already installed. The update is not installed because this system does not have
{service} enabled.
 The update is not installed because this system is attached to an
expired subscription.
 The update is not installed because this system is not attached to a
subscription.
 The update is not yet installed. There is a corrupted lock file in the system. To continue, please remove it
from the system by running:

$ sudo rm {lock_file_path} There is {updates} pending security update. There are {updates} pending security updates. This command must be run as root (try using sudo). This machine is NOT attached to an Ubuntu Pro subscription. This machine is NOT receiving security patches because the LTS period has ended
and esm-infra is not enabled. This machine is attached to an Ubuntu Pro subscription. This machine is now attached to '{contract_name}'
 This machine is now detached. This machine is now successfully attached'
 This machine is receiving security patching for Ubuntu Main/Restricted
repository until {date}. This token is not valid. To get more information about the packages, run
    pro security-status --help
for a list of available options. To install run the following: Try 'pro --help' for more information. Try Ubuntu Pro with a free personal subscription on up to 5 machines.
Learn more at {url}
 UNKNOWN: {status} Ubuntu Pro is not available for non-LTS releases. Ubuntu Pro support now requires ubuntu-advantage-pro Ubuntu Pro with '{service}' enabled provides security updates for
{repository} packages until {year}. Ubuntu kernel with PREEMPT_RT patches integrated Ubuntu security engineers are investigating this issue. Ubuntu standard updates Unable to determine unattended-upgrades status: {error_msg} Unable to determine version: {error_msg} Unable to disable '{service}' as recommended during contract refresh. Service is still active. See `pro status` Unable to perform: {lock_request}.
 Unable to process uaclient.conf Unable to refresh your subscription Unable to update Ubuntu Pro related APT and MOTD messages. Uninstalling {packages} Unknown/Expired Unset Ubuntu Pro configuration setting Updating '{service}' apt sources list on changed directives. Updating '{service}' on changed directives. Updating package lists Updating {name} package lists Use pro help <service> to get more details about each service Use {name} {command} --help for more information about a command. Valid until Value provided was not found in {enum_class}'s allowed: value: {values} Variants: WARNING: this output is intended to be human readable, and subject to change.
In scripts, prefer using machine readable data from the `pro api` command,
or use `pro {command} --format json`.
 Warning: Failed to load /etc/ubuntu-advantage/uaclient.conf.preinst-backup
         No automatic migration will occur.
         You may need to use "pro config set" to re-set your settings. Warning: Failed to migrate /etc/ubuntu-advantage/uaclient.conf
         Please add following to uaclient.conf to keep your config: Warning: Failed to migrate user_config from /etc/ubuntu-advantage/uaclient.conf
         Please run the following to keep your custom settings: Warning: Setting the {current_proxy} proxy will overwrite the {previous_proxy}
proxy previously set via `pro config`.
 Warning: {old} has been renamed to {new}. You have no packages installed from a third party. You have no packages installed that are no longer available. You have received {updates} security
update. You have received {updates} security
updates. a connectivity error a service to view help output for. One of: {options} advanced allow beta service to be enabled an error from Canonical servers: "{error_msg}" an error while reaching {url} an unknown error arguments in JSON format to the API endpoint attach this machine to an Ubuntu Pro subscription automatically attach on supported platforms check for and mitigate the impact of a CVE/USN on this system collect Pro logs and debug information configuration key to unset from Ubuntu Pro services. One of: {options} current status of all Ubuntu Pro services disable a specific Ubuntu Pro service on this machine disabled do not auto-install packages. Valid for cc-eal, cis and realtime-kernel. do not enable any recommended services automatically do not prompt for confirmation before performing the {command} does the system need to be rebooted enable a specific Ubuntu Pro service on this machine enabled essential features.disable_auto_attach set in config json formatted response requires --assume-yes flag. key=value pair to configure for Ubuntu Pro services. Key must be one of: {options} list available security updates for the system manage Ubuntu Pro configuration on this machine n/a no output in the specified format (default: {default}) refresh Ubuntu Pro services related remove this machine from an Ubuntu Pro subscription requested show all debug log messages to console show detailed information about Ubuntu Pro services show system information related to Pro services show version of {name} simulate the output status using a provided token snapd does not have a wait command.
Enabling Livepatch can fail under this scenario.
Please, upgrade snapd if Livepatch enable fails and try again. standard tarball where the logs will be stored. (Defaults to ./ua_logs.tar.gz) the name(s) of the Ubuntu Pro services to disable. One of: {options} the name(s) of the Ubuntu Pro services to enable. One of: {options} the pro lock was held by pid {pid} token obtained for Ubuntu Pro authentication use the provided attach config file instead of passing the token on the cli warning yes {arg} must be one of: {choices} {bold}The machine has an expired subscription.
To proceed with the fix, a prompt would ask for a new Ubuntu Pro
token to renew the subscription.
{{ pro detach --assume-yes }}
{{ pro attach NEW_TOKEN }}{end_bold} {bold}The machine is not attached to an Ubuntu Pro subscription.
To proceed with the fix, a prompt would ask for a valid Ubuntu Pro token.
{{ pro attach TOKEN }}{end_bold} {bold}Ubuntu Pro service: {{service}} is not enabled.
To proceed with the fix, a prompt would ask permission to automatically enable
this service.
{{{{ pro enable {{service}} }}}}{end_bold} {bold}WARNING: The option --dry-run is being used.
No packages will be installed when running this command.{end_bold} {count} affected source package is installed: {pkgs} {count} affected source packages are installed: {pkgs} {count} packages installed: {dependent_service} depends on {service_being_disabled}.
Disable {dependent_service} and proceed to disable {service_being_disabled}? (y/N)  {endpoint} accepts no arguments {issue} is resolved. {num_pkgs} package is still affected: {pkgs} {num_pkgs} packages are still affected: {pkgs} {offset}{count} package from Ubuntu {repository} repository {offset}{count} packages from Ubuntu {repository} repository {offset}{count} package from a third party {offset}{count} packages from third parties {offset}{count} package no longer available for download {offset}{count} packages no longer available for download {repository} packages are receiving security updates from
Ubuntu Pro with '{service}' enabled until {year}. {service_being_enabled} cannot be enabled with {incompatible_service}.
Disable {incompatible_service} and proceed to enable {service_being_enabled}? (y/N)  {service_being_enabled} cannot be enabled with {required_service} disabled.
Enable {required_service} and proceed to enable {service_being_enabled}? (y/N)  {service} is required for upgrade, but current subscription is expired. {service} is required for upgrade, but it is not enabled. {service} is required for upgrade. {source} returned invalid json: {out} {title} access enabled {title} enabled Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-09-25 12:29-0400
Last-Translator: Lucas Moura <lucas.moura@canonical.com>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
Erro: A definição do proxy apt em escopo global e no escopo 'pro' ao mesmo tempo não é suportada. Nenhum proxy apt alterado. * Serviço tem variantes  Uma nova versão está disponível: {version}
Por favor, execute:
    sudo apt-get install ubuntu-advantage-tools
para obter as últimas correções de erros e novas funcionalides. Lembre de executar
    sudo apt-get update
para obter as informações mais recentes dos pacotes direto do apt.  {issue} [{context}] não afeta o seu sistema.  {issue} [{context}] não foi resolvida {issue} [{context}] foi resolvida  {issue} não afeta o seu sistema.  {issue} não foi resolvida  {issue} for resolvida pelo patch {version} do livepatch.  {issue} resolvida %lu atualizações de segurança do esm-apps %lu atualizações de segurança do esm-infra %lu atualizações de segurança do esm-infra e %lu atualizações de segurança do esm-apps %lu atualizações de segurança do esm-infra e 1 atualização de segurança do esm-apps %lu atualizações de segurança de LTS %lu atualizações de segurança de LTS and %lu atualizações de segurança do esm-apps %lu atualizações de segurança de LTS e %lu atualizações de segurança do esm-infra %lu atualizações de segurança de LTS e 1 atualização de segurança do esm-apps %lu atualizações de segurança de LTS e 1 atualização de segurança do esm-infra %lu atualizações de segurança de LTS, %lu atualizações de segurança do esm-infra e %lu atualizações de segurança do esm-apps %lu atualizações de segurança de LTS, %lu atualizações de segurança do esm-infra e 1 atualização de segurança do esm-apps %lu atualizações de segurança de LTS, 1 atualização de segurança de esm-infra e%lu atualizações de segurança do esm-apps %lu atualizações de segurança de LTS, 1 atualização de segurança do esm-infra e 1 atualização de segurança do esm-apps '{arg}' não está formatado como 'chave=valor' '{endpoint}' não é um endpoint válido *Sua assinatura do Ubuntu PRO EXPIROU*
Renove sua assinatura em {url} *Sua assinatura do Ubuntu Pro EXPIROU*
{{pkg_num}} atualização de segurança necessitam do ubuntu Pro com '{{service}}' habilitado.
Renove sua assinatura em {url} *Sua assinatura do Ubuntu Pro EXPIROU*
{{pkg_num}} atualizações de segurança necessitam do ubuntu Pro com '{{service}}' habilitado.
Renove sua assinatura em {url} 1 atualização de segurança do esm-apps 1 atualização de segurança do esm-infra 1 atualização de segurança do esm-infra e %lu atualizações de segurança do esm-apps 1 atualização de segurança do esm-infra e 1 atualização de segurança do esm-apps 1 atualização de segurança de LTS 1 atualização de segurança de LTS e %lu atualizações de segurança do esm-apps 1 atualização de segurança de LTS e %lu atualizações de segurança do esm-infra 1 atualização de segurança de LTS e 1 atualização de segurança do esm-apps 1 atualização de segurança de LTS e 1 atualização de segurança do esm-infra 1 atualização de segurança de LTS, %lu atualizações de segurança do esm-infra e%lu atualizações de segurança do esm-apps 1 atualização de segurança de LTS, %lu atualizações de segurança do esm-infra e 1 atualização de segurança do esm-apps 1 atualização de segurança de LTS, 1 atualização de segurança do esm-infra e %lu atualizações de segurança do esm-apps 1 atualização de segurança de LTS, 1 atualização de segurança do esm-infra e 1 atualização de segurança do esm-apps Uma mudança foi detectada no seu contrato.
Por favor, execute `sudo pro refresh`. Uma solução está disponível em {fix_stream}. Uma correção será entregue em breve. Tente de novo amanhã. É necessário reiniciar a máquina para completar a operação {operation} API endpoints que podem ser executados APT falhou ao instalar o pacote.
 APT install falhou O APT está ocupado. A configuração do Ubuntu Pro aguardará até que esteja disponível. APT update falhou AUTO_HABILITADO DISPONÍVEL Conta Todas as Atualizações para o Robot Operating System Anbox Cloud Tem certeza? (y/N)  Argumentos Vincule esta máquina a uma assinatura Ubuntu Pro com um token obtido de:
{url}

Ao executar este comando sem um token, um código pequeno será gerado
e um prompt surgirá pedindo para vincular esta máquina usando sua conta do Ubuntu Pro por meio
de um web browser. Vinculando a máquina... Suporte para auto-attach não está disponível para esta imagem
Veja {url} Suporte para auto-attach não está disponível para {{cloud_type}}
Veja: {url} Automaticamente vincular em uma instância cloud do Ubuntu Pro. Comandos Disponíveis Salvando {original} como {backup} Espera até o pro completar sua operação ATENÇÃO: sua assinatura do Ubuntu Pro expirou em {{expired_date}}.
Renove sua assinatura em {url} para garantir a
continuidade da cobertura de segurança para suas aplicações. Seu período de carência vai expirar em {{remaining_days}} dias ATENÇÃO: sua assinatura do Ubuntu Pro expirou em {{expired_date}}.
Renove sua assinatura em {url} para garantir a
continuidade da cobertura de segurança para suas aplicações. Seu período de carência vai expirar em {{remaining_days}} dias ATENÇÃO: Sua assinatura do Ubuntu Pro irá expirar em {{remaining_days}} dias.
Renove sua assinatura em {url} para garantir a
continuidade da cobertura de segurança para suas aplicações. ATENÇÃO: Sua assinatura do Ubuntu Pro irá expirar em {{remaining_days}} dias.
Renove sua assinatura em {url} para garantir a
continuidade da cobertura de segurança para suas aplicações. Chama os endpoints da API do Cliente. Não foi possível encontrar a série {series} na banco de dados do distro-info. Não foi possível carregar o banco de dados do distro-info. Não foi possível instalar o pacote {package} versão {version} Não é possível usar --args e --data ao mesmo tempo Não foi possível associar {key} a {value}: <value> precisa ser um inteiro positivo. Serviço de Livepatch da Canonical Os servidores da Canonical não reconheceram essa imagem como sendo associada ao Ubuntu Pro Os servidores da Canonical não reconheceram essa máquina como sendo associada ao Ubuntu Pro: "{detail}" Escolha: [E] para habilitar {service}; [C] para cancelar: Escolha: [R]enovar sua assinatura (em {url}); [C]ancelar Escolha: [S] para vincular através de {url}; [A] para vincular usando um token existente; [C] para cancelar: Coleta logs e outras informações relevantes do sistema em um tarball. Pacotes de Provisionamento do Common Criteria EAL2 Não foi possível desabilitar {title}. Não foi possível habilitar {title} Não foi possível extrair a série a partir de /etc/os-release.
o campo VERSION não tem a informação de versão: {version}
e o campo VERSION_CODENAME não está presente Não foi possível encontrar a versão anterior a {release} Não foi possível encontrar o arquivo yaml: {filepath} Não foi possível importar o módulo YAML. Certifique-se que o pacote 'python3-yaml' está corretamente instalada
e que /usr/lib/python3/dist-packages está no seu PYTHONPATH. Tentando agora vincular automaticamente essa máquina aos serviços do Ubuntu Pro DESCRIÇÃO Desvincule esta máquina de uma assinatura Ubuntu Pro Desvincular vai desabilitar o serviço: Desvincular vai desabilitar os serviços: Desabilite um serviço do Ubuntu Pro. Desabilitando serviço dependente: {required_service} Desabilitando serviço incompatível: {service} Não insira o parâmetro TOKEN se você está usando --attach-config.
Ao invés disso, inclua o token no arquivo de attach-config.
     Você quer prosseguir? (y/N)  Devido à atualização de contrato, '{service}' está agora desabilitado INCLUÍDO Habilite um serviço Ubuntu Pro. Habilite serviços com: {command} Habilite {service} com: pro enable {service} Habilitando serviço {name} Habilitando serviço necessário: {service} Providencie seu novo token para renovar sua assinatura do Ubuntu Pro neste sistema: Insira seu token (da {url}) para vincular seu sistema: Error ao analisar paramêtro data para API json:
{data} Erro ao analisar um arquivo yaml usando 'yaml' em {path} Erro: não é possível usar {option1} junto com {option2} Erro: Configurar a proxy de escopo global do apt junto com a proxy de escopo
pro de apt ao mesmo tempo não é suportado.
Cancelando o processo de configuração.
 Erro: O serviço {service} não está disponível para a atual assinatura do Ubuntu Pro.
Sem o serviço, não podemos corrigir o sistema. Erro: o serviço Ubuntu Pro: {service} não está habilitado.
Sem o serviço, não podemos corrigir o sistema. Erro: problema de segurnça "{issue}" não foi reconhecido.
Use: "pro fix CVE-yyyy-nnnn" ou "pro fix USN-nnnn" Erro: {issue_id} não encontrada. Mesmo que tenha acontecido uma falha ao corrigir uma USN relacionada, note
que {{issue_id}} for corrigida. USNs relacionadas não afetam a USN
original. Para saber mais sobre USNs relacionadas, por favor entre na página:

{url}
 Executando `{command}` O comando `{command}` falhou Manutenção de Segurança Expandida para Aplicações Manutenção de Segurança Expandida para Infraestrutura Esperava valor com tipo {expected_type}, mas recebeu tipo {got_type} Esperava {expected} mas encontrou: {actual} FUNCIONALIDADES Pacotes de criptografia compatíveis com FIPS com atualizações de segurança estáveis Falha ao executar comando '{cmd}' [exit({exit_code})]. Mensagem: {stderr} Falha ao executar o script reboot_cmds. Veja mais em: /var/log/ubuntu-advantage.log {num_attempts} falha(s) ao vincular automaticamente aos serviços do Ubuntu Pro.
A falha ocorreu devido a: {reason}.
A próxima tentativa está agendada para {next_run_datestring}.
Você pode tentar manualmente executando `sudo pro auto-attach`. {num_attempts} falha(s) ao vincular automaticamente aos serviços do Ubuntu Pro.
A falha mais recente ocorreu devido a: {reason}.
Tenter criar uma nova instância ou reporte esse problema executando `ubuntu-bug ubuntu-advantage-tools`
Você pode tentar manualmente executando `sudo pro auto-attach`. Falha ao vincular Falha ao verificar 'APT policy' Falha ao desinstalar {packages} Atualização das configurações de serviço do Ubuntu Pro concluída Corrigindo USNs relacionados Corrigindo {issue_id} Para uma lista como todos os serviços e variantes do Ubuntu Pro, execute 'pro status --all' Para uma lista com todos os serviços do Ubuntu Pro, execute 'pro status --all' Para segurança de forma mais conveniente no(a) {title}, use instâncias Ubuntu Pro.
Aprenda mais em {cloud_specific_url} Por exemplo, execute:
    apt-cache show {package}
para saber mais sobre este pacote. CVEs encontradas: Bugs do Launchpad encontrados: USNs relacionadas foram encontradas:
- {related_usns} Pacotes adicionais instalados cobertos por {service}: chave GPG '{keyfile}' não foi encontrada Versão genérica do kernel RT (padrão) Obtenha mais uma atualização de segurança através do Ubuntu Pro com 'esm-apps' habilitado: Obtenha mais atualizações de segurança através do Ubuntu Pro com 'esm-apps' habilitado: Valor com tipo incorreto na posição {index}:
{nested_msg} Valor com tipo incorreto para o campo "{key}":
{nested_msg} Se usado, fix não vai executar modificações. Ao invés disso, irá mostrar tudo que irá acontecer na máquina durante a execução real do comando. Se usado, ao corrigir uma USN, o comando não tentará corrigar as USN relacionadas à USN principal. Inclui os serviços beta Inclui serviços beta e indisponíveis Começando a operação de vínculo... Inspecionar e corrigir CVEs and USNs (Ubuntu Security Notices) nesta máquina. Pacotes instalados cobertos por {service}: Pacotes instalados com atualizações disponíveis por {service}: Pacotes instalados com atualizações aplicadas por {service}: Instalando pacotes baseado nas novas diretivas: {packages} Instalando snap necessário: {snap} Instalando snaps necessários Instalando {packages} Instalando pacotes do {title} Sinal de interrupção recebido; saindo. Comando inválido especificado: {cmd} url inválida no arquivo de configuração. {key}: {value} Valor inválido para {path_to_value} em /etc/ubuntu-advantage/uaclient.conf.Esperava {expected_value}, mas {value} foi encontrado. Saiba mais sobre o Ubuntu Pro em %s Saiba mais sobre o Ubuntu Pro para a versão 16.04 em %s Saiba mais sobre o Ubuntu Pro para a versão 16.04 no Azure em %s Saiba mais sobre o Ubuntu Pro para a versão 18.04 em %s Saiba mais sobre o Ubuntu Pro para a versão 18.04 no Azure em %s Saiba mais sobre o Ubuntu Pro na AWS em %s Saiba mais sobre o Ubuntu Pro no Azure em %s Saiba mais sobre o Ubuntu Pro no GCP em %s Lista e mostra informações sobre pacotes esm-apps Lista e mostra informações sobre pacotes esm-infra Lista e mostra informações presentes sobre pacotes de terceiros Lista e mostra informações sobre pacotes indisponíveis pacotes Main/Restricted receberão atualizações até {date}. Gerencie a configuração do Ubuntu Pro Ferramenta de gerenciamento e administração para o Ubuntu Metadados para {issue} não são válidos. Erro: {error_msg}. Migrando /etc/ubuntu-advantage/uaclient.conf '{arg}' está faltando para endpoint {endpoint} Pacotes FIPS de criptografia certificados pelo NIST NOTÍCIAS Nenhuma operação Ubuntu Pro está sendo executada Nenhum serviço do Ubuntu Pro está disponível neste sistema. Nenhum pacote fonte afetado está instalado Ajuda não disponível para '{name}' Nenhum proxy configurado; entrentato, um proxy está configurado para: {{services}}.
Veja {url} para mais informações sobre configuração de proxy no pro.
 Um momento, checando a sua assinatura Operação em andamento: {lock_holder} (pid:{pid}) Valor ou valores opcionais ao mostrar as configurações. Opções para passar ao endpoint da API, formatadas como chave=valor Mostre informações de sistema relacionados aos serviços do Ubuntu Pro As atualizações de pacotes não podem ser instaladas.
Para instalar os pacotes, execute esse comando como root (tente usando sudo) Pacotes de terceiros não são providenciados por meios oficiais do Ubuntu,
por examplo, pacotes de encontrados em Personal Package Archives (PPAs) do Launchpad. Pacotes que não estão disponíveis para download podem ter sobrado de
versões anteriores do Ubuntu, podem ter sido instalados diretamente por um
arquivo .deb, ou de uma fonte que foi desabilitada. Pacotes: Por favor, entre na sua conta do Ubuntu Pro neste link:
{url}
E forneça o código: {bold}{{user_code}}{end_bold} Prévia de pacotes FIPS de criptografia em processo de certificação pelo NIST Providencia informações detalhadas sobre os serviços do Ubuntu Pro. Kernel RT otimizado para a plataforma Intel IOTG Kernel RT otimizado para a plataforma NVIDIA Tegra Atualize três artefatos distintos no seu sistema relacionados ao Ubuntu Pro:

* contract: Atualize detalhes do contratos através do servidor.
* config: Recarregue as configurações encontradas no arquivo de configuração.
* messages: Atualized as mensagens APT e MOTD relacionados ao Ubuntu Pro.

Você pode individualmente executar o comando para qualquer ums das três opções,
ao fornecer o nome da opção para o comando. Se nenhuma `opção` for especificada,
todas as opções serão atualizadas.
 Removendo arquivo de preferência do apt: {filename} Removendo arquivo do apt: {filename} Reportar o atual status dos serviços do Ubuntu Pro no sistema.

Este comando mostra se uma máquina está vinculada a um contrato de suporte
do Ubuntu Pro. Quando vinculada, serão mostrados detalhes específicos
do contrato de suporte, incluindo nome do contrato, data de vencimento e o
status de cada serviço no sistema.

O status de uma máquina vinculada a um contrato possui quatro colunas:

* SERVIÇO: o nome do serviço
* INCLUÍDO: Se o serviço está incluído no contrato vinculado a máquina.
  Valores possíves: sim ou não
* STATUS: Se o serviço está habilitado nesta máquina. valores possíveis
  são: habilitado, desabilitado, n/d (se o contrato diz que o serviço está
  disponível, mas o serviço não pode ser habilitado devido a uma restrição da máquina)
  ou — (se o contrato não mostra esse serviço como disponível)
* DESCRIÇÃO: uma breve descrição do serviço

Caso a máquina esteja desvinculada de um contrato, três colunas serão mostradas.
SERVIÇO e DESCRIÇÃO são as mesmas informações mostradas acima, com a adição da
coluna:

* DISPONÍVEL: se o serviço estaria disponível caso a máquina fosse vinculada a um contrato
  Os valores possíveis aqui são sim e não.

Se --simulate-with-token for usado, serão mostradas cinco colunas.
SERVIÇO, DISPONÍVEL, INCLUÍDO, DESCRIÇÃO (os mesmos mencionados acima) e
AUTO_HABILITADO mostrando se o serviço seria habilitado automaticamente se
a máquina for vinculada a um contrato.

Se a flag --all for usada, serviços beta e indisponíveis também
serão listado.
 Reporte o atual status de reboot-required para esta máquina.

Este comando irá mostrar um dos três estados para a máquina
relacionados à operação de reiniciar o sistema:

* no: A máquina não precisa ser reiniciada
* yes: A máquina precisa ser reiniciada
* yes-kernel-livepatches-applied: Só existem pacotes relacionados ao kernel que necessitam
  de reiniciar a máquina. Entretanto, o Livepatch já providenciou
  patches para o kernel atual em execução. A máquina ainda precisa ser
  reiniciada, mas você pode averiguar se o reinício pode acontecer no
  período de manutenção mais próximo.
 Execute 'pro help {service}' para saber mais SERVIÇO STATUS Android escalável na nuvem Atualizações de Segurança para o Robot Operating System Ferramentas de auditoria e conformidade de segurança ID da Vulnerabilidade de segurança para inspecionar e corrigir neste sistema. Formato: CVE-yyyy-nnnn, CVE-yyyy-nnnnnnn ou USN-nnnn-dd O serviço {name} é recomendado por padrão. Execute: sudo pro enable {name} Define e aplica as configurações do Ubuntu Pro Configurando APT proxy {scope} Definindo proxy para {service} Mostre as opções personalizáveis da configuração Mostrar atualizações de segurança para pacotes no sistema, incluindo todos
os conteúdos relacionados a Expanded Security Maintenance (ESM)

Mostrar quantos pacotes são suportados para atualizações de segurança
neste sistema.

Se executado com --format json|yaml, mostra um sumário dos
pacotes instalados baseados em sua origem:
- main/restricted/univers/multiverse: pacotes do arquivo do Ubuntu
- esm-infra/esm-apps: pacotes dos arquivos de ESM
- third-party: pacotes instalados por fontes que não são do Ubuntu
- unknown: pacotes que não tem uma origem de instalação (como pacotes
deb locais or pacotes cuja origem foi removida)

A saída contém informação básica sobre o Ubuntu Pro. Para um status completo
a respeito dos serviços do Ubuntu Pro, execute 'pro status'.
 Não instalando os pacotes: {packages} Desculpe, não existe uma correção disponível ainda. Desculpe, não existe uma correção disponível. O pacote fonte não existe para esta versão do Ubuntu. O pacote fonte não é afetado nesta versão do Ubuntu. Começando atualização das configurações de serviço do Ubuntu Pro Assinatura Sua configuração do pro foi processada com sucesso Sua assinatura foi atualizada com sucesso. suas messagens de APT e MOTD relacionadas ao Ubuntu Pro foram atualizadas com sucesso Sumário: Opção para atualizar Nível de suporte técnico O(s) pacote(s) à seguir serão reinstalados: A seguinte atualização de segurança requer o Ubuntu Pro com 'esm-infra' habilitado: As seguintes atualizações de segurança requerem o Ubuntu Pro com 'esm-infra' habilitado: O nome da variante para usar ao habilitar o serviço O cache do apt pode estar desatualizado As informações do apt foram atualizadas há {days} dia(s) atrás A atualização já está instalada. A atualização não pode ser instalada porque o sistema não tem o serviço {service}
habilitado.
 A atualização não pode ser instalada porque o sistema está vinculado a uma
assinatura vencida.
 A atualização não pode ser instalada porque o sistema não está vinculado a
uma assinatura.
 A atualização ainda não está instalada Existe um arquivo lock corrompido no sistema. Para contunuar, por favor remova-o
do sistema ao executar:

$ sudo rm {lock_file_path} Existe {updates} atualização de segurança pendente. Existem {updates} atualizações de segurança pendentes. Esse comando precisa ser executado como root (tente usando sudo). Esta máquina NÃO está vinculada a uma assinatura do Ubuntu Pro. Esta máquina NÃO está recebendo patches de segurança, pois o período LTS acabou
e esm-infra não está habilitado. Esta máquina está vinculada a uma assinatura do Ubuntu Pro. Esta máquina está agora vinculada a '{contract_name}'
 Esta máquina está desvinculada Esta máquina foi vinculada com sucesso
 Esta máquina está recebendo patches de segurança para o repositório
Main/Restricted do Ubuntu até {date} Este token não é válido. Para obter mais informações sobre os pacotes, execute
    pro security-status --help
para uma lista das opções disponíveis. Para instalar, execute: Tente 'pro --help' para mais informações. Experimente o Ubuntu Pro com uma assinatura pessoal gratuita para até 5 máquinas.
Saiba mais em {url}
 DESCONHECIDO: {status} Ubuntu Pro não está disponível para versões do Ubuntu não LTS. O suporte ao Ubuntu Pro agora requer o pacote ubuntu-advantage-pro Ubuntu Pro com '{service}' habilitado provê atualizações de segurança
para pacotes do {repository} até {year}. Kernel do Ubuntu com patches PREEMPT_RT integrados Engenheiros de segurança do Ubuntu estão investigando o problema. Atualizações padrão do Ubuntu Não foi possível determinar o status do unattended-upgrades: {error_msg} Não foi possível determinar a versão: {error_msg} Falha ao desabilitar '{service}' conforme esperado após atualização do seu contrato. O serviço ainda está ativo. Execute `pro status` para confirmar Falha ao executar: {lock_request}.
 Falha ao processar uaclient.conf Falha ao atualizar sua assinatura Falha ao atualizar as mensagens de APT e MOTD relacioandas ao Ubuntu Pro Desinstalando {packages} Desconhecido/expirado Desabilitar a configuração do Ubuntu Pro Atualizando a lista de apt sources do '{service}' baseado nas novas diretivas Atualizando '{service}' baseado nas novas diretivas Atualizando lista de pacotes Atualizando lista de pacotes: {name} Use pro help <service> para obter mais detalhes sobre cada serviço Use {name} {command} --help para mais informações sobre um comando. Válido até Valor fornecido não está presente nos valores permitidos de {enum_class}: {values} Variantes: ATENÇÃO: a saída deste comando é para ser legível para humanos. Tal saída está
sujeita à mudanças no futuro.
Em scripts, é melhor usar a formatos legíveis para máquinas via comandos `pro api`,
ou usar `pro {command} --format json`.
 Atenção: Falha ao ler /etc/ubuntu-advantage/uaclient.conf.preinst-backup
         Nenhuma migração automática irá ocorrer.
         Você talvez precise do comando "pro config set" para resetar suas configurações. Atenção: Falha ao migrar /etc/ubuntu-advantage/uaclient.conf
         Adicione o seguinte conteúdo a uaclient.conf para manter sua configuração: Atenção: Falha ao migrar configuração de usuário de /etc/ubuntu-advantage/uaclient.conf
         Por favor, execute os seguintes comandos para manter sua configuração atual: Atenção: Definir o proxy {current_proxy} irá sobrescrever o proxy {previous_proxy},
previamente definido via `pro config`.
 Atenção: {old} foi renomeado para {new}. Você não tem pacotes instalados de terceitos. Você não tem pacotes instalados que não estejam mais disponíveis. Você recebeu {updates} atualização de
segurança. Você recebeu {updates} atualizações de
segurança. um erro de conexão serviço para qual informação de ajuda será mostrada. Um de: {options} avançado permita que serviços beta sejam habilitados um erro dos servidores da Canonical: "{error_msg}" um error ao acessar {url} um erro desconhecido argumentos em formato JSON para o endpoint da API vincule esta máquina a uma assinatura Ubuntu Pro automaticamente vincule está máquina em plataformas suportadas checa e corrige os problemas de segurança de um CVE/USN na máquina coleta logs do Pro e informações de debug chave de configuração Ubuntu Pro para desabilitar. Uma de: {options} status atual de todos os serviços do Ubuntu Pro desabilita nesta máquina um serviço do Ubuntu Pro desabilitado não instale pacotes automaticamente. Válido para cc-eal, cis e realtime-kernel. não habilite nenhum serviço recomendado automaticamente não peça por confirmação antes de executar {command} se o sistema precisa ser reiniciado habilita nesta máquina um serviço Ubuntu Pro habilitado essencial features.disable_auto_attach definida na configuração resposta formatada em json necessita do paramêtro --assume-yes par chave=valor para configurar serviços Ubuntu Pro. Chave precisa ser uma dentre: {options} lista atualizações de segurança disponíveis para o sistema gerencia configuração do Ubuntu Pro nesta máquina n/d não saída no formato especificado (default: {default}) atualiza os serviços do Ubuntu Pro relacionado desvincula esta máquina de uma assinatura Ubuntu Pro requisitado mostra todas os logs de debug na saída do comando mostra informações detalhadas sobre os serviços do Ubuntu Pro mostra informações de sistema relacionados a serviços do Ubuntu Pro mostra versão de {name} simula a mensagem de status usando um token fornecido snapd não tem o comando wait.
Habilitar o Livepatch pode falhar neste cenário.
Por favor, atualize o snapd caso a habilitação do Livepatch falhe e tente novamente. padrão tarball onde os logs serão guardados. (Valor padrão ./ua_logs.tar.gz) os nome(s) do serviços do Ubuntu Pro para desabilitar. Um de: {options} os nome(s)n dos serviços Ubuntu Pro para habilitar. Um de: {options} A lock do pro está sendo mantida pelo pid {pid} token obtido para autenticação do Ubuntu Pro use para providenciar um arquivo de configuração ao vincular ao invés de inserir o token via linha de comando atenção sim {arg} precisa ser um de: {choices} {bold}A máquina possui uma assinatura vencida.
Para continuar com a correção, um prompt irá pedir por um novo token
para renovar a sua assinatura Ubuntu Pro.
{{ pro detach --assume-yes }}
{{ pro attach NEW_TOKEN }}{end_bold} {bold}Essa máquina não está vinculada a uma assinatura do Ubuntu Pro.
Para continuar com a correção, um prompt irá te requisitar um token válido para o Ubuntu Pro.
{{ pro attach TOKEN }}{end_bold} {bold}O serviço Ubuntu Pro: {{service}} não está habilitado.
Para continuar com a correção, um prompt irá te perdir permissão para automaticamente habilitar
este serviço.
{{{{ pro enable {{service}} }}}}{end_bold} {bold}ATENÇÂO: a opção --dry-run está sendo usada.
Nenhum pacote será instalado na execução deste comando.{end_bold} {count} pacote fonte afetado está instalado: {pkgs} {count} pacotes fonte afetados estão instalados: {pkgs} {count} pacotes instalados: {dependent_service} depende do {service_being_disabled}.
Desabilitar {dependent_service} e prosseguir com a desabilitação do {service_being_disabled}? (y/N)  {endpoint} não aceita paramêtros {issue} foi resolvida {num_pkgs} pacote ainda está afetado: {pkgs} {num_pkgs} pacotes ainda estão afetados: {pkgs} {offset}{count} pacote do repositório {repository} do Ubuntu {offset}{count} pacotes do repositório {repository} do Ubuntu {offset}{count} pacote de terceiros {offset}{count} pacotes de terceiros {offset}{count} pacote não mais disponível para download {offset}{count} pacotes não mais disponíveis para download Pacotes do {repository} estão recebendo atualizações de segurança do
Ubuntu Pro com '{service}' habilitado até {year}. {service_being_enabled} não pode ser habilitado ao mesmo tempo que {incompatible_service}.
Desabilitar {incompatible_service} e prosseguir com a habilitação do {service_being_enabled}? (y/N)  {service_being_enabled} não pode ser habilitado se {required_service} estiver desabilitado.
Habilitar {required_service} e prosseguir com a habilitação do {service_being_enabled}? (y/N)  {service} é necessário para atualização, mas a atual assinatura está vencida {service} é necessário para atualização, mas o serviço está desabilitado. {service} é necessário para atualização. {source} retornou um json inválido: {out} acesso habilitado para {title} {title} habilitado 