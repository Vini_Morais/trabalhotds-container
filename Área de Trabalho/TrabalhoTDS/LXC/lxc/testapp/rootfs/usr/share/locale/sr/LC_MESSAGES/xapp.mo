��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  
   �     	     )  I   >     �     �     �     �     �     �  %        3     B     S     d     s     �  
   �     �     �     �  $   �     		     	     '	     8	  .   K	  
   z	  0   �	  K   �	  -   
  6   0
  4   g
  (   �
  ;   �
            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-28 22:16+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <(nothing)>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
Language: sr
 О програму Радње Дозволите путању Апликације Подручје одакле потиче XApp иконица стања Погледај Одустани Категорије Категорија Изаберите икону Подразумевано Подразумевана икона Уређаји Обележја Смешкићи Омиљено Иконица Величина иконице Слика Учитавање... Миме врсте Отвори Радња није подржана Остало Локације Претрага Изаберите Изаберите датотеку слике Стање Подразумевана категорија. Иконица која се користи по подразумевано Жељена величина иконице. Унос који представља иконицу. Требало би дозволити путању. „XApp“ програмче стања Фабрика „XApp“ програмчета стања 