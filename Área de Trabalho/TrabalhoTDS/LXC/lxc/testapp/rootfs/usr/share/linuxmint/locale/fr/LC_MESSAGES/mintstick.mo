��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  6   �  F   �               8     S  	   f     p     �  "   �  %   �  U   �     A  n  S     �     �     �     �     �               5     K     b  
   p     {  R   �  &   �     �  )   �     )     9     <     [     x     �  #   �     �     �     �               1     L     d  	   t     ~  .   �  L   �  .   �  +   ,  "   X  ]   {  *   �  ?     %   D  1   j  F   �  >   �  <   "  '   _     �  ^   �               !     *  
   @     K     ^  	   t  5   ~     �  	   �  7   �                    K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 14:13+0000
Last-Translator: Jonas Knauber <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Une erreur s'est produite lors de la copie de l'image. Une erreur s'est produite lors de la création d'une partition sur %s. Une erreur s’est produite Une erreur s'est produite. Erreur d'authentification. Calcul en cours... Contrôle Somme de contrôle Fichiers de somme de contrôle Somme de contrôle non concordante Choisissez un nom pour votre clé USB Veuillez télécharger à nouveau l'image ISO. Sa somme de contrôle ne concorde pas. Tout semble bon ! FAT32
   + Compatible avec tout.
   - Ne peut pas gérer les fichiers de plus de 4 Go.

exFAT
   + Compatible avec presque tout.
   + Peut gérer les fichiers de plus de 4 Go.
   - Pas aussi compatible que FAT32.

NTFS
   + Compatible avec Windows.
   - Non compatible avec Mac et la plupart des périphériques matériels.
   - Problèmes de compatibilité occasionnels avec Linux (NTFS est propriétaire et rétro-ingéniéré).

EXT4
   + Moderne, stable, rapide, journalisé.
   + Prend en charge les autorisations de fichiers Linux.
   - Non compatible avec Windows, Mac et la plupart des périphériques matériels.
 Système de fichiers : Formater Formater une clé USB Go Signatures GPG Fichier signé GPG Fichier signé GPG  : Retourner en arrière Vérification de l'ISO Image ISO  : Images ISO ISO : Si vous faites confiance à cette signature, vous pouvez faire confiance à l'ISO. Le contrôle d'intégrité a échoué. ko Clé introuvable sur le serveur de clés. Fichiers locaux Mo Créer une clé USB amorçable Créer une clé USB bootable Plus d'info Aucun ID de volume trouvé Pas assez d'espace sur la clé USB. Somme SHA256 Fichier des sommes SHA256 Fichier des sommes SHA256  : Somme SHA256  : Sélectionnez une image Sélectionnez une clé USB Sélectionnez une image Signé par : %s Taille : To La somme SHA256 de l'image ISO est incorrecte. Le fichier des sommes SHA256 ne contient pas de sommes pour cette image ISO. Le fichier des sommes SHA256 n'est pas signé. La clé USB a été formatée avec succès. La somme de contrôle est correcte La somme de contrôle est correcte mais l'authenticité de la somme n'a pas été vérifiée. Le fichier GPG n'a pas pu être vérifié. Le fichier GPG n'a pas pu être téléchargé. Vérifiez l'URL. L'image a été écrite avec succès. Le fichier des sommes n'a pas pu être vérifié. Le fichier des sommes n'a pas pu être téléchargé. Vérifiez l'URL. Cette image ISO est vérifiée par une signature de confiance. Cette image ISO est vérifiée par une signature non fiable. Cet ISO ressemble à une image Windows. C'est une image ISO officielle. Toutes les données présentes sur la clé USB seront supprimées. Souhaîtez-vous continuer ? URLs Créateur de clé USB Clé USB Formateur de clé USB Clé USB : Signature inconnue Signature non fiable. Vérifier Vérifier l'authenticité et l'intégrité de l'image Nom de volume : Volume : Les images Windows nécessitent un traitement spécial. Écrire octets inconnu 