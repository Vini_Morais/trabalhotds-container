��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  .   �  5        H     [  *   o     �     �  !   �  !   �  3   �     3     G     a  !   u     �  j   �          0     <     T     a     v     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-04-12 12:20+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Ndodhi një gabim gjatë kopjimit të imazhit. Ndodhi një gabim gjatë krijimit të ndarjes mbi %s. Ndodhi një gabim. Gabim identifikimi. Zgjidhni një emër për pajisjen tuaj USB Formato Formato një pajisje USB Krijoni një pajisje USB bootable Krijoni një pajisje USB bootable Nuk ka hapësirë të mjaftueshme në pajisjen USB. Zgjidhni një imazh Zgjidhni një pajisje USB Zgjidhni një imazh Pajisja USB u formatua me sukses. Imazhi u shkrua me sukses. Kjo do të shkatërrojë të gjitha të dhënat në pajisjen USB, a jeni të sigurt që doni të vazhdoni? Shkrues imazhi USB Pajisje USB Formatues pajisjesh USB Pajisja USB: Etiketa e vëllimit: Shkruaj 