��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  &     /   (     X     l     �  �   �          <     O  1   `                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-02-23 10:17+0000
Last-Translator: Toni Estevez <toni.estevez@gmail.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s no es un nombre de dominio válido. Bloquear el acceso a los dominios seleccionados Dominios bloqueados Bloqueador de dominios Nombre del dominio Los nombres de dominio deben comenzar y terminar con una letra o un dígito, y solo pueden contener letras, dígitos, puntos y guiones. Ejemplo: my.number1domain.com Dominio no válido Control parental Escriba el nombre del dominio que quiere bloquear 