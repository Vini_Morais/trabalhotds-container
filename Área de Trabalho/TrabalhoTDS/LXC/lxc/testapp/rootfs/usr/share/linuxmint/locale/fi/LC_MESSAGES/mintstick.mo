��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  &   �  -   �     �     �     �                    -     E     a  :   �  "   �  @  �          6     =     T     W     l     �     �     �     �     �     �  :   �     #     B  $   E     j     �  !   �  !   �     �     �  -   �          ,     A     W     e     z     �     �     �     �  +   �  ;   �  -   (     V     r  B   �  !   �  3   �  %   #  "   I  4   l  D   �  H   �  .   /  "   ^  H   �  	   �     �     �     �          (     A     ^     g     �     �  /   �     �     �  
   �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-21 15:33+0000
Last-Translator: Kimmo Kujansuu <Unknown>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Levykuvaa kopioitaessa tapahtui virhe. Tapahtui virhe luodessa osiota kohteeseen %s. Tapahtui virhe Tapahtui virhe. Todennusvirhe. Lasketaan… Tarkista Tarkistussumma Tarkistussummatiedostot Tarkistussumma ei täsmää Valitse USB-muistitikulle nimi Tarkistussumma ei täsmää. Lataa ISO-levykuva uudelleen. Kaikki näyttää olevan kunnossa! FAT32 
  + Yhteensopiva kaikkialla.
  - Ei voi käsitellä suurempia tiedostoja kuin 4Gt.

exFat
  + Yhteensopiva melkein kaikkialla.
  + Voi käsitellä suurempia tiedostoja kuin 4Gt.
  - Ei yhtä hyvin yhteensopiva kuin FAT32.

NTFS 
  + Yhteensopiva Windowsin kanssa.
  - Yhteensopimaton Macin ja useimpien laitteistojen kanssa.
  - Ajoittain yhteensopimaton Linuxin kanssa (NTFS on omistettua ja takaisinmallinnettuna).

EXT4 
  + Moderni, vakaa, nopea, journaloitu.
  + Tukee Linuxin tiedostolupia.
  - Yhteensopimaton Windowsin, Macin ja useimpien laitteistojen kanssa.
 Tiedostojärjestelmä: Alusta Alusta USB-muistitikku Gt GPG-allekirjoitukset GPG-allekirjoitettu tiedosto GPG-allekirjoitettu tiedosto: Takaisin ISO aitouden vahvistus ISO-levykuva: ISO-levykuvat ISO: Jos luotat allekirjoitukseen, voit luottaa ISO-levykuvaan. Eheyden tarkistus epäonnistui kt Avainta ei löydy avainpalvelimelta. Paikalliset tiedostot Mt Tee käynnistyvä USB-muistitikku Tee käynnistyvä USB-muistitikku Lisätietoja Aseman tunnusta ei löytynyt USB-muistitikulla ei ole riittävästi tilaa. SHA256 summa SHA256 summatiedosto SHA256 summatiedosto: SHA256 summa: Valitse iso-levykuva Valitse USB-muistitikku Valitse levykuva Allekirjoittaja: %s Koko: Tt ISO-levykuvan SHA256-summa on virheellinen. SHA256-tiedosto ei sisällä summia tälle ISO-levykuvalle. SHA256-summatiedostoa ei ole allekirjoitettu. USB-tikun alustus onnistui. Tarkistussumma on oikea Tarkistussumma on oikea, mutta summan aitoutta ei ole varmistettu. GPG-tiedostoa ei voitu tarkistaa. GPG-tiedostoa ei voitu ladata. Tarkista URL-osoite. Levykuva kirjoitettiin onnistuneesti. Summatiedostoa ei voitu tarkistaa. Summatiedostoa ei voitu ladata. Tarkista URL-osoite. Tämä ISO-levykuva on vahvistettu luotettavalla allekirjoituksella. Tämä ISO-levykuva on vahvistettu epäluotettavalla allekirjoituksella. Tämä ISO näyttää olevan Windows-levykuva. Tämä on virallinen ISO-levykuva. Tämä tuhoaa kaikki tiedot USB-muistitikulla. Haluatko varmasti jatkaa? Osoitteet USB-levykuvan kirjoittaja USB-muistitikku USB-muistitikun alustaja USB-muistitikku: Tuntematon allekirjoitus Epäluotettava allekirjoitus Tarkista Tarkista kuvan aitous ja eheys Aseman nimi: Asema: Windows-levykuvat vaativat erityiskäsittelyä. Kirjoita tavua tuntematon 