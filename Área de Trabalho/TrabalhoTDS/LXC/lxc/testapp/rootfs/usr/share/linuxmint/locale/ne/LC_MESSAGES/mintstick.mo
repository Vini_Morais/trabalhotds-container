��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  d     k   �  C   �  J   1	  V   |	  ~  �	  7   R  ;   �  5   �  5   �  A   2  ,   t  )   �  /   �  T   �  N   P  �   �     L     g  "   k     �  &   �     �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-16 11:49+0000
Last-Translator: Dipendra Khanal <Unknown>
Language-Team: Nepali <ne@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 छवि प्रतिलिपि गर्दा त्रुटि देखापर्यो %s मा विभाजन सिर्जना गर्दा त्रुटि भेटियो। एउटा त्रुटि उत्पन्न भयो । प्रमाणीकरणको क्रममा त्रुटि आफ्नो USB लागि एउटा नाम छान्नुहोस् FAT32 
  ‌+ सबै ठाँउमा उपयुक्त।
  - 4 जीबी भन्दा ठूलो फाइलहरू ह्यान्डल गर्न सक्दैन।

exFAT 
  + लगभग सबै ठाँउमा उपयुक्त।
  - 4 जीबी भन्दा ठूलो फाइलहरू ह्यान्डल गर्न सक्दछ।

NTFS 
  + विन्डोजसँग मिल्दो।
  - म्याक र धेरै हार्डवेयर उपकरणहरूसँग मिल्दो छैन।
  - Linux (NTFS स्वामित्व र रिवर्स ईन्जिनियर) सँगको कहिलेकाहीँ अनुकूलता समस्याहरू।

EXT4 
  + आधुनिक, स्थिर, छिटो, मिलाइएको।
  ‌+ लिनक्स फाइल अनुमति समर्थन गर्दछ।
  - विन्डोज, म्याक र सबैभन्दा हार्डवेयर उपकरणहरूसँग मिल्दो छैन।
 ढाँचाबद्ध गर्नुहोस् USB ढाँचाबद्ध गर्नुहोस् बुटेवल USB बनाउनुहोस् बुटेवल USB बनाउनुहोस् यस USB मा पर्याप्त ठाउँ छैन छवि चयन गर्नुहोस USB चयन गर्नुहोस् छवि चयन गर्नुहोस् USB सफलतापूर्वक ढाँचाबद्ध भऐको छ ! छवि सफलतापूर्वक लेखिएको थियो यो USB मा सबै डेटा नष्ट हुर्नेछ, के तपाईं प्रक्रिया गर्न चाहनुहुन्छ? USB छवि लेखक USB USB ढाँचाकर्ता USB: भोल्युम लेबुल: लेख्नुहोस् अज्ञात 