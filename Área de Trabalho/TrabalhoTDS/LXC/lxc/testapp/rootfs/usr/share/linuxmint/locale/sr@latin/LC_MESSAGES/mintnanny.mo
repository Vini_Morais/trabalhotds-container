��          t      �              %   0     V     f     u  q   �     �             -   1  �  _       *         K     \     j  x   w     �            ,   1                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-23 15:56+0000
Last-Translator: Saša Marjanović <Unknown>
Language-Team: Serbian Latin <sr@latin@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nije važeće ime domene. Blokiraj pristup izabranim nazivima domena Blokirani domeni Bloker domena Naziv domena Imena domena moraju počinjati i završavati se slovom ili cifrom i mogu sadržavati samo slova, cifre, tačke i crtice. Primer: my.number1domain.com Nevažeći domen Roditeljski nadzor Unesite ime domena koji želite da blokirate 