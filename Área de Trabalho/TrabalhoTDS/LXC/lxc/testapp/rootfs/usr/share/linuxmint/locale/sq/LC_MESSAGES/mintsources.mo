��    *      l  ;   �      �     �     �  .   �          	     $     +  :   A     |     �     �     �     �  $   �                     $     6    :     H     M     R     ^     e     i  8   n     �     �     �  
   �     �               ,     4  /   :  6   j  2   �     �     �  �  �     �	      �	  3   �	     
     
     $
  #   *
  B   N
     �
     �
     �
  
   �
  $   �
  &   �
     "     A     F     N     c    l     |     �     �     �     �     �  :   �     �                          ?     T     i     q  0   }  .   �  @   �          8                                                                                    *          "       %         (                    $      '      !             &                   
   )   	           #        More info: %s Adding PPAs is not supported Adding private PPAs is not supported currently Base CD-ROM (Installation Disc) Cancel Cannot add PPA: '%s'. Configure the sources for installable software and updates Country Edit the URL of the PPA Edit the URL of the repository Enabled Error: must run as root Error: need a repository as argument Fix MergeList problems GB/s Install Installed version Key Linux Mint uses Romeo to publish packages which are not tested. Once these packages are tested, they are then moved to the official repositories. Unless you are participating in beta-testing, you should not enable this repository. Are you sure you want to enable Romeo? MB/s Main Maintenance Open.. PPA PPAs Please enter the name of the repository you want to add: Purge residual configuration Remove Remove foreign packages Repository Restore the default settings Select a mirror Software Sources Sources Speed The problem was fixed. Please reload the cache. There is no more residual configuration on the system. Unable to prompt for response.  Please run with -y Unstable packages kB/s Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-11-03 19:38+0000
Last-Translator: Indrit Bashkimi <indrit.bashkimi@gmail.com>
Language-Team: Albanian <sq@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
  Më shumë informacione: %s Shtimi i PPA-ve nuk përballohet Shtimi i PPA-ve private nuk përballohet aktualisht Bazë CD-ROM (Disk instalimi) Anulo Nuk mund të shtojë PPA-në: '%s'. Konfiguro burimet për programet e instalueshme dhe përditësimet Vendi Ndryshoni URL-në e PPA-së Ndrysho URL-në e depos Aktivizuar Gabim: duhet të ekzekutohet si root Gabim: nevojitet një depo si argument Korrigjo problemet e MergeList GB/s Instalo Versioni i instaluar Çelësi Linux Mint përdor Romeo për të publikuar paketa të cilat nuk janë testuar. Pasi këto paketa të jenë testuar, ata zhvendosen në depot zyrtare. Nëse nuk merrni pjesë në testimin, nuk duhet të aktivoni këtë depo. A jeni të sigurt që doni të aktivoni Romeo? MB/s Kryesore Mirëmbajtje Hap... PPA PPA-të Ju lutem shkruani emrin e depos që dëshironi të shtoni: Fshij konfigurimin e mbetur Hiq Hiq paketat e huaja Depo Rikthe parametrat e parazgjedhur Zgjidhni një server Burimet e programeve Burimet Shpejtësia Problemi u zgjidh. Ju lutem rignarkoni cache-in. Nuk ka më konfigurime të mbetura në sistem. Nuk është e mundur të kërkohet përgjigje. Ekzekutoje me -y. Paketa të paqëndrueshme kB/s 