��    U      �  q   l      0  ?   1  B   q  !   �  $   �      �               %     1     5     F  5   ^     �     �  _   �     	     	  3   "	  +   V	     �	     �	     �	  	   �	     �	     �	  	   �	  
   �	     �	     �	  Z   �	  C   G
     �
     �
     �
     �
     �
  	   �
     �
  
   �
  F   �
     4     =     B     I     i     o     �     �  L   �                    &     *     2  +   >     j  
   v     �     �     �     �     �     �  D   �               &     .  3   D  *   x  +   �     �     �                    (  6   5  E   l     �     �     �     �  �  �  �   �  �   '  T   �  ]     7   t     �     �     �     �  6   �  T   0  z   �        9     &  V  6   }     �  z   �  Q   H     �     �  9   �       *        J  	   ]     g  *   �  '   �  �   �  r   i     �     �            6   .  !   e  ?   �  $   �  �   �  $   �     �     �  x   �     o  <   �  R   �  <     �   [     '  0   C     t     �     �     �  r   �  -   @  
   n     y  -   �     �  6   �     �  !     �   -     �     �  !      B   :   �   }   �   !     �!  K   "  <   k"  $   �"     �"  3   �"  <   #  x   N#  �   �#     n$     �$  *   �$     �$         K   (         M            ,       %   '       B                     3   -   4   1   =                 O       	   0          Q       G   6   S          >               L   @   A       <      T   /             $   I   J       9   U      R   2   7          E   F   :       +      !          .   5      ?   *   #          C   "           )                         D       H       P   ;              
   &      8   N    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-04-02 07:06+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <th@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s ที่จะดาวน์โหลด ว่าง %(localSize)s ของพื้นที่ว่างดิสก์ %(downloadSize)s ที่จะดาวน์โหลด ต้องการ %(localSize)s ของพื้นที่ว่างดิสก์ ว่าง %(localSize)s ของพื้นที่ว่างดิสก์ ต้องการ %(localSize)s ของพื้นที่ว่างดิสก์ กำลังทำงาน %d งานอยู่ 3 มิติ เกี่ยวกับ Accessories ทั้งหมด แอปพลิเคชันทั้งหมด การดำเนินการทั้งหมดเสร็จสิ้น เกิดข้อผิดพลาดขณะพยายามเพิ่มคลังแพคเกจ Flatpak เกมกระดาน ไม่สามารถติดตั้งได้ ไม่สามารถประมวลผลแฟ้มนี้ขณะที่มีการทำงานอื่น ๆ อยู่ได้
โปรดลองอีกครั้งหลังจากที่การทำงานเสร็จสมบูรณ์ ไม่สามารถเอาออกได้ การสนทนา คลิก<a href='%s'>ที่นี่</a>เพื่อเพิ่มบทวิจารณ์ของคุณ กำลังทำงานกับแพคเกจต่อไปนี้ รายละเอียด งานวาด คัดสรรโดยบรรณาธิการ การศึกษา อิเล็กทรอนิกส์ อีเมล์ Emulators สิ่งจำเป็น การแบ่งปันแฟ้ม บุคคลที่หนึ่ง ไม่มีการรองรับ Flatpak ในขณะนี้ ลองติดตั้ง flatpak และ gir1.2-flatpak-1.0 ไม่มีการรองรับ Flatpak ในขณะนี้ ลองติดตั้ง flatpak แบบอักษร เกมส์ กราฟิก ติดตั้ง ติดตั้งโปรแกรมใหม่ ติดตั้งแล้ว แอปพลิเคชันที่ติดตั้ง กำลังติดตั้ง การติดตั้งแพคเกจนี้อาจทำให้ระบบของคุณเสียหายและไม่สามารถซ่อมแซมได้ อินเทอร์เน็ต Java เรียกใช้ จำกัดการค้นหาเพียงรายการปัจจุบันเท่านั้น คณิตศาสตร์ ตัวลงรหัสมัลติมีเดีย ตัวลงรหัสมัลติมีเดียสำหรับ KDE ไม่พบแพคเกจที่ตรงกัน ไม่มีแพคเกจที่จะแสดง
ข้อความนี้อาจหมายถึงมีปัญหา โปรดลองรีเฟรชแคชใหม่ ไม่มีอยู่ ยังไม่ได้ติดตั้ง สำนักงาน PHP Package (แพคเกจ) การถ่ายภาพ กรุณาใช้คำสั่ง apt-get เพื่อติดตั้งแพกเกจนี้ การเขียนโปรแกรม Publishing Python กลยุทธ์เวลาจริง รีเฟรช รีเฟรชรายการแพกเกจ เอาออก กำลังถอดถอน การเอาแพคเกจนี้ออกอาจทำให้ระบบของคุณเสียหายและไม่สามารถซ่อมแซมได้ Reviews กำลังค้นหา วิทยาศาสตร์ วิทยาศาสตร์และการศึกษา ค้นหารายละเอียดของแพกเกจ (การค้นหาอาจใช้เวลานานมาก) ค้นหาข้อมูลแพกเกจอย่างย่อ (การค้นหาอาจใช้เวลานาน) กำลังค้นหาคลังแพคเกจซอฟต์แวร์ โปรดรอสักครู่ แสดงแอปพลิเคชันที่ติดตั้ง การจำลองและการแข่งรถ การจัดการ Software เสียง เสียงและวิดีทัศน์ เครื่องมือบริหารระบบ คลังแพคเกจ Flatpak ที่คุณพยายามเพิ่มมีอยู่แล้ว มีงานที่กำลังทำอยู่ในขณะนี้
คุณแน่ใจหรือไม่ว่าต้องการออก Turn-based strategy วิดีโอ โปรแกรมแสดงภาพ เว็บ 