��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  L   �  O   �  '      *   H  !   s  =   �     �  
   �     �     �     �             >   .     m     z     �  v   �          *  @   /  +   p     �     �  	   �     �     �     �     �  
   �     �     	          1  g   >  R   �     �        "        '     /     B     O     k     z  	   �  M   �     �     �       ,     
   :     E     Y     u  -   }  m   �          5     G  
   X     c     y  	   }  
   �  7   �  /   �     �     	               2      A     b     n     v  N   �     �     �     �     �  2     -   9  2   g      �     �  	   �     �  -   �               )     ?  .   U  5   �     �     �     �     �  U        g     s     z     �     �  $   �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-03 08:58+0000
Last-Translator: Marek Hladík <mhladik@seznam.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: cs
 %(downloadSize)s ke stažení, %(localSize)s místa na úložišti uvolněno %(downloadSize)s ke stažení, požadováno %(localSize)s místa na úložišti %(localSize)s místa na disku uvolněno požadováno %(localSize)s místa na disku %d Recenze %d Recenze %d Recenzí spuštěna %d úloha spuštěny %d úlohy spuštěno %d úloh 3D O aplikaci Příslušenství Přidat Vše Všechny aplikace Všechny operace dokončeny Došlo k chybě při pokusu o přidání Flatpak repozitáře. Deskové hry Větev: Není možné nainstalovat Tento soubor nyní není možné zpracovat, protože probíhají jiné operace.
Až budou dokončeny, zkuste to znovu. Není možné odebrat Chat Kliknutím <a href='%s'>sem</a> přidáte svou vlastní recenzi. Nyní zpracovává následující balíčky Podrobnosti Dokumentace Kreslení Editor doporučuje Výuka Elektronika E-mail Emulátory Nejpodstatnější Sdílení souborů Z vlastního pohledu Flatpak (%s) Podpora pro Flatpak není v tuto chvíli k dispozici. Zkuste nainstalovat flatpak a gir1.2-flatpak-1.0. Podpora pro Flatpak není v tuto chvíli k dispozici. Zkuste nainstalovat flatpak. Písma Hry Generování mezipaměti, strpení Grafika Domovská stránka Nainstalovat Nainstalovat nové aplikace Nainstalováno Nainstalované aplikace Instalace Instalace tohoto balíčku by způsobilo neopravitelné poškození systému. Internet Progr. jazyk Java Spustit Omezit vyhledávání na stávající výpis Matematika Kodeky multimédií Kodeky multimédií pro KDE Název: Nenalezeny žádné odpovídající balíčky Nejsou žádné balíčky k zobrazení.
To může značit problém – zkuste obnovit mezipaměť balíčků. Náhledy nejsou k dispozici Není k dispozici Nenainstalováno Kancelář Volitelné součásti PHP Balíček Fotografie Prosím buďte trpěliví. Toto může chvíli trvat… K instalaci tohoto balíčku použijte apt-get. Programování Publikování Python Real-time strategie Načíst znovu Znovu načíst seznam balíčků Vzdálený: Odebrat Odstraňování Odebrání tohoto balíčku by způsobilo neopravitelné poškození systému. Recenze Skenování Věda Věda a vzdělávání Hledat v popisech balíčků (ještě pomalejší) Hledat ve shrnutích balíčků (pomalejší) Hledání repozitářů software, okamžik prosím Zobrazit nainstalované aplikace Simulace a závody Velikost: Správa software Něco se pokazilo. Klikněte na Zkusit znovu. Zvuk Zvuk a video Systémový balíček Systémové nástroje Tento Flatpak repozitář už máte přidaný. Právě probíhají operace.
Opravdu chcete ukončit? Toto je systémový balíček Toto je balíček Flatpak Zkusit znovu Tahové strategie Nelze komunikovat se servery. Zkontrolujte připojení k internetu a zkuste to znovu. Nedostupné Verze: Video Prohlížeče Web Správce balíčků vašeho systému 