��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  >   I  Q   �  K   �  #   &  \   J     �  e   �     $     C     L     Y     s     �  6   �  D   �  .     �   ?  I   �  $   B  ]   g  Q   �  /        G     X     l  4   z     �     �     �     �  �   �     �  	   �     �  y   �     $     D     W  H   h  B   �  8   �  8   -  ;   f     �     �     �     �     �          '  (   D  <   m  4   �  #   �       E        U     c  .   q                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-04-22 10:36+0000
Last-Translator: MohammadSaleh Kamyab <Unknown>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s از %(number)d فایل - در حال ارسال به %(service)s %(percentage)s از یک فایل - در حال ارسال به %(service)s %(size_so_far)s از %(total_size)s %(size_so_far)s از %(total_size)s - %(time_remaining)s زمان مانده (%(speed)s/sec) ویژگی‌های %s <b>خواهشمند است یک نام برای سرویس جدید ارسال وارد کنید:</b> درباره‌ی برنامه بایت انصراف لغو بارگذاری؟ بررسی ارتباط بستن فضای موردنیاز قابل دسترس نیست تغییر در تنظیمات قابل ذخیره‌شدن نیست سرویسهای ارسال را مشخص کن شاخه برای ارسال . <TIMESTAMP> جایگزین برچسب زمانی فعلی می شود. فرمت برچسب زمانی روبرو داده شده است. پیش‌فرض: آیا می‌خواهید از این ارسال انصراف دهید؟ Drag &amp; Drop here to upload to %s پرونده بزرگ‌تر از حداکثر فضای قابل دسترس سرویس است پرونده بزرگتر از حداکثر گنجایش سرویس‌ها است پرونده با موفقیت ارسال شد. گیگابایت گیگا‌بایت میزبان: نام هاست یا آدرس IP، پیش‌فرض:  کیلوبایت کیلو‌بایت مگابایت مگا‌بایت رمز عبور، به صورت پیش فرض: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections رمزعبور: مسیر: درگاه: درگاه کنترل از راه دور، پیش فرض برای FTP عدد 21 و برای SFTP و SCP عدد 22 است. اجرا در پس‌زمینه نام سرویس: سرویس ها: %(number)d فایل با موفقیت به '%(service)s' ارسال شد یک فایل با موفقیت به '%(service)s' ارسال شد ارسال به '%(service)s' ابطال شده‌است این سرویس نیاز به رمزعبود دارد. فرمت برچسب زمانی (strftime). پیش‌فرض: یرچسب زمانی: نوع: نشانی وب: سرویس نا‌آشنا: %s مدیر بارگذاری مدیریت ارسال ... سرویس های ارسال ارسال به '%s' شکست خورد:  در حال ارسال %(number)d فایل به  %(service)s در حال ارسال ۱ فایل به  %(service)s در حال ارسال فایل ... کاربر: نام کاربری، پیش‌فرض برای نام محلی شما _پرونده _راهنما اتصال با موفقیت برقرار شد 