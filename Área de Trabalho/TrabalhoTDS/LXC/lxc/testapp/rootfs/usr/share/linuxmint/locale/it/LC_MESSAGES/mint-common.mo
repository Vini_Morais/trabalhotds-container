��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  3      (   T     }  "   �  (   �  (   �  3     +   F  +   r  (   �     �  N   �  	   1	     ;	     C	     X	  9   a	     �	  /   �	     �	  
   �	     �	     �	  %   
  R   7
  &   �
     �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-30 08:13+0000
Last-Translator: Dragone2 <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Verranno occupati %s di spazio aggiuntivo su disco. Verranno liberati %s di spazio su disco. In totale saranno scaricati %s. Sono richieste ulteriori modifiche Sarà retrocesso del software aggiuntivo Sarà installato del software aggiuntivo Sarà completamente rimosso del software aggiuntivo Sarà re-installato del software aggiuntivo Sarà disinstallato del software aggiuntivo Sarà aggiornato del software aggiuntivo Si è verificato un errore Impossibile rimuovere il pacchetto %s poiché è richiesto da altri pacchetti. Retrocedi Flatpak Applicazioni Flatpak Installa Il pacchetto %s è una dipendenza dei seguenti pacchetti: Pacchetti da rimuovere Controllare l'elenco delle modifiche qui sotto. Pulisci Reinstalla Rimuovi Salta aggiornamento I seguenti pacchetti saranno rimossi: Questa voce di menu non è associata a nessun pacchetto. Vuoi rimuoverla dal menu? Gli aggiornamenti verranno tralasciati Aggiorna 