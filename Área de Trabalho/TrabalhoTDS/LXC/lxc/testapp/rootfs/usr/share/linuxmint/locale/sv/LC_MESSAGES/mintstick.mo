��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  *   �  2   �     �               1     >     J     X     q  "   �  ;   �     �  Q  �  
   N  	   Y     c     {     ~     �     �     �     �     �     �     �  <   �  "   (     K  '   N     v     �     �     �     �     �  6   �          )     ?     V     d     q     �     �     �     �  .   �  K   �  *   )  *   T       ?   �  "   �  7   �     2  1   I  F   {  5   �  =   �  ,   6  "   c  e   �     �     �  	          
   -     8     H  	   \  +   f     �     �  3   �     �     �     �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-02 13:35+0000
Last-Translator: Jan-Olof Svensson <jan-olof.svensson@abc.se>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Ett fel uppstod vid kopiering av avbilden. Ett fel uppstod vid skapande av ny partion på %s. Ett fel uppstod Ett fel har uppstått. Autentiseringsfel. Beräknar... Kontrollera Kontrollsumma Filer med kontrollsummor Kontrollsumman stämmer inte Välj ett namn för ditt USB-minne Hämta ISO-avbilden igen. Dess kontrollsumma stämmer inte. Allt stämmer! FAT32 
  + Fungerar med alla system.
  - Kan inte hantera filer som är större än 4 GB.

exFAT
  + Fungerar med de flesta system.
  + Kan hantera filer som är större än 4 GB.
  - Inte lika vanligt som FAT32.

NTFS 
  + Fungerar med Windows.
  - Fungerar inte med Mac och de flesta inbyggda system.
  - Passar inte riktigt ihop med Linux (NTFS har stängd källkod vilket ger problem).

EXT4 
  + Ett modernt, snabbt och stabilt så kallat journalförande filsystem.
  + Har fullt stöd för rättigheter med mera i Linux.
  - Fungerar inte med Windows, Mac och de flesta inbyggda system.
 Filsystem: Formatera Formatera ett USB-minne GB GPG-signaturer GPG-signerad fil GPG-signerad fil: Gå tillbaka Kontroll av ISO ISO-avbild: ISO-avbilder ISO: Om du litar på signaturen så kan du lita på ISO-avbilden. Integritetskontrollen misslyckades KB Hittade inte nyckeln på nyckelservern. Lokala filer MB Skapa ett startbart USB-minne Skapa startbart USB-minne Mer information Hittade inget volym-ID Det är inte tillräckligt med utrymme på USB-minnet. SHA256-summa Fil med SHA256-summor Fil med SHA256-summor: SHA256-summa: Välj avbild Välj ett USB-minne Välj avbild Signerad av: %s Storlek: TB SHA256-summan för ISO-avbilden stämmer inte. Filen med SHA256-summor innehåller ingen summa för den här ISO-avbilden. Filen med SHA256-summor är inte signerad. USB-minnet har framgångsrikt formaterats. Kontrollsumman stämmer Kontrollsumman stämmer men dess äkthet kunde inte bekräftas. GPG-filen kunde inte kontrolleras. GPG-filen kunde inte hämtas. Kontrollera webbadressen. Avbilden har skrivits. Filen med kontrollsummor kunde inte kontrolleras. Filen med kontrollsummor kunde inte hämtas. Kontrollera webbadressen. ISO-avbilden har bekräftats med en betrodd signatur. ISO-avbilden har bekräftats med en otillförlitlig signatur. Det här verkar vara en avbild för Windows. Detta är en officiell ISO-avbild. Detta kommer att radera allt som är lagrat på USB-minnet, är du säker på att du vill fortsätta? Webbadresser USB-avbildsskrivare USB-minne USB-minnesformaterare USB-minne: Okänd signatur Ej betrodd signatur Verifiera Bekräfta avbildens äkthet och integritet. Volymens text: Volym: Avbilder för Windows kräver särskild behandling. Skriv byte okänd 