��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  )         5     V     s     �  I   �     �     �  %        *  5   H     ~     �  ^   �                                            	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-18 19:26+0000
Last-Translator: Ernestas Karalius <Unknown>
Language-Team: Lithuanian <lt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Bus papildomai panaudota %s vietos diske. Bus atlaisvinta %s disko vietos. Iš viso bus atsisiųsta %s. Reikalingi papildomi pakeitimai Įvyko klaida Neįmanoma pašalinti paketo %s, nes jis yra reikalingas kitiems paketams Flatpak paketai Įdiegti Šie paketai priklauso nuo paketo %s: Paketai, kurie bus pašalinti Peržiūrėkite žemiau esantį pakeitimų sąrašą. Šalinti Bus pašalinti šie paketai: Šis meniu elementas nėra susietas su jokiu paketu. Vis tiek norite pašalinti jį iš meniu? Naujinti 