��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  9   _  0   �  .   �     �       X   7     �     �  A   �     �  0        >  1   E  �   w     �                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: https://launchpad.net/~linuxmint-translation-team-japanese
PO-Revision-Date: 2022-06-24 19:39+0000
Last-Translator: vega m57 <Unknown>
Language-Team: Japanese <https://launchpad.net/~linuxmint-translation-team-japanese>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ja
 %s の更なるディスク領域が使用されます。 %s のディスク領域が解放されます。 合計 %s がダウンロードされます。 追加の変更が必要です エラーが発生しました 他のパッケージから要求されたパッケージ %s を削除できません｡ Flatpaks インストール パッケージ %s は次のパッケージ独自のものです: 削除されるパッケージ 以下の変更点を確認してください。 削除 これらのパッケージが削除されます: このメニュー項目は、どのパッケージにも関連付けられていません。メニューから削除しますか？ アップグレード 