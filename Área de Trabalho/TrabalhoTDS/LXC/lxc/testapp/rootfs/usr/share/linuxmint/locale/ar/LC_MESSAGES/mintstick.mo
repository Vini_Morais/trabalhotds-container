��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  -   @  5   n     �     �      �  �  �     �     �  -   �  -   (  /   V     �     �     �  &   �  *   �  �        �  
   �     �     �  #   �          %                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-27 22:01+0000
Last-Translator: Ibrahim Saed <ibraheem5000@gmail.com>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com >
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ar_EG
 حدث خطأ أثناء نسخ الصورة. حدث خطأ أثناء إنشاء قسم على %s. حدث خطأ. خطأ في المصادقة. اختيار اسم لقرص USB FAT32 
  + متوافق في كل مكان.
  - لا يمكنه التعامل مع ملفات أكبر من 4 ج.بايت.

exFAT
  + متوافق تقريبا في كل مكان.
  + يمكنه التعامل مع ملفات أكبر من 4 ج.بايت.
  - توافقه أقل من FAT32.

NTFS 
  + متوافق مع ويندوز.
  - غير متوافق مع ماك ومعظم الأجهزة.
  - مشاكل توافقية بين الحين والآخر مع لينكس (NTFS هو ملكية خاصة ومبرمج بالهندسة العكسية).

EXT4 
  +حديث، مستقر، سريع و موثق.
  + يدعم أذونات ملفات لينكس.
  - غير متوافق مع ويندوز، ماك، ومعظم الأجهزة.
 التنسيق تهيئة قرص USB إنشاء قرص USB قابل للتشغيل إنشاء قرص USB قابل للتشغيل لا توجد مساحة كافية على USB. تحديد الصورة تحديد قرص USB تحديد الصورة تم تهيئة قرص USB بنجاح. تمت كتابة الصورة بنجاح. سيؤدي ذلك إلى تدمير جميع البيانات الموجودة على قرص USB، ل تريد بالتأكيد المتابعة؟ كاتب صورة USB قرص USB مهيء قرص USB قرص USB: تسمية وحدة التخزين: الكتابة غير معروف 