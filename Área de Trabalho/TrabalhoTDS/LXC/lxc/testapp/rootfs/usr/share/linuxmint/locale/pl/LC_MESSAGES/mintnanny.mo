��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  '   �  !   '     I     \     m  |   z  &   �          1  9   G                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-22 11:40+0000
Last-Translator: Piotr Strębski <strebski@gmail.com>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s nie jest prawidłową nazwą domeny. Blokuj dostęp do wybranych domen Zablokowane domeny Blokowanie domen Nazwa domeny Nazwy domen muszą zaczynać się i kończyć literą lub cyfrą, i mogą zawierać tylko litery, cyfry, kropki i myślniki. Przykład: mój.domena_numer_jeden.com Niepoprawna domena Kontrola rodzicielska Prosimy wpisać nazwę domeny, którą chcesz zablokować 