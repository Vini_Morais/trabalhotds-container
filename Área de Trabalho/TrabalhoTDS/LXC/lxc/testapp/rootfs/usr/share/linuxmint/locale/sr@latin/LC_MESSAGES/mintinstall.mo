��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  
  N  L   Y  I   �  +   �  (     >   E     �  
   �     �     �     �     �  C   �            j   ,     �  	   �  >   �  &   �          "     *     :     F     R  	   [  	   e     o     �  `   �  K   �     ;     C     H  
   P     [  
   v     �  
   �  Q   �     �     �       &     
   2     =     R  &   n  a   �  
   �                    "     (  -   4  0   b     �     �     �     �     �     �     �     �  L   �     K  
   S     ^     d  -   x  '   �  ;   �     
     *     ?     R     W     d  ;   t  O   �                "     .         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-23 12:19+0000
Last-Translator: Saša Marjanović <Unknown>
Language-Team: Serbian <sr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s za preuzimanje, %(localSize)s prostora na disku oslobođeno %(downloadSize)s za preuzimanje, %(localSize)s prostora na disku potrebno %(localSize)s prostora na disku oslobođeno %(localSize)s prostora na disku potrebno %d zadatak pokrenut %d zadatka pokrenuta %d zadataka pokrenuto 3D O programu Dodaci Sve Sve aplikacije Sve radnje su završene Dogodila se greška pri pokušaju dodavanja Fletpek repozitorijuma. Igre na tabli Ne mogu da instaliram Nije moguće obrađivati ovu datoteku kada su radnje u toku.
Molim vas pokušajte ponovo nakon završetka. Ne mogu da uklonim Ćaskanje Kliknite <a href='%s'>ovde</a> kako bi dodali svoju recenziju. Trenutno se radi na sledećim paketima Detalji Crtanje Izbori urednika Obrazovanje Elektronika E-pošta Emulatori Neophodno Razmena datoteka Iz prvog lica Fletpek podrška trenutno nije dostupna. Pokušajte da instalirate fletpek i gir1.2-flatpak-1.0. Flatpak podrška trenutno nije dostupna. Pokušajte da instalirate flatpak. Fontovi Igre Grafika Instaliraj Instaliraj nove aplikacije Instaliran Instalirane aplikacije Instaliram Instaliranje ovog paketa može dovesti do nepopravljive štete na Vašem sistemu. Internet Java Pokreni Ograničite pretragu na trenutnu listu Matematika Multimedijski kodeci Multimedijski kodeci za KDE Nema pronađenih odgovarajućih paketa Nema paketa za prikazati.
Ovo može ukazivati na problem - pokušajte da osvežite keš memoriju. Nedostupno Nije instaliran Kancelarija PHP Paket Fotografija Budite strpljivi. Ovo može potrajati dugo... Upotrebite 'apt-get' za instalaciju ovog paketa. Programiranje Izdavaštvo Python Strategija u realnom vremenu Osveži Osveži listu paketa Ukloni Uklanjam Uklanjanje ovog paketa može uzrokovati nepopravljivu štetu Vašem sistemu. Kritike Skeniranje Nauka Nauka i obrazovanje Traži u opisu paketa (još sporija pretraga) Traži unutar paketa (sporija pretraga) Pretraživanje softverskih repozitorijuma, pričekajte malo Prikaži instalirane aplikacije Simulacija i trkanje Menadžer programa Zvuk Zvuk i video Sistemski alati Fletpek repozitorijum koji pokušavate dodati već postoji. Trenutno postoje aktivne operacije.
Da li ste sigurni da želite da odustanete? Strategija "potez za potez" Video Pregledači Web 