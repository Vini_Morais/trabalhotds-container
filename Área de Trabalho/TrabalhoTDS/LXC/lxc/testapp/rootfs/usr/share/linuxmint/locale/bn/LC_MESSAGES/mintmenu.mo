��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
  "   �     �  *   �  	   �  :   �  0   7  �   h  e   H  �   �  o   0  m   �  �        �  8   �  1   $  J   V  G   �     �     �  +     )   K     u  %   �     �  )   �  :     �   <     �  >   �     $  4   A     v     �  1   �  	   �     �     �  S   �     R  1   k     �  B   �     �     �  �     x   �       7   $  _   \     �  B   �  [     N   k  K   �  G     9   N  B   �  Q   �  u     7   �  F   �       !   (  
   J     U  %   e  B   �     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-05-12 19:14+0000
Last-Translator: Soham Sen <Unknown>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <সেট করা হয়নি> পরিচিতি অতিরিক্ত MATE মেনু সকল সকল অ্যাপ্লিকেশনসমূহ অ্যাপ্লিকেশনসমূহ এই কম্পিউটার থেকে এক্সেস করা যায় এমন সব স্থানীয় ও প্রত্যন্ত ফোল্ডারসমূহ ব্রাউজ করুন সফটওয়্যার ব্রাউজ করুন এবং ইনস্টল করুন বুকমার্ক করা ও স্থানীয় নেটয়ার্কসমূহ ব্রাউজ করুন মুছে ফেলা হয়েছে এমন ফাইলগুলো ব্রাউজ করুন ডেস্কটপে অবস্থিত আইটেমগুলো ব্রাউজ করুন মেনু খোলার এবং বন্ধ করার জন্য একটি নতুন বেগবর্ধক কী সেট করতে ক্লিক করুন।   কম্পিউটার সিস্টেম কনফিগার করুন কন্ট্রোল সেন্টার প্লাগিন শুরু করা সম্ভব হয় নি প্লাগ-ইন লোড করা সম্ভব হয়নি ডেস্কটপ ডেস্কটপ থিম মেন্যু সম্পাদনা ট্রাশ খালি করুন পছন্দসমূহ হোম ফোল্ডার বিভাজক বসান খালি জায়গা বসান '%s' প্যাকেজ ইনস্টল করুন সফটওয়্যার প্যাকেজসমূহ ইনস্টল করুন, মুছুন এবং আপগ্রেড করুন লঞ্চ আমি লগ ইন করলে শুরু করুন লক স্ক্রিন লগ আউট বা সুইচ ইউজার লগআউট মেন্যু মেন্যুর পছন্দসমূহ নাম নামঃ নেটওয়ার্ক আপনারব্যক্তিগত ফোল্ডার খুলুন অপশনসমূহ প্যাকেজ ম্যানেজার পথ একটি বেগবর্ধক বাছাই করুন প্লেসেস পছন্দসমূহ বিদ্যমান কিবাইন্ডিং মুছে ফেলতে ব্যাকস্পেস চাপুন. Escape চাপুন অথবা অপারেশন বাতিল আবার ক্লিক করুন।   বন্ধ প্লাগ-ইন রি-লোড করুন শেষ ক্যাটাগরি বা অনুসন্ধান মনে রেখো মুছুন পছন্দের তালিকা হতে মুছুন আনলক করার জন্য পাসওয়ার্ড প্রয়োজন ইন্সটলের জন্য প্যাকেজ খুঁজুন একটি ফোল্ডার নির্বাচন করুন সকল অ্যাপ্লিকেশন প্রদর্শন বাটন আইকন প্রদর্শন কর বিভাগের আইকন প্রদর্শন কর আমার পছন্দের তালিকায় প্রদর্শন শাটডাউন, রিস্টার্ট, সাসপেন্ড অথবা হাইবারনেট সফটওয়্যার ম্যানেজার সোয়াপ নাম এবং জেনেরিক নাম সিস্টেম টার্মিন্যাল থিম: ট্রাশ আনইনস্টল করুন কমান্ড লাইন ব্যবহার করুন 