��    '      T  5   �      `  ,   a  w   �               ,     3     E     L     R     b  �   o                ;  ^   Y     �     �     �     �     �               *  	   ;     E     W  ,   \     �     �     �     �     �     �     �     �       
           �  $  9   �  �   "	     �	     �	     
  2   &
     Y
     k
  %   t
     �
    �
     �  /   �  )      �   *  &   �  *   �          "     6     =     Y     u     �  7   �     �  f   �  
   H     S  $   Z  0     +   �     �     �     
  &        A     Y                                                   &                  $                                         !      #       %       	                "   '                  
          %d language installed %d languages installed <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Cancel Close Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese XIM Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-07-19 05:57+0000
Last-Translator: Murat Käribay <d2vsd1@mail.ru>
Language-Team: Kazakh <kk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %d тіл орнатылды %d тіл орнатылды <b><small>Ескерту: Тіл бумаларын орнату немесе жаңартылуы қосымша тілдерді орнатылуына әкелу мүмкін</small></b> Жаңа тілді қосу Жаңа тілді қосу Қосу... Барлық жүйе үшін іске асыру Бас тарту Жабу Толығымен орнатылды Енгізу әдісі Бұл енгізу әдісі пернетақтадағы жоқ таңбаларды енгізуге мүмкіндік береді. Олар пайдалы, егер сіз қытай, жапон, корей, тай, вьетнам тілінде терсеңіз... Орнату Тілді орнату немесе өшіру Тілдерді Орнату / Жою... Жетіспейтін тіл бумалары, аударма файлдар, таңдалған тіл үшін сөздіктер орнату Тіл бумаларды орнату Таңдалған тілді орнату Жапон тілі Кәріс тілі Тіл Тіл баптаулары Тіл баптаулары Тілдік қолдау Тілдер Орналасқан жері таңдалған жоқ Ештеңе Сандар, ақша бірліктері, мекен-жайлар, өлшемдер жүйесі... Аймақ Жою Таңдалған тілді жою Қытай тілі (жеңілдетілген) Кейбір тіл бумалары жоқ Жүйенің тілі Телугу тілі Тай тілі Қытай тілі (дәстүрлі) Вьетнам тілі XIM 