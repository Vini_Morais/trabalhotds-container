��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  S   �  s   G  %   �  "   �       4  !  D   V  "   �  B   �  �                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-07-21 05:13+0000
Last-Translator: Abhinav Jha <Unknown>
Language-Team: Maithili <mai@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s कोनो मान्य डोमेनक नाम नहि अछि। चुनल गेल डोमेन(के नाम) तक  पहुँच अवरोधित करू। अवरोधित डोमेन डोमेन अवरोधक डोमेनक नाम डोमेनक नाम कोनो अक्षर या अंकक सङ्ग प्रारंभ व समाप्त होयबाक चाहि, अऔर अहिमे मात्र अक्षर, अंक, बिंदु व हायफ़न भ' सकैत अछि। उदाहरण: हमर.नंबर1डोमेन.कोम अमान्य डोमेन अभिभावक(द्वारा) नियंत्रण कृपया ओहि डोमेनक नाम लिखु जेकरा आहाँ अवरोधित कर' चाहैत छियै 