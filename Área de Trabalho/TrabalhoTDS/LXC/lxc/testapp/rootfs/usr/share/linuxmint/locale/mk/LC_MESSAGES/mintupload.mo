��    +      t  ;   �      �  >   �     �  6        =     C     E     K  #   i     �     �  $   $  *   I  "   t     �     �     �  !   �     �     �     �     �  u   �  	   `     j     p  7   v     �  	   �  !   �  (   �  
             "     '     ;     J     \     l  )   �     �     �  #   �  �  �  j   �	     )
  y   B
     �
     �
     �
  `   �
  U   4  ;   �  �   �  E   �  g   �  k   e  :   �            G        [     ^     b     e  �   i          $     2  X   >     �     �  )   �  Q   �     A     \     d  !   i  ;   �  >   �  9     3   @  i   t     �     �  A   �                      !            
                 $       &          )   #   	      "       %                                                           +             '   (                 *                  %(1)s is not set in the config file found under %(2)s or %(3)s %s Properties <b>Please enter a name for the new upload service:</b> About B Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Service name: Services: This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Uploading the file... Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2010-04-15 09:53+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Macedonian <mk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s не е поставено во датотеката со поставки под %(2)s или %(3)s Својства на %s <b>Ве молам внесете го името на сервисот за поставување датотеки:</b> За Б Затвори Не може да се добие големината на слободниот простор Не може да се зачуваат промените во поставките Дефинирај сервис за поставување Именик за поставување на датотеката. <TIMESTAMP> се заменува со моменталниот временски жиг во формат кој претходно е одбран. Стандардно: Влечи и пушти тука за поставување на %s Датотеката е поголема од слободниот простор на сервисот Датотеката е поголема од дозволениот максимум на сервисот Датотеката е поставена успешно. GB GiB Име на сервер или IP адреса, стандардно:  KB KiB MB MiB Лозинка, стандардно: SCP конекција без лозинка, null-string FTP конекција, ~/.ssh клучеви за SFTP конекција Лозинка: Патека: Порта: Одалечена порта, стандардно 21 за FTP, 22 за SFTP или SCP Име на сервисот: Сервиси: Сервисот бара лозинка. Формат на временскиот жиг (strftime). Стандардно: Временски жиг: Тип: URL: Непознат сервис: %s Менаџер за поставување датотеки Менаџер за поставување датотеки... Сервис за поставување датотеки Поставување на датотеката... Корисничко име, стандардно вашето локално корисничко име _Датотека _Помош конекцијата е успешно воспоставена 