��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  #   �  '         H     \  
   m  �   x          "     1  5   D                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-20 20:26+0000
Last-Translator: kristian <kristianm24@gmail.com>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s är inte ett giltigt domännamn. Blockera åtkomst till valda domännamn Blockerade domäner Domänblockerare Domännamn Domännamn måste börja och sluta med en bokstav eller en siffra, och kan endast bestå av bokstäver, siffror, punkter och bindestreck. Exempel: min.favoritdomän1.com Ogiltig Domän Föräldrakontroll Var god och ange namnet på domänen du vill blockera 