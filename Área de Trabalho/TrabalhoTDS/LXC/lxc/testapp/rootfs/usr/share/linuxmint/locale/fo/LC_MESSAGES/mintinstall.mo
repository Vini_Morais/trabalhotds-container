��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  D   ,  G   q  &   �  &   �     	     
	     	     	     +	     1	     A	     M	     Y	     e	     n	  
   {	     �	  	   �	     �	  !   �	     �	     �	     �	  
   �	     �	  
   �	     �	     
     
  
   '
     2
     :
     F
  C   ]
  L   �
     �
     	     "     2     ?     ^     d            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2011-07-15 14:25+0000
Last-Translator: Gunleif Joensen <Unknown>
Language-Team: Faroese <fo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s at taka niður, %(localSize)s av diskaplássi fría %(downloadSize)s at taka niður, %(localSize)s av diskaplássi tørvað %(localSize)s av diskaplássi er fría %(localSize)s av diskaplássi tørvað 3D Um forritið Tilhoyr Brettispøl Kjatt Nærri greining Tekniforrit Útbúgving Teldupostur Hermarar Fílu deilan Stavasnið Spøl Grafikkur Legg inn Legg inn nýggjar nýtsluskipanir Innlagt Alnet Ikki lagt inn Skrivstova Pakki Myndatøka Forritan Telduprentgerð Sonn-tíðar atføri Tak burtur Ummæli Ljóslestur Vísind og útbúgving Leita ígjøgnum pakkar, ið nýta lýsing (enn spakuligari leitan) Leita ígjøgnum pakkum, ið nýta stutta frágreiðing (spakuligari leitan) Samlátarar og kappkoyring Ritbúnaðar fyristiting Ljóð og video Kervisamboð Túr-grundað krígslistaspøl Kagar Net 