��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  <  N  t   �  p      H   q  D   �  w   �     w     {     �     �     �  *   �  w   �     `  ,   }  �   �  !   m     �  ^   �  @   �     >     U     b     ~     �     �     �     �     �     �  �     �   �  
   0     ;     D     S  &   `     �  +   �     �  �   �     a     r     �  F   �     �  )   �  2     0   J  �   {     %     ?     W     n  
   {     �  D   �  M   �     0     I     ^  5   r     �  (   �     �     �  �   �     ~     �  
   �  "   �  N   �  U     ]   q  :   �      
  !   +     M     V     o  h   �  �   �  @   �      �      �      �          4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-07 18:51+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <(nothing)>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: sr
 %(downloadSize)s за преузимање, %(localSize)s простора на диску је ослобођено %(downloadSize)s за преузимање, %(localSize)s простора на диску је потребно %(localSize)s простора на диску је ослобођено %(localSize)s простора на диску је потребно %d задатак је покренут %d задаци су покренути %d задатак је покренут 3Д О програму Прибори Сви Сви програми Све операције завршене Дошло је до грешке приликом покушаја додавања Флатпак спремишта. Игрице на табли Није могуће инсталирати Не може се обрадити ова датотека док постоје активне операције.
Покушајте поново касније након завршетка. Не могу да уклоним Ћаскање Кликните <a href='%s'>овде</a> да додате ваш лични преглед. Тренутно ради на следећим пакетима Појединости Цртање Избор удедника Образовање Електроника Е-пошта Опонашање Основе Дељење датотека Прво лице Флатпак подршка тренутно није доступна. Покушајте да инсталирате флатпак и gir1.2-flatpak-1.0. Флатпак подршка тренутно није доступна. Покушајте да инсталирате флатпак. Слова Игре Графика Угради Угради нове програме Уграђен Инсталиране Апликације Инсталирам Инсталирање овог пакета може узроковати непоправљиву штету вашем систему. Интернет Јава (Java) Покрени Ограничите претрагу на тренутну листу Математика Мултимедијални кодеци Мултимедијални кодеци за KDE Нема одговарајућих пакета Нема пакета за приказати.
Ово може указивати на проблем - покушајте да освежите кеш меморију. Није доступан Није уграђен Канцеларија ПХП (PHP) Пакет Фотографија Будите стрпљиви. Ово може потрајати... Употребите „apt-get“ за уградњу овог пакета. Програмирање Издаваштво Питон (Python) Стратегије у реалном времену Освежи Освежите листу пакета Уклони Уклањам Уклањање овог пакета може узроковати непоправљиву штету вашем систему. Утисци Прегледам Наука Наука и образовање Тражи у опису пакета (још спорија претрага) Тражи унутар сажетка пакета (спорија претрага) Претражујем софтверска спремишта, мало причекајте Прикажи инсталиране апликације Симулације и трке Управник програма Звук Звук и снимци Системске алатке Флатпак спремиште које покушавате да додате већ постоји. Тренутно постоје активне операције.
Да ли сте сигурни да желите да затворите? Вештина ратовања „потез за потез“ Снимци Прегледачи Веб 