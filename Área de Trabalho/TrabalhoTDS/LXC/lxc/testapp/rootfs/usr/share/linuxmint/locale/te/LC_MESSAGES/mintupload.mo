��    <      �  S   �      (  >   )  =   h  3   �  !   �  K   �     H  6   V     �     �     �     �     �     �     �  #   �            "   �  $   �  *   �  "        1     M     P     T  !   Z     |          �     �  u   �  	    	     
	     	  7   	     N	     d	  	   r	  7   |	  -   �	  )   �	  !   
  (   .
  
   W
     b
     h
     m
     �
     �
     �
     �
  )   �
     �
          *  )   0     Z     `  #   f  �  �  �   l  �   �  }   p  E   �  �   4     �  �   �     �     �     �  A   �  F   	     P  t   i  h   �  J   G  J  �  i   �  q   G  �   �  R   W  T   �     �            j   $     �     �     �     �    �     �  
   �     �  �   �  +   }     �     �     �  u   R  [   �  D   $  V   i     �  
   �     �  #   �  4     7   C  (   {  D   �  ^   �  R   H  @   �     �  �   �     t     �  e   �                     ;      .          /                              9   &      0       !              (            )                +                    1      6   #          2   
   3         "   8              7       4       <   '                :   $              *   -   %   	   5              ,    %(1)s is not set in the config file found under %(2)s or %(3)s %(percentage)s of %(number)d files - Uploading to %(service)s %(percentage)s of 1 file - Uploading to %(service)s %(size_so_far)s of %(total_size)s %(size_so_far)s of %(total_size)s - %(time_remaining)s left (%(speed)s/sec) %s Properties <b>Please enter a name for the new upload service:</b> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Directory to upload to. <TIMESTAMP> is replaced with the current timestamp, following the timestamp format given. By default: . Do you want to cancel this upload? Drag &amp; Drop here to upload to %s File larger than service's available space File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password, by default: password-less SCP connection, null-string FTP connection, ~/.ssh keys used for SFTP connections Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Run in the background Service name: Services: Successfully uploaded %(number)d files to '%(service)s' Successfully uploaded 1 file to '%(service)s' The upload to '%(service)s' was cancelled This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Upload to '%s' failed:  Uploading %(number)d files to %(service)s Uploading 1 file to %(service)s Uploading the file... User: Username, defaults to your local username _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-04-16 09:28+0000
Last-Translator: Praveen Illa <mail2ipn@gmail.com>
Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(1)s అనేది స్వరూపణ ఫైలు %(2)s లేదా %(3)s లలో అమర్చబడిలేదు %(percentage)s అయినది %(number)d ఫైళ్ళలో - %(service)s కు ఎక్కిస్తున్నది %(percentage)s అయినది 1 ఫైల్ నందు - %(service)s కు ఎక్కిస్తున్నది %(size_so_far)s అయినది %(total_size)s కి గాను %(size_so_far)s అయినది %(total_size)s కి గాను- %(time_remaining)s మిగిలివున్నాయి (%(speed)s/sec) %s లక్షణాలు <b>దయచేసి కొత్త ఎక్కింపు సేవకు ఒక కొత్త పేరును ప్రవేశపెట్టండి:</b> గురించి బై రద్దుచేయి ఎక్కింపును రద్దుచేయాలా? అనుసంధానాన్ని పరీక్షించు మూసివేయి అందుబాటులోవున్న స్థలాన్ని పొందలేకపోతుంది స్వరూపణం మార్పును భద్రపరుచలేకపోయింది ఎక్కింపు సేవలను నిర్వచించు ఎక్కించవలసిన సంచయం. <TIMESTAMP> ప్రస్తుత కాలముద్రను ప్రతిస్థాపిస్తుంది, క్రిందిపేర్కొన్న కాలముద్ర ఆకారం అప్రమేయంగా ఇవ్వబడింది: ఈ ఎక్కింపును రద్దుచేయాలనుకుంటున్నారా? %s కు ఎక్కించుటకు ఫైలును లాగి &amp; ఇక్కడ వదులు సేవ నందు అందుబాటులోవున్న స్థలం కంటే ఫైల్ పెద్దదిగా ఉన్నది సేవ యొక్క అత్యధిక ఫైల్ పెద్దది ఫైల్ విజయవంతంగా ఎక్కించబడింది. గిబై GiB హోస్టు: హోస్టుపేరు లేదా ఐపీ చిరునామా, అప్రమేయం:  కిబై KiB మెబై MiB సంకేతపదం, అప్రమేయముగా: సంకేతపదం లేని SCP అనుసంధానం, null-string FTP అనుసంధానం, ~/.ssh కీలు SFTP అనుసంధానాల కోసం వాడబడతాయి సంకేతపదం: పథం: ద్వారం: సుదూర మార్గం, FTP కొరకు 21, SFTP మరియు SCP కొరకు 22 అప్రమేయం నేపథ్యంలో నడుపు సేవ పేరు: సేవలు: %(number)d ఫైళ్ళను '%(service)s' కు విజయవంతంగా ఎక్కించబడ్డాయి 1 ఫైల్ '%(service)s'కు విజయవంతంగా పైకిఎక్కించబడింది '%(service)s' కు ఎక్కించుట రద్దుచేయబడింది ఈ సేవకు ఒక సంకేతపదం అవసరం. కాలముద్ర ఫార్మేట్ (strftime) అప్రమేయం: కాలముద్ర: రకం: URL: తెలియని సేవ: %s ఎక్కింపు నిర్వాహకం ఎక్కింపు నిర్వాహకం... ఎక్కింపు సేవలు '%s'కు ఎక్కించుట విఫలమైంది:  %(number)d ఫైళ్ళను %(service)s కు ఎక్కిస్తున్నది 1 ఫైలును %(service)s కు ఎక్కిస్తున్నది ఫైలును ఎక్కిస్తున్నది... వాడుకరి: వాడుకరిపేరు, మీ స్థానిక వాడుకరిపేరుకు అప్రమేయం ఫైల్ (_F) సహాయం (_H) అనుసంధానం విజయవంతంగా స్థాపించబడింది 