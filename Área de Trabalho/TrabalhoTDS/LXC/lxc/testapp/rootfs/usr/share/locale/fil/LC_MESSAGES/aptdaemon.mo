��          t      �                 /     J     _     |  A   �  6   �  <     2   R  ;   �  �  �  "   S     v     �  (   �  /   �  X   
  K   c  R   �  A     \   D                	                                     
       Change software configuration Change software repository Install package file List keys of trusted vendors Remove downloaded package files To change software repository settings, you need to authenticate. To change software settings, you need to authenticate. To clean downloaded package files, you need to authenticate. To install this package, you need to authenticate. To view the list of trusted keys, you need to authenticate. Project-Id-Version: aptdaemon
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-05-02 07:53+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Filipino <fil@li.org>
Language: fil
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2013-03-09 23:28+0000
X-Generator: Launchpad (build 16523)
 Baguhin ang software configuration Baguhin ang software repository I-install ang package file Ilista ang mga pinagkakatiwalaang vendor Alisin ang mga di-nownload na mga package file. Upang baguhin ang mga setting ng repositoryo ng software, kailangan mo itong patotohanan Upang baguhin ang mga settiing ng software, kailangan mo itong patotohanan. Upang linisin ang mga di-nownload na package file, kailangan mo itong patotohanan. Upang i-install ang paketeng ito, kailangan mo itong patotohanan. Upang matingnan ang listahan ng mga pinagkakatiwalaang keys, kailangan mo itong patotohanan. 