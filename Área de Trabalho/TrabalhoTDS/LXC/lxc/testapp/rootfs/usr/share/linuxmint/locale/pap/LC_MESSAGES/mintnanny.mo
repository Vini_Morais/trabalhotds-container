��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  $        8     J     ^  e   r  "   �     �       6   !                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-23 04:52+0000
Last-Translator: Frank <f.boye@hotmail.nl>
Language-Team: Papiamento <pap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s no ta un domein valido Blòkia akseso na e domeinnan skohí Domeinnan blòkia Blòkiado di domein Nòmber di e domein Nòmber di domeinnan mester kuminsa i kaba ku un lèter of number. E nòmber por kontene solamenten l Por ehèmpel: my.number1domain.com Domein inválido Kontròl pa mayornan Por fabor yena e nòmber di domein ku lo bo ke blòkia 