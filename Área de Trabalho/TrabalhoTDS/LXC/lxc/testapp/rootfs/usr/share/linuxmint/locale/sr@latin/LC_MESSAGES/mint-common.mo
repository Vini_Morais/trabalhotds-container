��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  )   p  '   �     �     �     �  
     
        "     A     ^  !   e  j   �  	   �                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-22 21:14+0000
Last-Translator: Saša Marjanović <Unknown>
Language-Team: Serbian Latin <sr@latin@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Biće zauzeto %s više prostora na disku. Biće oslobođeno %s prostora na disku. Biće preuzeto ukupno %s. Potrebne su dodatne izmene Došlo je do greške Fletpekovi Instaliraj Paketi koji će biti uklonjeni Pogledaj spisak izmena niže Ukloni Sledeći paketi bi će uklonjeni: Ova stavka menija nije povezana ni sa jednim paketom. Da li i pored toga želite da je uklonite iz menija? Nadogradi 