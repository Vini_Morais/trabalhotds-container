��    )      d  ;   �      �  
   �     �     �     �     �     �     �     �     �       -        ?     Z  
   _     j  
   p  	   {     �  
   �     �     �     �     �     �  0   �               +     1     B  	   T     ^     g     �     �     �     �     �     �     �  �  �     �  D   �  ;   �          5     Q     a  4   w  :   �     �  �   �  Y   �	  %   �	     
     5
  7   T
     �
     �
  (   �
  %   �
  $   
  O   /       !   �  �   �  b   x  h   �     D  8   Z  J   �     �  -   �  4   ,     a  �   }  G     %   N     t     �  n  �               &               '                                          !               #              
                      "      (   $               )              %                    	                  Advantages Buttons labels: Buttons layout: Compiz Compton Computer Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Metacity Mounted Volumes Network Openbox Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Welcome to Desktop Settings. Window Manager Windows Xfwm4 translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-07-30 06:19+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 நன்மைகள் பொத்தான்கள் அடையாளங்கள்: பொத்தான்கள் அமைப்பு: காம்பிஸ் காம்ப்டன் கணிணி பணிமேடை பணிமேடை அமைப்புகள் பணிமேடைச் சின்னங்கள் தீமைகள் இழுக்கும்போது சாளரம் உள்ளடக்கத்தை காண்பிக்க வேண்டாம் பணிமேடை அமைப்புகளை சீரமைக்கவும் துவக்க இல்லம் படவுரு அளவு சின்னங்கள் படவுருக்கள் மட்டும் இடைமுகம் பெரியது லினக்ஸ் மின்ட் மேக் நடை (இடது) மெட்டாசிட்டி பிணைக்கப்பட்ட வட்டுத்தொகுதி பிணையம் ஓபென்பாக்ஸ் நீங்கள் டெஸ்க்டாப் பார்க்க விரும்பும் உருப்படிகளை தேர்ந்தெடுக்கவும்: பொத்தான்களில் படவுருக்களைக் காட்டு மெனுக்களில் சின்னங்களையும் காட்டவும் சிறியது உருப்படிகள் கீழே உரை உருப்படிகள் பக்கத்தில் உரை உரை மட்டும் கருவிப்பட்டைகள் பாரம்பரிய நடை (வலது) அகற்றிடம் தலைப்பு பட்டியில் கணினி எழுத்துரு பயன்படுத்தவும் பணிமேடை அமைவுகளுக்கு வருக சாளர நிர்வாகி சாளரங்கள் Xfwm4 Launchpad Contributions:
  Kamala Kannan https://launchpad.net/~kamalakannan
  Ramesh https://launchpad.net/~rame20002007
  Satheesh https://launchpad.net/~satheesh-rsk
  Senthil https://launchpad.net/~senthilkumar
  mano-மனோ https://launchpad.net/~manoj-neyveli
  சதீஸ்குமார் வரதராசு https://launchpad.net/~vsathishkumarmca 