��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  $   �  !        7     H  
   W  t   b  !   �     �     
  2                          
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-11-15 16:22+0000
Last-Translator: Johannes <Unknown>
Language-Team: Afrikaans <af@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s is nie 'n geldige domeinnaam nie. Blok toegang na sekere domeinname Geblokte domeine Domein Blokker Domeinnaam Domeinname moet begin en eindig met 'n letter of 'n syfer en kan slegs letters, syfers, punte en koppeltekens bevat. Byvoorbeeld: my.nommer1domein.com Ongeldige domein Ouer Beheer Tik asseblief die domeinnaam wat u wil blokkeer in 