��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  -     &   A     h     �     �  F   �     �     
  )        <  #   X     |  !   �  [   �                                            	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-14 06:42+0000
Last-Translator: Martin Srebotnjak <miles@filmsi.net>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Dodatno bo porabljenega %s prostora na disku. Sproščenega bo %s prostora na disku. Skupno bo preneseno %s. Zahtevane so dodatne spremembe Prišlo je do napake Paketa %s ni mogoče odstraniti, saj so drugi paketi odvisni od njega. Paketi flatpak Namesti Paket %s je potreben za naslednje pakete: Paketi, ki bodo odstranjeni Oglejte si spodnji seznam sprememb. Odstrani Odstranjeni bodo sledeči paketi: Ta predmet menija ni povezan z nobenim paketom. Ali ga vseeno želite odstraniti iz menija? Nadgradi 