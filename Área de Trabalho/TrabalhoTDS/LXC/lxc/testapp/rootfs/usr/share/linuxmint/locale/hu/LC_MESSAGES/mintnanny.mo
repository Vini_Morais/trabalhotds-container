��          t      �              %   0     V     f     u  q   �     �             -   1  ~  _  !   �  1         2     H     Z  �   i     �          3  7   H                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: mintnanny
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-01-22 22:33+0000
Last-Translator: KAMI <kami911@gmail.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: hu
 %s nem érvényes tartománynév. Nem kívánt weboldalak elérésének blokkolása Blokkolt tartományok Weboldalblokkoló Tartománynév A tartományneveknek betűvel vagy számmal kell kezdődniük, és kizárólag betűket, számokat, pontokat és mínusz jeleket tartalmazhatnak. Példa: www.tartomanyom.hu Érvénytelen tartománynév Szülői felügyelet Adja meg a blokkolni kívánt weboldal tartománynevét 