��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  !     $   2     W     j     z  x   �                -  $   :                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-21 08:37+0000
Last-Translator: tomoe_musashi <hkg.musashi@gmail.com>
Language-Team: Chinese (Hong Kong) <zh_HK@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s 不是有效的網域名稱。 封鎖對已選網域名稱的存取 已封鎖的網域 網域封鎖器 網域名稱 網域名稱必須以字母或位數作開始和結束，並只能含有字母、位數、點號 (.) 和連字號 (-)。 範例: my.number1domain.com 無效的網域 家長控制 請輸入你要封鎖的網域名稱 