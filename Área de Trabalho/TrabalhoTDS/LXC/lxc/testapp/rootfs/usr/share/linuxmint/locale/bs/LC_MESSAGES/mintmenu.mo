��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
     �  
   �     �     �     �  
   �  N   �  '   7  1   _     �     �  P   �          $     9  #   M     q     �     �  
   �     �     �     �     �     �     �  8        J     R     j  $   {     �     �     �     �     �     �     �     �     �               !     (  L   /  ?   |     �     �  ,   �            $   #     H     g     |     �     �     �  ;   �          6     T     [     d     j     q     ~     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-07-11 14:08+0000
Last-Translator: Almir Zulic <zalmir@yahoo.com>
Language-Team: Bosnian <bs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <nije postavljeno> O programu Napredni MATE meni Sve Sve aplikacije Aplikacije Pretraži sve lokalne i udaljene diskove i direktorije dostupne ovom računaru Pretraži i instaliraj dostupni softver Pretraži zabilježene i lokalne mrežne lokacije Pretraži obrisane datoteke Pretraži stavke na desktopu Kliknite da postavite novu ubrzivačku tipku za otvaranje i zatvaranje menija.   Računar Podesite vaš sistem Upravljački centar Nije moguće inicijalizirati plugin Nije moguće učitati plugin: Desktop Desktop tema Uredi meni Isprazni smeće Omiljeni Početni direktorij Umetni separator Umetni razmak Instaliraj paket '%s' Instalacija, nadogradnja i uklanjanje programskih paketa Pokreni Pokreni kad se prijavim Zaključaj ekran Odjavite se ili zamijenite korisnika Odjava Meni Opcije menija Naziv Naziv: Mreža Otvorite lični direktorij Opcije Upravljanje paketima Putanja Izaberite ubrzivač Mjesta Opcije Pritisnite Backspace da obrišete postojeće povezivanje tipki na tastaturi. Pritisnite Escape ili kliknite ponovo da prekinete operaciju.   Zatvori Ponovo učitaj plugine Zapamti zadnju kategoriju ili pretraživanje Ukloni Ukloni iz omiljenih Potrebna je šifra za otključavanje Pretraga paketa za instalaciju Odaberite direktorij Prikaži sve aplikacije Prikaži ikone za dugmad Pokaži ikone kategorije Prikaži u mojim omiljenim Gašenje, ponovno pokretanje, suspendovanje ili hibernacija Upravljanje aplikacijama Zamijeni ime i generičko ime Sistem Terminal Tema: Smeće Deinstaliraj Koristite komandnu liniju 