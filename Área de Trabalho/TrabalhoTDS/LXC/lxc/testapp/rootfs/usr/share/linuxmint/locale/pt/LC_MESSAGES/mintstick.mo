��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  #   �  -   �     �               0  	   >     H  #   ^  #   �  &   �  L   �       �  1     �     �     �               "     8     O     V     k     w     �  -   �  %   �     �  ,   �                     ?     [  '   m  /   �     �     �     �                     =     P     a     j  ,   m  C   �  /   �  ,     %   ;  W   a  ,   �  :   �  !   !  1   C  C   u  <   �  A   �  -   8     f  @   �     �     �     �     �          '     ?  	   Z  1   d     �     �  6   �     �     �     �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-02 16:00+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: pt
 Ocorreu um erro ao copiar a imagem. Ocorreu um erro a criar uma partição em %s. Ocorreu um erro Ocorreu um erro. Erro de autenticação. A calcular... Verificar Soma de verificação Ficheiros de somas de verificação Soma de verificação incompatível Escolha um nome para o dispositivo USB Transfira a imagem ISO novamente. A sua soma de verificação não coincide. Tudo parece estar bem! FAT32 
  + Compatível com todos os sistemas operativos.
  - Não é possível lidar com ficheiros maiores do que 4 GB.

exFAT
  + Compatível com quase todos os sistemas operativos.
  + Pode lidar com ficheiros maiores do que 4 GB.
  -  Não é tão compatível quanto o FAT32.

NTFS 
  + Compatível com o Windows.
  -  Não é compatível com Mac nem com a maioria dos dispositivos.
  -  Problemas ocasionais de compatibilidade em Linux (NTFS é patenteado e foi conseguido com engenharia reversa).

EXT4 
  + Moderno, estável, rápido, com 'journal'.
  + Suporta permissões de ficheiros em Linux.
  -  Não é compatível com o Windows, Mac nem com a maioria dos dispositivos.
 Sistema de ficheiros: Formatar Formatar disco USB GB Assinaturas GPG Ficheiro GPG assinado Ficheiro GPG assinado: Voltar Verificação da ISO Imagem ISO: Imagens ISO ISO: Se confia na assinatura, pode confiar na ISO. Falha na verificação de integridade KB Chave não encontrada no servidor de chaves. Ficheiros locais MB Criar um disco USB de arranque Criar disco USB de arranque Mais informação Não foi encontrado nenhum ID do volume Não há espaço suficiente no dispositivo USB. Soma SHA256 Ficheiro de somas SHA256 Ficheiro de somas SHA256: somaSHA256: Escolher imagem Selecione um dispositivo USB Escolha uma imagem Assinado por: %s Tamanho: TB A soma SHA256 da imagem ISO está incorreta. O ficheiro de somas SHA256 não contém somas para esta imagem ISO. O ficheiro de somas SHA256 não está assinado. O dispositivo USB foi formatado com sucesso. A soma de verificação está correta A soma de verificação está correta, mas a autenticidade da soma não foi verificada. Não foi possível verificar o ficheiro gpg. Não possível transferir o ficheiro gpg. Verifique o URL. A imagem foi gravada com sucesso. Não foi possível verificar o ficheiro de somas. Não foi possível transferir o ficheiro de somas. Verifique o URL. Esta imagem ISO é verificada por uma assinatura confiável. Esta imagem ISO é verificada por uma assinatura não confiável. Esta ISO parece-se com uma imagem do Windows. Esta é uma imagem ISO oficial. Irá destruir todos os dados no dispositivo USB. Quer continuar? URLs Gravador de imagens em USB Dispositivo USB Formatador de dispositivos USB Dispositivo USB: Assinatura desconhecida Assinatura não confiável Verificar Verifique a autenticidade e integridade da imagem Nome do volume: Volume: As imagens do Windows requerem processamento especial. Gravar bytes desconhecido 