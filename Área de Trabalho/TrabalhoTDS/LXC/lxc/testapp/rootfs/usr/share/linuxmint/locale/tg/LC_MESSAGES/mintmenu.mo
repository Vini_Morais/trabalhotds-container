��    C      4  Y   L      �  	   �     �     �     �     �  K   �  %   /  -   U     �  "   �  F   �               !     0     K     a     i  	   w     �  	   �     �     �     �     �  -   �                     ,     C     J     O     `     e     k     s     �     �     �     �     �     �  1   �  6   	     :	     ?	  $   N	     s	     z	     �	     �	     �	     �	     �	     
     
  '   +
     S
     d
     
     �
     �
     �
  	   �
     �
  �  �
     �      �     �     �     �  �   �  M   �  f   �  ;   L  \   �  �   �     �  !   �  !   �      �  !   �          +  $   J  "   o     �     �  .   �  $   �  )     U   C     �  3   �     �  )   �          .     7     W     ^     f  ,   s     �     �  
   �  ,   �            �   '  �   �     -  2   A  T   t     �  1   �  A     @   S  ,   �  7   �  (   �  C   "  >   f  Q   �     �  2        J     Y     j  
   x      �  *   �     )      -   &       (          ?   =                           %         .                0             !   *       ,       B   :   2          +              #                >   $   <   3      
   '          @              C              9       7      	   A   1   ;      5      8                  "   6         4   /        <not set> About All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2014-01-15 20:26+0000
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: tg
 <танзимнашуда> Дар бораи барнома Ҳама Ҳамаи барномаҳо Барномаҳо Тамошо кардани ҳамаи дискҳо ва ҷузвдонҳои маҳаллӣ ва дурдасти дастрас аз ин компютер Тамошо ва насб кардани нармафзори дастрас Тамошо кардани маконҳои шабакавии маҳаллӣ ва иловашуда Тамошо кардани файлҳои нестшуда Тамошо кардани объектҳои дар мизи корӣ ҷойгиршуда Барои танзим кардани тугмаи миёнбур барои кушодан ва пӯшидани меню, ин ҷо зер кунед.   Компютер Танзимоти система Маркази идоракунӣ Плагин оғоз наёфт Плагин оғоз наёфт: Мизи корӣ Мавзӯи мизи корӣ Таҳрир кардани меню Холӣ кардани сабад Баргузидаҳо Ҷузвдони асосӣ Дарҷ кардани ҷудокунанда Дарҷ кардани фосила Насб кардани бастаи '%s' Насб, тоза ва такмил додани бастаҳои нармафзор Оғози кор Оғози кор ҳангоми воридшавӣ Экрани қулф Баромад ё ивази корбар Баромад Меню Хусусиятҳои меню Ном Ном: Шабака Кушодани ҷузвдони шахсӣ Имконот Мудири бастаҳо Масир Интихоби тугмаи миёнбур Маконҳо Хусусиятҳо Барои тоза кардани тугмабандии мавҷудбуда, тугмаи "Backspace"-ро пахш кунед. Тугмаи "Esc"-ро пахш кунед ё барои бекор кардани ин амал аз нав зер кунед.   Анҷоми кор Аз нав бор кардани плагинҳо Категория ё ҷустуҷӯи охиринро дар хотир доред Тоза кардан Тоза кардан аз баргузидаҳо Барои кушодани қулф парол лозим аст Ҷустуҷӯи бастаҳо барои насб кардан Интихоб кардани ҷузвдон Намоиш додани ҳамаи барномаҳо Намоиши нишонаи тугма Намоиш додани нишонаҳои категорияҳо Намоиш додан дар баргузидаҳои ман Анҷоми кор, бозоғозӣ, таваққуф ё гибернатсия Мудири нармафзор Номи мубодила ва номи умумӣ Система Терминал Мавзӯъ: Сабад Лағв кардани насб Истифодаи сатри фармон 