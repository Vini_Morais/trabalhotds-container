��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  ?   d  A   �      �  %        -  "   J     m     p  	   s     }     �     �     �  D   �  	   �            d   !     �     �  ?   �  .   �     
          !     1     I  
   V     a  
   h     s  	   �     �     �  q   �  [        w     �     �     �  
   �  
   �     �     �     �        K        X     a     f  !   l  	   �     �     �     �     �  S   �  !   9     [     m     ~     �     �     �     �  :   �  4   �          '     3     :     K     T     p     y       G   �     �     �  	   �     �  5     ,   <  $   i     �     �     �     �  (   �                    %  B   7  K   z     �     �  
   �       M        e     w     �  
   �     �  $   �     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 18:16+0000
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s til hentning, %(localSize)s diskplads frigjort %(downloadSize)s til hentning, %(localSize)s diskplads nødvendig %(localSize)s diskplads frigjort %(localSize)s af diskplads nødvendig %d anmeldelse %d anmeldelser %d opgave kører %d opgaver kører 3D Om Tilbehør Tilføj Alle Alle programmer Alle handlinger færdiggjorte Der opstod en fejl ved forsøg på at tilføje flatpak-pakkearkivet. Brætspil Gren: Kan ikke installere Kan ikke behandle filen, mens der er aktive handlinger.
Prøv venligst igen, når de er gennemført. Kan ikke fjerne Chat Klik <a href='%s'>her</a> for at tilføje din egen bedømmelse. Arbejder i øjeblikket på de følgende pakker Detaljer Dokumentation Tegneprogrammer Redaktørernes udvalgte Undervisning Elektronik E-mail Emulatorer Det væsentlige Fildeling Første person Flatpak (%s) Understøttelse af Flatpak er i øjeblikket ikke tilgængelig. Prøv at installere flatpak og gir1.2-flatpak-1.0. Understøttelse af Flatpak er i øjeblikket ikke tilgængelig. Prøv at installere flatpak. Skrifttyper Spil Genererer cache, et øjeblik Grafik Hjemmeside Installér Installér nye programmer Installeret Installerede programmer Installerer Installeres denne pakke, kan det medføre uoprettelig skade på dit system. Internet Java Start Begræns søgningen til det viste Matematik Multimediecodecs Multimediecodecs til KDE Navn: Fandt ingen matchende pakker Der er ingen pakke at vise.
Det kan tyde på et problem - prøv at opdatere cachen. Ingen skærmbilleder tilgængelig Ikke tilgængelig Ikke installeret Kontor Valgfrie komponenter PHP Pakke Foto Vær venligst tålmodig. Dette kan godt tage noget tid … Brug venligst apt-get for at installere denne pakke. Programmering Publicering Python Realtidsstrategi Opdatér Opdatér listen over pakker Ekstern: Fjern Fjerner Fjernes denne pakke, kan det medføre uoprettelig skade på dit system. Anmeldelser Skanning Videnskab Videnskab og uddannelse Søg i pakkebeskrivelser (endnu langsommere søgning) Søg i pakkeresuméer (langsommere søgning) Søger i programarkiver, et øjeblik Vis installerede programmer Simulation og kørsel Størrelse: Programhåndtering Noget gik galt. Klik for at prøve igen. Lyd Lyd og video Systempakke Systemværktøjer Flatpak-pakkearkivet, som du prøver at tilføje, findes allerede. Der er i øjeblikket aktive handlinger.
Er du sikker på, du  vil afslutte? Dette er en systempakke Denne pakke er en Flatpak Prøv igen Turbaseret strategi Kan ikke kommunikere med servere. Tjek din internetforbindelse og prøv igen. Ikke tilgængelig Version: Video Fremvisere Web Dit systems pakkehåndteringsprogram 