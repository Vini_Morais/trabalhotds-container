��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  -   �     $     -  
   6  	   A     K     ]     e     t     �     �  	   �     �     �     �     �  
   �     �     �     �     �  	      
   
          4     ;  &   P     w     �     �     �  ,   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-03 12:23+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Acerca Ações Permitir caminhos Aplicações Área onde aparecem os ícones de estado XApp Explorar Cancelar Categorias Categoria Escolha um ícone Padrão Ícone padrão Dispositivos Emblemas Emoji Favoritos Ícone Tamanho do ícone Imagem A carregar... Tipos MIME Abrir Operação não suportada Outro Locais Pesquisar Selecionar Selecione o ficheiro de imagem Estado A categoria padrão. O ícone a utilizar por predefinição O tamanho preferido do ícone. A linha representando o ícone. Opção para permitir caminhos. Mini-aplicação de estado XApp Fábrica de mini-aplicações de estado XApp 