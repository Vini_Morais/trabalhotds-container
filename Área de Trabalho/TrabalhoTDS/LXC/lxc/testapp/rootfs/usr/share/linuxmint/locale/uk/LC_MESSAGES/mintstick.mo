��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  M   �  V   �     1     O  %   n     �     �     �  &   �  5     G   <  h   �  #   �  �       �     �  5   �            !   +  #   M     q     �     �     �     �  l   �  E   2     x  3   |     �     �  P   �  P   "     s  !   �  N   �     �     	           8     H  -   `     �     �     �     �  0   �  l     1   p  K   �  ,   �  t     ;   �  o   �  3   <  U   p  �   �  [   O  W   �  ;      '   ?   �   g      �   ;   �   	   6!  9   @!     z!     �!  #   �!     �!  T   �!     H"     ]"  P   e"     �"  
   �"     �"     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-27 12:16+0000
Last-Translator: Rostyslav Haitkulov <Unknown>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Під час копіювання образу сталася помилка Під час створення розділу на %s. сталася помилка Виникла помилка Сталася помилка. Помилка розпізнання Обчислення… Перевірити Контрольна сума Файл контрольних сум Не співпадає контрольна сума Оберіть позначку для USB-нагромаджувача Завантажте ISO-образ ще раз. Його контрольна сума не вірна. Все виглядає добре! FAT32 
  + Сумісна з усіма ОС і апаратними пристроями.
  - Не можна зберігати файли більші 4Гб.

exFAT
  + Сумісна майже з усіма ОС і апаратними пристроями.
  + Можливо зберігати файли більші 4Гб.
  - Не така широко підтримувана, як FAT32.

NTFS 
  + Сумісна з Windows.
  - Несумісна з Mac і більшістю апаратних пристроїв.
  - Незначні проблеми сумісности з Linux (NTFS є власницькою файловою системою).

EXT4 
  + Сучасна, стабільна, швидка, журнальована.
  + Підтримує файлові дозволи Linux.
  - Несумісна з Windows, Mac і більшістю апаратних пристроїв.
 Файлова система: Форматувати Форматувати USB-нагромаджувач Гб GPG сигнатури Файл підписаний GPG Файл підписаний GPG : Повернутися Перевірка ISO Образ ISO: ISO-образи ISO: Якщо у вас є довіра до підпису, ви можете довіряти образу ISO. Перевірка цілосності зазнала невдачі Kб Ключ не знайдено на сервері. Локальні файли Мб Створити завантажувальний USB-нагромаджувач Створити завантажувальний USB-нагромаджувач Докладніше ID тому не знайдено Бракує вільного місця на USB-нагромаджувачі SHA256 сума Файл сум SHA256 Файл сум SHA256: SHA256сума: Обрати образ Оберіть USB-нагромаджувач Оберіть образ Підписано: %s Розмір: Тб Сума SHA256 образу ISO невірна. Файл сум SHA256 не містить контрольних сум для цього образу ISO. Файл суми SHA256 не підписано. USB-нагромаджувач успішно відформатовано. Невірна контрольна сума Контрольна сума вірна, але її автентичність не була перевірена. Файл gpg не може бути перевіреним. Файл gpg  не може бути завантажений. Перевірте правильність URL. Образ було успішно записано Файл контрольних сум не може бути перевіреним. Файл контрольних сум не може бути завантажений. Перевірте правильність URL. Даний образ ISO підтверджено достовірним підписом. Цей образ ISO підтверджено недовіреним підписом. Цей образ ISO схожий на образ Windows. Це офіційний образ ISO. Це знищить усі дані на USB-нагромаджувачі, Ви впевнені, що бажаєте продовжити? URL'и Запис образу на USB-нагромаджувач USB Stick Форматування USB-нагромаджувача USB-нагромаджувач: Невідомий підпис Недовірений підпис Перевірити Перевірка автентичності та цілісності образу Мітка тому: Том: Образи Windows потребують спеціальної обробки. Записати байти невідомо 