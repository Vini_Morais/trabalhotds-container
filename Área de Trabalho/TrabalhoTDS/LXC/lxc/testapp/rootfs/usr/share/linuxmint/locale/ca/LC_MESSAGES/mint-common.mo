��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  &   -  !   T     v     �  #   �  %   �  !   �  (     "   B  %   e     �  J   �  	   �     �     �  
   	  9   	     M	  0   k	     �	     �	  	   �	     �	  $   �	  W   �	     L
  
   f
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 10:56+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 S'utilitzaran %s més d'espai de disc. S'alliberarà %s d'espai en disc. Es descarregaran %s en total. Calen canvis addicionals Es revertirà programari addicional S'instal·larà programari addicional Es purgarà programari addicional Es reinstal·larà programari addicional S'esborrarà programari addicional S'actualitzarà programari addicional S'ha produït un error No es pot suprimir el paquet %s perquè és requerit per d'altres paquets. Reverteix Flatpak Flatpaks Instal·la El paquet %s és una dependència dels següents paquets: Paquets que s'han de suprimir Feu un cop d'ull a la següent llista de canvis. Purga Reinstal·la Suprimeix Omet l'actualització Se suprimiran els paquets següents: Aquest element del menú no està associat amb cap paquet. Voleu suprimir-ho igualment? S'ometran actualitzacions Actualitza 