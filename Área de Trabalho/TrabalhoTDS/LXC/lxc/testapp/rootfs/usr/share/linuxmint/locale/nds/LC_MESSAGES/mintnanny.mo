��          t      �              %   0     V     f     u  q   �     �             -   1  �  _     �  -        F     X     g  �   |  !        @     L  9   a                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-03-15 21:51+0000
Last-Translator: Sebastian Bohmholt <Unknown>
Language-Team: German, Low <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ist eine ungültige URL Blockiere Zugang zu ausgewählten Domainnamen Bockierte Domains Domain Blocker Internetadresse(URL) Domainnamen müssen mit einem Buchstaben oder einer Ziffer starten und enden, und können ausschließlich Buchstaben, Ziffern, Punkte und Bindestriche enthalten. Beispiel: meine.nummer1domain.com Falsche URL Elterliche Kontrolle Bitte geben Sie die zu blockende Internetadresse(URL) ein 