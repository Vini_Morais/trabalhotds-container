��          <      \       p      q   '   �   _   �   �       �  %   �  Y   �                   Packages to be removed The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-05-01 18:34+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German, Low <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Zu entfernende Pakete Die folgenden Pakete werden entfernt: Der Menüeintrag ist mit keinem Paket verknüpft. Wollen Sie ihn aus dem Menü entfernen? 