��    J      l  e   �      P  )   Q  2   {     �     �     �     �     �     �                (     I    `     m	     y	     �	     �	     �	     �	     �	  
   �	  
   �	     �	  1   �	     
     "
     %
     A
     M
     P
     j
     �
  "   �
  
   �
     �
     �
     �
     �
               "     (  -   +  >   Y  #   �  )   �     �  I   �  "   H  4   k  #   �  #   �  5   �  2     5   Q  $   �     �  N   �            	   0     :  
   N     Y     k       2   �     �     �  *   �     �             �    +   �  5   �          (     >     O     c     k     z     �     �     �    �     �  
             -     0     <     H  
   W  	   b     l  -   q  %   �     �  0   �     �               *     F  #   W     {     �     �     �     �     �     �  	   �     �  #     5   &  "   \  (        �  K   �  "     0   3     d  #   �  1   �  2   �  4     #   A     e  W   }     �     �  
   �                '     5     N  ,   V     �     �  )   �     �     �     �     7      %      I                 ?                   2       5                *   $         !                   F          :   3          6   A   1   -       9   J   B   ,          E   8   (         0      ;          C                      /   '   G   .   "          D       >       @   &      	   H   #          =                             <   
      )   +       4       An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-31 17:12+0000
Last-Translator: Martin Srebotnjak <miles@filmsi.net>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Med kopiranjem odtisa je prišlo do napake. Med ustvarjanjem razdelka na %s je prišlo do napake. Prišlo je do napake Prišlo je do napake. Napaka overitve. Preračunavanje ... Preveri Nadzorna vsota Datoteke kontrolnih vsot Neujemanje kontrolne vsote Izberite ime za ključ USB Vse je videti dobro! FAT32 
  + Združljiv povsod.
  - Ne podpira datoteh, večjih od 4 GB.

exFAT
  + Združljiv skoraj povsod.
  + Podpira datoteke, večje od 4 GB.
  - Ni tako združljiv kot FAT32.

NTFS 
  + Združljiv s sistemi Windows.
  - Ni združljiv s sistemi macOS in večino strojnih naprav.
  - Občasni problemi z združljivostjo na Linuxu (NTFS je lastniški in reverse-engineered).

EXT4 
  + Moderen, stabilen, hiter, dnevniški.
  + Podpora za dovoljenja datotek Linux.
  - Ni združljiv s sistemi Windows, macOS in večino strojnih naprav.
 Datotečni sistem: Formatiraj Formatiraj ključ USB GB Podpisi GPG Pojdi nazaj Preveritev ISO Slika ISO: Slike ISO ISO: Če zaupate podpisu, lahko zaupate sliki ISO. Preverjanje integritete je spodletelo KB Ključa ni mogoče najti na strežniku ključev. Krajevne datoteke MB Ustvari zagonski ključ USB Ustvari zagonski ključ USB Več podrobnosti Na ključku USB ni dovolj prostora. Vsota SHA256 Datoteka vsto SHA256 Datoteka vsot SHA256: Izberite odtis Izberite ključ USB Izberite odtis Podpisano z: %s Velikost: TB Vsota SHA256 slike ISO ni pravilna. Datoteka vsot SHA256 ne vsebuje vsot za to sliko ISO. Datoteka vsot SHA256 ni podpisana. Ključek USB je bil uspešno formatiran. Kontrolna vsota je pravilna Kontrolna vsota je pravilna, vendar avtentičnost vsote ni bila preverjena. Datoteke gpg ni mogoče preveriti. Datoteke gpg ni mogoče prenesti. Preverite URL. Odtis je bil uspešno zapisan. Datoteke vsot ni mogoče preveriti. Datoteke vsto ni mogoče prenesti. Preverite URL. Slika ISO je overjena z zaupanja vrednim podpisom. Slika ISO je overjena z zaupanja nevrednim podpisom. Ta ISO je videti kot slika Windows. To je uradna slika ISO. To bo uničilo vse podatke na ključu USB. Ali ste prepričani, da želite nadaljevati? URL-ji Zapisovalnik odtisov USB Ključ USB Formatirnik ključev USB Ključek USB: Neznan podpis Zaupanja nevreden podpis Preveri Preverite avtentičnost in integriteto slike Oznaka nosilca: Pogon: Slike Windows zahtevajo posebno obdelavo. Zapiši bajtov neznano 