��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  T   "  H   w     �  "   �  5   �  �  .	     �  #     ,   /  ,   \  7   �     �  !   �     �  .     $   J  ^   o  (   �     �  !        &     3     O     X                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-07-16 16:59+0000
Last-Translator: Murat Käribay <d2vsd1@mail.ru>
Language-Team: Kazakh <kk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Бейнені көшіру барысында қателік пайда болды. Бөлімше құрғанда %s қателік пайда болды. Қате кетті. Аутенфикация Қате. USB-флеш үшін таңбаны таңдаңыз FAT32 
  + Барлығына үйлесімді.
  - 4ГБ-тан үлкен мөлшерлі файлды өңдей алалмайды.

exFAT
  + Жуықтағанда барлығына үйлесімді.
  + 4ГБ-тан үлкен мөлшерлі файлды өңдей алады.
  - FAT32 сияқты үйлесімді емес.

NTFS 
  + Windows-пен үйлесімді.
  - Mac-пен үйлеспейді.
  - Linux-пен аздаған қиындықтары болады.

EXT4 
  + Заманауи, тұрақты, жылдам.
  + Linux файлдарына толық құқық беріліді.
  - Windows, Mac және көптеген құрылғылармен үйлеспейді.
 Пішімі USB-флешті форматтау Жүктелмелі USB-флеш жасау Жүктелмелі USB-флеш жасау USB-флешта бос орын жеткіліксіз Бейнені таңдау USB-флешті таңдаңыз Бейнені таңдау USB-флеш сәтті форматталды Бейне сәтті жазылды USB-флештағы барлық мәлімет өшіріледі. Жалғасыртыру? USB-флешқа бейнені жазу USB-флеш USB-флеш форматтауы USB-флеш Томның таңбасы Жазу белгісіз 