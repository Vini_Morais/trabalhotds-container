��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  0     <   L     �     �  #   �  W  �     2     >     V     t  #   �     �     �     �  &   �  "     N   ?     �  	   �     �  
   �     �     �  
   �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-11-03 13:28+0000
Last-Translator: Dorian Baciu <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 A apărut o eroare în timpul copierii imaginii. A apărut o eroare în timpul creării unei partiții pe %s. A apărut o eroare. Eroare de autentificare. Alegeți un nume pentru stickul USB FAT32
   + Compatibil peste tot.
   - Nu se pot ocupa de fișiere mai mari de 4 GB.

exFAT
  + Compatibil aproape pretutindeni.
  + Poate manipula fișiere mai mari de 4GB.
  - Nu este la fel de compatibil ca FAT32.

NTFS
  + Compatibil cu Windows.
  - Nu este compatibil cu Mac și majoritatea dispozitivelor hardware.
  - Probleme de compatibilitate ocazionale cu Linux (NTFS este proprietar și inversat).

EXT4
  + Moderne, stabile, rapide, periodice.
  + Suportă permisiunile pentru fișiere Linux.
  - Nu este compatibil cu Windows, Mac și majoritatea dispozitivelor hardware.
 Formatează Formatați un stick USB Faceți un stick USB bootabil Faceți un stick USB bootabil Spațiu insuficient pe stickul USB. Selectare imagine Selectați un stick USB Selectați o imagine Stickul USB a fost formatat cu succes. Imaginea a fost scrisă cu succes. Se vor distruge toate datele de pe stickul USB, sigur doriți să continuați? Scriitor imagine USB Stick USB Formator stick USB Stick USB: Etichetă volum: Scrie necunoscut 