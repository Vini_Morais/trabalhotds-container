��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  ~  �  .   o  B   �     �     �                $     -     @      [  &   |  Q   �     �  �       �  
   �     �     �     �     �          $     2     R     f     v  S   {  %   �     �  +   �     $     2  !   5  !   W     y  !   �  '   �     �     �           4     O      h     �     �     �     �  A   �  f     5   l  $   �      �  N   �  !   7  ;   Y  $   �  0   �  J   �  K   6  O   �  5   �  $     K   -     y     �     �     �  	   �     �     �     �  4   �     3     A  C   I     �     �  
   �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: mintstick
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-29 13:24+0000
Last-Translator: KAMI <kami911@gmail.com>
Language-Team: Hungarian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: hu
 Hiba történt a lemezkép másolása közben. Hiba történt a partíció létrehozása közben a %s eszközön. Hiba történt Hiba történt. Hitelesítési hiba. Számítás… Jelölő Ellenőrzőösszeg Ellenőrzőösszeg-fájlok Az ellenőrzőösszeg megegyező Válasszon nevet a pendrive eszköznek Töltse le ismét az ISO-lemezképet, mert az ellenőrzőösszeg nem egyezik meg. Minden rendben. FAT32 
  + Teljes körű kompatibilitás.
   - Nem képes 4 GB-nál nagyobb fájlt kezelni.

exFAT
  + Szinte teljes körű kompatibilitás.
  + Képes 4 GB-nál nagyobb fájlt kezelni.
  -  Kevésbé kompatibilis mint a FAT32 fájlrendszer.

NTFS 
  + Windows kompatibilitás.
   - Nem kompatibilis Mac operációs rendszerrel és a legtöbb 
hardvereszközzel.
   - Esetenként kompatibilitási problémák lehetnek Linux operációs 
rendszeren (az NTFS zárt formátum és visszafejtett).

EXT4 

  + Modern, stabil, gyors és naplózott.
  + Támogatja a Linux jogosultság-kezelését.
   - Nem kompatibilis a Windows, a Mac operációs rendszerekkel és a legtöbb 
hardvereszközzel.
 Fájlrendszer: Formázás Pendrive formázása GB GPG aláírások GPG aláírt fájl GPG aláírt fájl: Ugrás vissza ISO-lemezképfájl ellenőrzés ISO-lemezképfájl: ISO-lemezképek ISO: Amennyiben megbízik az aláírásban, akkor megbízhat a ISO-lemezképfájlban is. Az épségellenőrzés hibát jelzett KB A kulcs nem található kulcskiszolgálón. Helyi fájlok MB Indítható pendrive készítése Indítható pendrive készítése További információ Nem található kötetazonosító Nincs elég hely a pendrive eszközön. SHA256 ellenőrzőösszeg SHA256 ellenőrzőösszeg-fájl SHA256 ellenőrzőösszeg-fájl: SHA256 ellenőrzőösszeg: Lemezkép kiválasztása Válasszon egy pendrive eszközt Válasszon egy lemezképet Aláírta: „%s” Méret: TB Az ISO-lemezképfájl SHA256 ellenőrzőösszege nem egyezik meg. Az SHA256 ellenőrzőösszeg-fájl nem tartalmazza ennek az ISO-lemezképfájl ellenőrző összegét. Az SHA256 ellenőrzőösszeg-fájl nincsen aláírva. A pendrive sikeresen formázva lett. Az ellenőrzőösszeg megegyező Az ellenőrzőösszeg megegyező, de annak hitelessége nem volt ellenőrizve. Az GPG-fájl nem ellenőrizhető. Az GPG-fájl nem tölthető le. Ellenőrizze az URL-címet. A lemezkép sikeresen ki lett írva. Az ellenőrzőösszeg-fájl nem ellenőrizhető. Az ellenőrzőösszeg-fájl nem tölthető le. Ellenőrizze az URL-címet. Az ISO-lemezképfájl hitelessége megbízható aláírással ellenőrizve. Az ISO-lemezképfájl hitelessége nem megbízható aláírással ellenőrizve. Ez az ISO-lemezképfájl Windows lemezképnek tűnik. Ez egy hivatalos ISO-lemezképfájl. Minden adat törlése a pendrive eszközről. Biztosan folytatni kívánja? URL-ek Pendrive lemezkép-író Pendrive Pendrive formázó Pendrive: Ismeretlen aláírás Nem megbízható aláírás Ellenőrzés Ellenőrizze a lemezkép hitelessége és épségét Kötetcímke: Kötet: A Windows lemezkép-fájlok különleges feldolgozást igényelnek. Írás bájt ismeretlen 