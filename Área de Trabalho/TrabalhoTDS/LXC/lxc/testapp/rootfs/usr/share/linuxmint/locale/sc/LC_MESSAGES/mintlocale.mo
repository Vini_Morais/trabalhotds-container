��    %      D  5   l      @  w   A     �     �     �     �     �     �            �   "     �     �     �  ^        k     �     �     �     �     �     �     �  	   �     �     
  ,        <     C     J     g     z     �     �     �     �  
   �  �  �  z   �     	     (	     ?	     H	     b	     j	     p	     �	  �   �	     Z
     c
     z
  q   �
  !        (  	   F     P     X     ^     s     �     �     �     �  (   �     �     �     �          !     @     T  
   [     f  
   |                                               
   %                  #                                                "       $                        !                      	          <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Cancel Close Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-09-03 16:12+0000
Last-Translator: Utalabi <utalabi@gmail.com>
Language-Team: Sardinian <sc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Noda: installende o annoende pachetes de limbas si podet achicare s'installatzione de limbas in prus</small></b> Agiunghe un'Àtera Limba Agiunghe una limba noa Agiunghe Àplica a Totu su Sistema Annulla Serra Installadu de su totu Mètodu de mintidura Sos mètodos de mintidura s'impreant pro iscrìere signos e caràteres chi non s'agatant in sa tastiera. Sunt utilosos pro iscrìere in Tzinesu, Giaponesu, Coreanu, Tailandesu, Vietnamita... Installa Installa / Boga Limbas Installa / Boga Limbas... Installa sos pachetes de sa limba mancantes, sos file de tradutzione, sos ditzionàrios pro sa limba ischirriada. Installa sos pachetes de sa limba Installa sa limba ischirriada Giaponesu Coreanu Limba Sèberos de sa Limba Sèberos de sa limba Limbas apoderadas Limbas Mancu una limba definida Mancunu Nùmeros, moneda, indiritzos, mesuras... Regione Boga Boga sa limba ischirriada Tzinesu Simpre Mancat carchi pachete de limba Limba de su sistema Telugu Tailandesu Tzinesu Traditzionale Vietnamita 