��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  K   �  c   B  A   �  "   �       �   +  Q   �  )   H  %   r     �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-24 16:40+0000
Last-Translator: Siddharth Belbase <Unknown>
Language-Team: Nepali <ne@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s प्रभावकारी डोमेनको नाम भएन चुनिएका डोमेनहरु हेर्नबाट रोक्नुहोस अवरुद्ध गरिएका डोमेनहरु डोमेन अवरोधक डोमेनको नाम डोमेन नाममा अक्षर वा अंक अथवा अक्षर्,अंक,थोप्ला अनि योजकचिन्ह मात्र् हुन्छ । उदाहरण्:मेरो.नम्बरएकडोमेन्.कम डोमेनको नाम भएन बालक नियंत्रण हजुरले ब्लक गर्न चाहेको डोमेनको नाम भन्नुहोस् । 