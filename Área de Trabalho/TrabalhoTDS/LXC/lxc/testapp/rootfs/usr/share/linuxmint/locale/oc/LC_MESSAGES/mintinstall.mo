��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  C     D   [  #   �  $   �  7   �     !  	   $     .     :     >  !   T  <   v     �     �  u   �     R     h  B   |  +   �     �     �     �  	               	   1  
   ;     F     ]  w   v  b   �     Q     Z     `  	   h      r     �     �     �  D   �     
            &         G     S     f  /   �  r   �     $     3     A     H     L  
   S  1   ^  /   �     �     �     �     �  
   �           "  
   (  C   3  	   w     �  	   �     �  F   �  ;   �  1   ,  #   ^     �     �     �     �     �  9   �  >        J     f  
   m  	   x         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-13 06:45+0000
Last-Translator: Quentin PAGÈS <Unknown>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s de telecargar, %(localSize)s d'espaci disc liberat %(downloadSize)s de telecargar, %(localSize)s d'espaci disc requesit %(localSize)s d'espaci disc liberat %(localSize)s d'espaci disc requesit %d prètzfach en execucion %d prètzfaches en execucion 3D A prepaus Accessòris Tot Totas las aplicacions Totas las operacions son acabadas S'ha produit un error al ensajar adiccionar lo repo Flatpak. Jòcs de platèu Installacion impossibla Es pas possible de processar aqueste archiu durant operacions activas.
Se vos plai, ensaja de nòu après lo seu fin. Supression impossibla Discussion en linha Clicatz <a href='%s'>ici</a> per apondre vòstra pròpria critica. D’operacion en cors suls paquets seguents Detalhs Dessenh Suggestions Educacion Electronica Adreça electronica Emulators Essencials Partiment de fichièrs Jòc en vista subjectiva La presa en carga de Flatpak es pas prepausada actualament. Volgatz ensajar d’installar flatpak e gir1.2-flatpak-1.0. La presa en carga de Flatpak es pas prepausada actualament. Volgatz ensajar d’installar flatpak. Poliças Jòcs Grafics Installar Installar d'aplicacions novèlas Installat(s) Aplicacions installadas Installacion Installar aqueste paquet poiriá far de mal irreparable al sistèma. Internet Java Lançar Limitar la recèrca a la lista actuala Matematicas Codecs Multimèdia Codecs Multimèdia per KDE Cap de paquet correspondent es pas estat trobat I a pas cap de paquet d’afichar.
Aquò pòt indicar un problèma. Volgatz ensajar d’actualizar l’escondedor. Pas disponible Pas installat Burèu PHP Paquet Fotografia Volgatz esperar. Aquesta operacion pòt trigar... Utilizatz apt-get per installar aqueste paquet. Desvolopament Edicion Python Estrategia en temps real Actualizar Actualizar la lista dels paquets Levar Supression Suprimir aqueste paquet poiriá far de mal irreparable al sistèma. Revisions Analisi Sciéncia Sciéncia e educacion Inclure la descripcion dels paquets dins la recèrca (encara mai lent) Inclure lo resumit dels paquets dins la recèrca (mai lent) Recèrca pels depauses logicials, volgatz esperar Mostrar las aplicacions installadas Simulacions e corsas Gestionari de logicials Son Son e vidèo Aisinas sistèma Lo repo Flatpak qu'estàs ensajant adiccionar ja existè. D’operacions son en cors.
Volètz vertadièrament quitar ? Estrategia al torn per torn Vidèo Afichadors Sites Web 