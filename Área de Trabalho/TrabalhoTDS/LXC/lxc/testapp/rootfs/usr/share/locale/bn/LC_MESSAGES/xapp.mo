��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  !   �  0   �  0     k   O     �     �  !   �  !     ?   <     |     �     �  -   �     �     
	     &	     3	  	   S	     ]	  %   z	     �	  2   �	     �	     �	  (   
  %   B
  ?   h
     �
  1   �
  ^   �
  /   L  d   |  M   �  Q   /  m   �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-01-27 09:24+0000
Last-Translator: Fahim Shahriar <Unknown>
Language-Team: Bengali <bn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 পরিচিতি ক্রিয়াকলাপ পাথস(Paths) অনুমতি দিন অ্যাপ্লিকেশনসমূহ এক্স-অ্যাপ স্টেটাস আইকন উপস্থিতির অঞ্চল ব্রাউজ করুন বাতিল করুন শ্রেণীবিভাগ শ্রেণীবিভাগ একটি আইকন নির্বাচন করুন মৌল মান মৌল আইকন ডিভাইসসমূহ প্রতীকচিহ্নসমূহ ইমোজি পছন্দসমূহ আইকন আইকনের আয়তন ছবি লোড হচ্ছে... মাইম টাইপসমূহ খুলুন অপারেশন সমর্থিত নয় অন্যান্য স্থান সমূহ অনুসন্ধান করুন নির্বাচন করুন ইমেজ ফাইল নির্বাচন করুন অবস্থা মৌল শ্রেণীবিভাগ। মৌলরূপে(Default) ব্যবহার করার জন্য আইকন পছন্দসই আইকন আকার যে স্ট্রিংটি আইকনটাকে উপস্থাপন করছে। পাথস(Paths) অনুমতি দিতে হবে কিনা। এক্স-অ্যাপ স্ট্যাটাস অ্যাপলেট এক্স-অ্যাপ স্ট্যাটাস অ্যাপলেট ফ্যাক্টরি 