��    6      �  I   |      �  ,   �  )   �     �  I     6   ]  w   �               2     9     K     ]     d     j     z          �     �  �   �     Z     b     }  ^   �     �          /     8     ?     H     Z     l  	   }     �     �  ,   �     �     �     �     �     		     )	     7	     >	  _   C	  *   �	  (   �	  .   �	  6   &
  7   ]
     �
  
   �
     �
     �
  1  �
  �   �  )   �  9   �  l   �  N   g  �   �     F     c     �     �  )   �  
   �     �     �                '  #   A  "  e  
   �  $   �  '   �  o   �     P  $   m     �     �  
   �     �     �     �               2  B   >     �  
   �  $   �     �  '   �               /  �   F  E   �  K     I   ^  T   �  V   �  !   T     v     �     �     '   )   &                  #   /                2       -                !   3      0          *   .                                                       ,       "                     %              (   $           4             
      5      6   +   	   1    %d language installed %d languages installed - Install the language support packages:  - Log out and log back in. - Right-click the Fcitx applet in the system tray and choose "Configure". - Switch between input methods with <b>Ctrl+Space</b>. <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Already installed Apply System-Wide Cancel Close Fully installed Help Input Method Input method Input method framework: Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai To add support for a particular language, select it in the sidebar and follow the instructions. To write in Telugu follow the steps below: To write in Thai follow the steps below: To write in Vietnamese follow the steps below: To write in simplified Chinese follow the steps below: To write in traditional Chinese follow the steps below: Traditional Chinese Vietnamese Welcome XIM Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-08-26 17:10+0000
Last-Translator: أنس <Unknown>
Language-Team: anwar AL_iskandrany <anwareleskndrany13@gmail.com >
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 && n % 100 <= 99 ? 4 : 5;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ar_EG
 تم تثبيت %d لغة تم تثبيت %d لغة تم تثبيت %d لغات تم تثبيت %d لغات تم تثبيت %d لغات تم تثبيت %d لغات - تثبيت حزم دعم اللغات:  - تسجيل الخروج ثم الولوج مجددًا. - انقر نقرة يمنى على البريمج Fcitx في علبة النظام واختر "تكوين". - التبديل بين طرق الإدخال عن طريق <b>Ctrl+Space</b>. <b><small>ملاحظة: يمكن أن يؤدي تثبيت أو ترقية حزم اللغات إلى تثبيت لغة إضافية</small></b> إضافة لغة جديدة إضافة لغة جديدة إضافة... مثبّتة بالفعل تطبيق على مستوى النظام إلغاء أغلق مثبتة بالكامل مساعدة طريقة الإدخال أسلوب الإدخال هيكل طريقة الإدخال: تُستخدم طرق الإدخال لكتابة الرموز والأحرف غير الموجودة على لوحة المفاتيح. فهي مفيدة في الكتابة باللغة الصينية واليابانية والكورية والتايلاندية والفيتنامية... تثبيت تثبيت / إزالة اللغات تثبيت / إزالة اللغات... تثبيت أي حزم لغة مفقودة وملفات الترجمة وقواميس اللغة المحددة تثبيت حزم اللغة تثبيت اللغة المحددة اليابانية الكورية اللغة إعدادات اللغة إعدادات اللغة دعم اللغة اللغات لا توجد لغة محددة لا شيء الأرقام، العملة، العناوين، القياس... المنطقة إزالة إزالة اللغة المحددة الصينية المبسطة بعض حزم اللغات مفقودة لغة النظام التيلوغوية التايلاندية لإضافة الدعم للغة معيّنة، اختر اللغة من الشريط الجانبي واتبّع التعليمات. للكتابة بالتيلوجو اتبع الخطوات أدناه: للكتابة بالتايلاندية اتبع الخطوات أدناه: للكتابة بالفيتنامية اتبع الخطوات أدناه: للكتابة بالصينية المبسّطة اتبع الخطوات أدناه: للكتابة بالصينية التقليدية اتبع الخطوات أدناه: الصينية التقليدية الفيتنامية مرحبًا XIM 