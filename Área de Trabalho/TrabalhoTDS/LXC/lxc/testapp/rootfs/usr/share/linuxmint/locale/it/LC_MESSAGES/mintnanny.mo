��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  $   �  ,        <     L     Z  �   g      �            2   /                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-26 11:43+0000
Last-Translator: Fs00 <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s non è un nome valido di dominio. Blocca accesso a nomi di dominio selezionati Domini bloccati Blocco domini Nome dominio I nomi di dominio devono iniziare e finire con una lettera o un numero, e possono contenere solo lettere, numeri, punti e trattini. Esempio: mio.dominio-numero1.com Dominio non valido Filtro famiglia Inserire il nome del dominio che si vuole bloccare 