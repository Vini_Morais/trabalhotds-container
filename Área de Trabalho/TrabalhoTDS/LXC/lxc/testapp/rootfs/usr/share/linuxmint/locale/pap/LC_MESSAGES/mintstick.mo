��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  -   �  8        ;     L      b     �     �     �     �  +   �                &  .   7  "   f  S   �     �  	   �     �  
             +     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-06-23 14:32+0000
Last-Translator: Frank <f.boye@hotmail.nl>
Language-Team: Papiamento <pap@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Un eror a surgi durante kopiamentu di e image Un eror a surgi durante kreashon di e partishon riba %s. Un eror a surgi. Authentication Error. Skohe un nòmber pa bo USB Stick Format Format un USB stick Hasi un USB stick bootable Hasi un USB stick bootable No tin sufisiente espasio riba e USB stick. Selekta un image Selekta un USB stick Selekta un image E USB stick a wòrdu geformateerd korektamente E image a wòrdu krea korektamente E akshon aki lo destrui tur dokumento riba e USB stick, bo tin sigur ku bo ke sigi? USB Image Writer USB Stick USB Stick Formatter USB stick: Label di e volumen: Skibi 