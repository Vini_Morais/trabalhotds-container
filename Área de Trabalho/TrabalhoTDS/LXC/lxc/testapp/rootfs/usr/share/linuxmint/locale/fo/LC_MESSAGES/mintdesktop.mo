��    4      �  G   \      x  
   y  D   �  {   �     E     U  9   e     �     �     �     �     �  -   �       �   &  >   �       
          
     \   (  X   �  \   �     ;  
   A     L     ]  �   c     �     	     	  2   	      I	     j	  �   �	  0   
     D
     Z
     n
     t
     �
  	   �
  5   �
     �
     �
     �
             2   9     l  `   t     �  �  �  	   �  F   �  z   �     M     ]  <   n     �     �     �     �     �  1   �  %   #  �   I  ;   �     %     *     8     A  e   Q  ]   �  b        x  
   ~     �     �  �   �     *     7     ;  2   C  #   v  $   �  �   �  5   K     �     �     �     �      �     �  3        5     D  	   [  ,   e  %   �  4   �     �  B   �  �   8     %   4          *      &         
             	                                        .      )                3            0             -          /           '           #             +                     2   "       $      !   ,       1       (    Advantages Better look &amp; feel: Window animations, shadows and transparency. Better support for modern applications: Client-side decorations in GTK3 are better supported by composited window managers. Buttons labels: Buttons layout: Compositing (shadows, animations, effects, GPU rendering) Computer Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings GPU rendering: Depending on your graphics card and your drivers, rendering with the GPU (as opposed to the CPU) can result in visual artefacts or screen tearing. Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Compton is not installed already, you can install it with <cmd>apt install compton</cmd>. If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Openbox is not installed already, you can install it with <cmd>apt install openbox</cmd>. Large Linux Mint Mac style (Left) Marco Marco is the default window manager for the MATE desktop environment. It is very stable and very reliable. It can run with or without compositing. Mounted Volumes Network Openbox Openbox is a fast and light-weight window manager. Overview of some window managers Pros and cons of compositing Resource usage: Compositing requires more resources. This can result in slower performance or reduced battery autonomy on laptop computers. Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only The Windows section lets you choose a window manager. Toolbars Traditional style (Right) Trash Use system font in titlebar Welcome to Desktop Settings. Window events (moving, resizing, focusing windows) Windows Within the operating system, the <gui>window manager</gui> is the part which is responsible for: translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-03-09 20:54+0000
Last-Translator: Richard Schwartson <Unknown>
Language-Team: Faroese <fo@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Fyrimunir Betri útsjónd &amp; feel: Windows myndir, skuggar og gjøgnumskygni. Betri stuðlan av  nýggjari forritum: Client-side myndagerð í GTK3 eru betri stuðlað við composited window managers. Knappaspjaldur: Knappauppseting: Kompositing (skuggar, animatiónir, effektir, GPU rendering) Telda Skriviborð Uppsetan av skriviborð Skriviborðsímyndir Vansar Vís ikki gluggainnihald, tá hann verður drigin Neyvstilla skriviborðs innstillingar GPU rendering: Tað velst um grafikk kort og dreivarir, men rendering við GPU (í mun til CPU) kann elva til sjónligar artifaktar ella ólag í skíggjamynd. Her er ein lýsing av nøkur window managers sum eru tøkir Heim Ímyndastødd Ímyndir Einans ímyndir Um Compton ikki longu er lagt inn, so kanst tú leggja tað inn við  <cmd>apt install compton</cmd>. Um Marco ikki longu er lagt inn, kanst tú leggja tað inn við <cmd>apt install marco</cmd>. Um Openbox ikki longu er lagt inn, kanst tú leggja hann inn við  <cmd>apt install openbox</cmd>. Stór Linux Mint Mac snið (vinstru) Marco Marco er sjálvsettir window manager fyri MARE desktop umhvørvi.  Tað er álítandi og virkar væl bæði við og uttan compositing. Í sett bind Net Openbox Openbox er ein skjótur og lættur window manager. Yvirlit yvir nakrar window managers Fyrimunir og vansar við compositing Resource usage: Compositing requires more resources. This can result in slower performance or reduced battery autonomy on laptop computers. Vel tann liðin, ið tú vil hava á skriviborðinum: Vís ímyndir á knappum Vís ímyndir á valmyndum Lítil Tekstur undir liðum Tekstur við síðinar av liðum Einans tekstur í Windows partinum kanst tú velja window manager. Amboðsstengur Vanligt snið (høgru) Ruskspann Nýt stavasniðið á kervinum í yvurskrift Vælkomin til uppsetan av skriviborð Hendingar í Windows (flyta, minka, vaksa, hálýsa) Gluggar Í stýriskipanini er <gui>window manager</gui>  tað sum stýrir: Launchpad Contributions:
  Gunleif Joensen https://launchpad.net/~gunleif
  Richard Schwartson https://launchpad.net/~schwartson 