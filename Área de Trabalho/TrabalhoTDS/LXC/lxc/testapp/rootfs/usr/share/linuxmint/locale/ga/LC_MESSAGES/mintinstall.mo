��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  J   W  J   �  )   �  -   	     E	  
   H	     S	     _	     n	     v	  
   �	  	   �	     �	     �	     �	     �	     �	     �	  	   �	     �	     
  	   
     
     -
  	   3
     =
     S
     b
     t
     �
     �
  
   �
     �
  9   �
  4        6     R     i     }  $   �     �     �            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-04-28 16:57+0000
Last-Translator: Gearóid  Ó Maelearcaidh <gearoid@omaelearcaidh.com>
Language-Team: Irish <ga@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n==2 ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s le híosluchtú, %(localSize)s de spás ar diosca saortha %(downloadSize)s le híosluchtú, %(localSize)s de spás ar diosca saortha %(localSize)s de spáis ar diosca saortha %(localSize)s de spás ar diosca ag teastáil 3D Eolas Faoi Oiriúintí Cluichí Boird Comhrá Mionsonraí Líníocht Oideachas R-phost Aithriseoirí Comhroinnt comhad Clôfhoirne Cluichí Grafaic Suiteáil Suiteáil feidhmchláir nua Suiteáilte Idirlíon Níl sé suiteáilte Oifig Pacáiste Grianghrafadóireacht Ríomhchlárú Foilsitheoireacht Stráitéis i bhfíor-ama Bain Léirmheasanna Ag Scanadh Eolaíocht agus Oideachas Cuardaigh i dtuairisc ar phacáistí (even slower search) Cuardaigh in achoimre na bpacáistí (slower search) Ionsamhlú agus rásaíocht Bainisteoir bogearraí Fuaim agus Físeán Uirlisí an chórais Stráitéis bunaithe ar shealaíocht Amharcóirí An Gréasán 