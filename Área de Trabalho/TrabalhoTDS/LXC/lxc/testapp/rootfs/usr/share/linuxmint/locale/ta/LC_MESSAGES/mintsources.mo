��    *      l  ;   �      �     �     �     �     �     �     �  :   �     7     ?     W     v     ~     �     �     �     �     �     �     �     �     �     �     �     �     �  8   �     8     U     \  
   t          �     �     �     �  /   �  6   �     2     O     a     f  �  �  *     U   @     �     �  ?   �  E   	  �   H	     /
  N   <
  f   �
  0   �
  i   #  1   �     �     �  7   �               !     4  3   S     �     �     �     �  �   �  S   y     �  D   �  :   %  S   `  O   �  7        <     U  �   e  }   �  ?   o  :   �     �  b   �                                   *                                             
   )          !       $                                #      &                   %                  	   (      '       "        More info: %s Backported packages Base Cancel Cancelling... Cannot add PPA: '%s'. Configure the sources for installable software and updates Country Edit the URL of the PPA Edit the URL of the repository Enabled Fix MergeList problems Foreign packages GB/s Install Installed version Key MB/s Main Maintenance Mirrors Open.. PPA PPAs Package Please enter the name of the repository you want to add: Purge residual configuration Remove Remove foreign packages Repository Restore the default settings Select a mirror Software Sources Sources Speed The problem was fixed. Please reload the cache. There is no more residual configuration on the system. This PPA does not support %s Unstable packages kB/s version %s already installed Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-02 09:15+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
  மேலும் விவரம்: %s பின்துறையிடப்பட்ட தொகுப்புகள் அடித்தளம் தவிர் ரத்துசெய்யப்படுகிறது... பிபிஏ சேர்க்க முடியாது: '%s'. நிறுவத்தக்க மென்பொருட்கள் மற்றும் புதுப்பித்தல்களுக்கான மூலங்களைக் கட்டமைக்கவும் நாடு பிபிஏ வின் யூஆர்எல்ஐ திருத்த தொகுபதிவகத்தின் யூஆர்எல் ஐ திருத்து செயலாக்கப்பட்டது பட்டியல் இணைப்பு பிரச்சினைகளை சரிசெய் அயல் தொகுப்புகள் GB/s நிறுவு நிறுவப்பட்ட பதிப்பு விசை MB/s பிரதான பராமரிப்பு பிரதிபலிப்பான்கள் திற... பிபிஏ பிபிஏ தொகுப்பு நீங்கள் சேர்க்க விரும்பும் தொகுபதிவகத்தின் பெயரை உள்ளிடவும்: எஞ்சிய கட்டமைப்பை சுத்தமாக்கு நீக்கு அயல் தொகுப்புகளை நீக்கு மென்பொருள் களஞ்சியம் இயல்புநிலை அமைப்புகளை மீட்டமை பிம்பத்தை தேர்ந்தெடுக்கவும் மென்பொருள் மூலங்கள் மூலங்கள் வேகம் பிரச்சனை சரி செய்யப்பட்டது. கேச்சை மறுஏற்றம் செய்க. கணினியில் இன்னும் எஞ்சிய கட்டமைப்புகள் இல்லை. %s-ஐ இந்த PPA ஆதரிப்பதில்லை நிலையற்ற தொகுப்புகள் kB/s பதிப்பு %s ஏற்கனவே நிறுவப்பட்டுள்ளது 