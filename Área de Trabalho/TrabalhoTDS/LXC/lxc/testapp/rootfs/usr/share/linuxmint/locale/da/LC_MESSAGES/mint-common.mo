��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  $   8  '   ]     �  #   �  )   �  )   �  %     ,   B  +   o  (   �     �  G   �  
   	     *	     2	  
   ;	  1   F	     x	  ,   �	     �	     �	     �	     �	  )   �	  W   &
  $   ~
  	   �
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-04 13:38+0000
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s plads på disken vil blive brugt. %s plads på disken vil blive frigivet. %s i alt vil blive downloadet. Yderligere ændringer er påkrævet Yderligere software vil blive nedgraderet Yderligere software vil blive installeret Yderligere software vil blive fjernet Yderligere software vil blive geninstalleret Yderligere software vil blive afinstalleret Yderligere software vil blive opgraderet Der opstod en fejl Kan ikke afinstallere pakken %s, da den er nødvendig for andre pakker. Nedgradér Flatpak Flatpaks Installér Pakken %s er en afhængighed af følgende pakker: Pakker der skal afinstalleres Se venligst listen over ændringer nedenfor. Fjern Geninstallér Afinstallér Spring opgradering over Følgende pakker vil blive afinstalleret: Dette menupunkt er ikke tilknyttet nogen pakke. Vil du fjerne det fra menuen alligevel? Opdateringer vil blive sprunget over Opgradér 