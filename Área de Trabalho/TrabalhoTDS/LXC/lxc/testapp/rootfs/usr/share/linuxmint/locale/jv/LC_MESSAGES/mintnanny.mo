��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  !   �  .        A     Y     j  w   x     �            0   8                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-07-02 01:54+0000
Last-Translator: anandiamy <Unknown>
Language-Team: Javanese <jv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s dudu jeneng domain sing bener. Ngeblok akses reng jengen domain sing dipilehi Pengeblok domain-domain Pengeblok Domain Jeneng domain Jeneng domain kudu diwiwiti lan diakhiri karo huruf utawa angka, lan mung bisa ngemot huruf, angka, titik, lan hyphens. Contoh: my.number1domain.com Domain ora valid Kontrolan kanggo wongtuwo Mangga ngetik jeneng domain sing pengin diblokir 