��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     	   �  	   �     �  
   �  *   �     
            
   $     /  	   ?  
   I     T  	   \  
   f     q     z     �     �     �     �     �     �     �     �     �     �     �     �     �          )     C     _     t     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-10-05 09:30+0000
Last-Translator: vaba <Unknown>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Lähemalt Tegevused Asukohtade lubamine Rakendused Piirkond, kus XApp olekuikoone näidatakse Lehitse Loobu Kategooriad Kategooria Ikooni valimine Vaikimisi Vaikeikoon Seadmed Embleemid Emotikonid Lemmikud Ikoon Ikooni suurus Pilt Laadimine... MIME-tüübid Ava Toiming ei ole toetatud Muud Asukohad Otsing Vali Vali pildi fail Olek Vaikimisi kategooria. Vaikimisi kasutatav ikoon Eelistatud ikooni suurus. Sõne, mis viitab ikoonile. Kas asukohad lubada. XApp olekuikoonide rakend XApp olekuikoonide rakend 