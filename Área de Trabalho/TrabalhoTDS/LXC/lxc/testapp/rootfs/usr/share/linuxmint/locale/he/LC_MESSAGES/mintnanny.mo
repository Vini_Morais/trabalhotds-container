��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  &   �  4        J     i     �  �   �      M     n     �  =   �                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-04 16:09+0000
Last-Translator: Omer I.S. <Unknown>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ‏%s אינו שם תחום תקין. חסימת גישה לשמות תחום שנבחרו שמות תחום חסומים חוסם שמות תחום שם תחום על שמות תחום להתחיל ולהסתיים באות אנגלית או תו, ויכולים להכיל רק אותיות לטיניות, מספרים, נקודות ומקפים. דוגמה: my.number1domain.com שם תחום לא תקין בקרת הורים נא להזין את שם התחום אותו יש לחסום 