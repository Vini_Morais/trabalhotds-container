��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  C     L   [     �  /   �  8   �  �  0	     �  .   �  :   $  :   _  A   �     �     �        B   8  4   {  �   �  4   6     k  .   z     �     �  
   �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-05-11 17:22+0000
Last-Translator: spacy01 <Unknown>
Language-Team: Bulgarian <bg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Появи се грешка по време на копиране. Възникна грешка при създаване на дял на %s. Възникна грешка. Грешка при идентификация. Изберете име на вашата USB карта FAT32 
  + Съвместим със всичко.
  - Не може да се справи с файлове, по-големи от 4ГБ.

exFAT 
  + Съвместим почти с всичко.
  + Може да се справи с файлове, по-големи от 4ГБ.
  - Не е чак толкова съвместим като FAT32.

NTFS 
  + Съвместим с Windows.
  - Не е съвместим с Mac и повечето хардуерни устройства.
  - Редки проблеми със съвместимостта с Линукс (NTFS е затворен и е използвано обратно проектиране.)

EXT4 
  + Модерен, стабилен, бърз, журнализиран.
  + Поддържа файловете разрешения на Линукс.
  - Не е съвместим с Windows, Mac и повечето хардуерни устройства.
 Форматиране Форматиране на USB картата Създаване на стартиращa USB карта Създаване на стартиращa USB карта Няма достатъчно място на USB паметта. Избор на образ Избор на USB карта: Избиране на образ USB паметта беше успешно форматирана. Образа беше успешно записан. Това ще изтрие всички данни от картата, наистина ли искате да продължите? Записване на образ на USB диск USB карта Форматиране на USB картата USB памет: Етикет на том: Запис непознат 