��    #      4  /   L           	          )     2     :     K  -   Y     �  >   �     �  
   �     �  
   �  	          
             .     >     F      N  0   o     �     �     �     �     �  	   �     �                &     B     J  �  ]     �  "        0     F  %   c  +   �  |   �     2  s   M     �  4   �     	     /	  !   O	     q	  %   �	     �	  1   �	     �	     
  Z   %
  q   �
  ?   �
  6   2     i  R   v  I   �  2     "   F  F   i     �     �     P  �  `        !   
                                            "                                       #             	                                                  Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Mounted Volumes Network Openbox Overview of some window managers Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Windows translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-03-09 02:57+0000
Last-Translator: Hatsune <Unknown>
Language-Team: Sinhalese <si@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 බොත්තම් නමය බොත්තම් හැඩය පරිගණකය මූලික තිරය වැඩතල සැකසුම් වැඩතල ප්‍රතිරැප ස්ථාන මරැ කරන අතරතුර කවුලුවේ අති දෑ නොපෙන්වන්න Fine-tune desktop settings පවතින කවුළු කළමනාකරුවන්ගේ විස්තරයක් මෙන්න මුල් පිටුව චායාරැවේ විශාලත්වය ප්‍රතිරැප අයිකන පමණයි අතුරුමුහුණත විශාල ලිනක්ස මින්ට් MAC ශෛලිය (වමට) භාවිතාවන දත්තතැටි ජාලය ඕපන්බොක්ස් කවුළු කළමනාකරුවන්ගේ දළ විශ්ලේෂණය ඔබට මුහුනත්තලයේ දිස්වීමට අවශ‍ය් දෑ තෝරන්න බොත්තම් හි රෑප පෙන්වන්න පෙලෙ හි රෑප පෙන්වන්න කුඩා අයිතම් වලට පහලින් අකුරැ යොදන්න අයිතම් වලට සමව අකුරැ යොදන්න අකුරැ පමණක් යොදන්න මෙවලම් තීරුව සම්ප්‍රදායික ශෛලිය (දකුණට) ඉවතලන බහලුම නම් තීරැව සදහා  සම්ප්‍රදායික අකුරැ භාවිතා කරන්න කවුළු Launchpad Contributions:
  Hatsune https://launchpad.net/~hatsune
  Nuwantha WANASINGHE | නුවන්ත වනසිංහ https://launchpad.net/~mail-nuwantha
  Prabhath Mannapperuma https://launchpad.net/~d-p-mannapperuma
  manjula https://launchpad.net/~manjula-mh
  පසිඳු කාවින්ද https://launchpad.net/~pkavinda
  වජිර කුරුප්පුආරච්චි https://launchpad.net/~vajira 