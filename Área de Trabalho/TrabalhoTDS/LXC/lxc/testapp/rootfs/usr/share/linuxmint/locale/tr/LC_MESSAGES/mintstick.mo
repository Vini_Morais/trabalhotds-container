��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  '   �  A   �     �     �          %     6     >     P      o  "   �  A   �     �         4     C     P     n     q          �     �     �     �     �     �  5   �  &        <  )   ?     i     x  !   {  !   �     �     �     �     
          4  
   O     Z     j     ~     �     �     �  *   �  E   �  #   %  (   I     r  E   �     �  /   �  *   $  %   O  5   u  8   �  A   �  (   &     O  W   n     �     �  
   �     �               $     7  6   @     w     �  -   �     �     �  
   �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-12 16:27+0000
Last-Translator: Sabri Ünal <Unknown>
Language-Team: Turkish <tr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Kalıp kopyalanırken bir hata oluştu. %s üzerinde disk bölümü oluştururken bir hata meydana geldi. Hata oluştu Bir hata oluştu. Kimlik Doğrulama Hatası Hesaplanıyor... Denetle Sağlama toplamı Checksum doğrulama dosyaları Checksum sağlaması eşleşmedi USB Bellek için bir isim seçiniz ISO kalıbını tekrar indirin. Checksum sağlaması eşleşmedi. Her şey yolunda görünüyor! FAT32 
  + Her yerde uyumlu.
  -  4GB.'tan daha büyük dosyaları işleyemez.

exFAT
  + Hemen hemen her yerde uyumlu.
  + 4GB'tan daha büyük dosyaları işleyebilir.
  -  FAT32 kadar uyumlu değil.

NTFS 
  + Windows ile uyumlu.
  -  Mac ve çoğu donanım aygıtıyla uyumlu değil.
  -  Linux ile nadir uyumluluk sorunları (NTFS tescillidir ve ters mühendislik ile destek var).

EXT4 
  + Modern, kararlı, hızlı, günlüklemeli.
  + Linux dosya izinlerini destekler.
  -  Windows, Mac ve çoğu donanım aygıtı ile uyumlu değil.
 Dosya sistemi: Biçimlendir Bir USB belleği biçimlendir GB GPG imzaları GPG imzalı dosya GPG imzalı dosya: Geri git ISO Doğrulama ISO kalıbı: ISO kalıpları ISO: İmzaya güveniyorsanız ISO'ya da güvenebilirsiniz. Bütünlük denetimi başarısız oldu KB Anahtar, anahtar sunucusunda bulunamadı. Yerel dosyalar MB Boot edebilir USB bellek hazırla Boot edebilir USB bellek hazırla Daha fazla bilgi Birim kimliği bulunamadı USB Bellekde yeterli alan yok. SHA256 toplamı SHA256 toplamlar dosyası SHA256 toplamlar dosyası: SHA256sum: Kalıbı Seçin USB Bellek seçiniz Bir kalıp dosyası seçiniz İmzalayan: %s Boyut: TB ISO kalıbının SHA256 toplamı yanlış. SHA256 toplamları dosyası, bu ISO kalıbı için toplam içermiyor. SHA256 toplamlar dosyası imzasız. USB Bellek başarıyla biçimlendirildi. Checksum sağlaması doğru Sağlama toplamı doğru ancak toplamın gerçekliği doğrulanmadı. gpg dosyası kontrol edilemedi. gpg dosyası indirilemedi. URL'yi kontrol edin. Kalıp dosyası başarıyla yazdırıldı. Toplamlar dosyası kontrol edilemedi. Toplamlar dosyası indirilemedi. URL'yi kontrol edin. Bu ISO kalıbı, güvenilir bir imza ile doğrulanmış. Bu ISO kalıbı, güvenilmeyen bir imza tarafından doğrulandı. Bu ISO, bir Windows kalıbına benziyor. Bu resmi bir ISO kalıbıdır. Bu işlem USB Bellek üzerindeki tüm verileri yok edecek, devam etmek istiyor musunuz? URL'ler USB Kalıp Yazıcı USB Bellek USB Bellek Biçimlendirici USB Bellek: Bilinmeyen imza Güvenilmeyen imza Doğrula Kalıbın gerçekliğini ve bütünlüğünü doğrula Birim etiketi: Birim: Windows kalıpları özel işleme gerektirir. Yaz bayt bilinmeyen 