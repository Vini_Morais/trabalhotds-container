��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  m     q   �  A   �  )   :	  m   d	  �  �	     �  J   �  `     `   h  y   �  :   C  S   ~  :   �  y     i   �  �   �  G   �  (   4  A   ]  )   �  )   �  	   �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-09-09 16:09+0000
Last-Translator: Sai Vinoba <Unknown>
Language-Team: Kannada <kn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ಪ್ರತಿಕೃತಿಯನ್ನು ನಕಲಿಸುವಾಗ ದೋಷ ಸಂಭವಿಸಿದೆ. %s ನಲ್ಲಿ ವಿಭಾಗವನ್ನು ರಚಿಸುವಾಗ ದೋಷ ಸಂಭವಿಸಿದೆ. ದೋಷವೊಂದು ಕಾಣಿಸಿಕೊಂಡಿದೆ. ದೃಢೀಕರಣ ದೋಷ. ನಿಮ್ಮ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಗಾಗಿ ಹೆಸರನ್ನು ಆರಿಸಿ FAT32
   + ಎಲ್ಲೆಡೆ ಹೊಂದಿಕೊಳ್ಳುತ್ತದೆ.
   - 4GB ಗಿಂತ ದೊಡ್ಡದಾದ ಕಡತಗಳನ್ನು ನಿಭಾಯಿಸಲಾಗದು.

exFAT
   + ಬಹುತೇಕ ಎಲ್ಲೆಡೆ ಹೊಂದಿಕೊಳ್ಳುತ್ತದೆ.
   + 4GB ಗಿಂತ ದೊಡ್ಡದಾದ ಕಡತಗಳನ್ನು ನಿಭಾಯಿಸಬಲ್ಲದು.
   - FAT32 ನಂತೆ ಹೊಂದಿಕೆಯಾಗುವುದಿಲ್ಲ.

NTFS
   + ವಿಂಡೋಸ್‌ನೊಂದಿಗೆ ಹೊಂದಿಕೊಳ್ಳುತ್ತದೆ.
   - ಮ್ಯಾಕ್ ಮತ್ತು ಹೆಚ್ಚಿನ ಯಂತ್ರಾಂಶ ಸಾಧನಗಳೊಂದಿಗೆ ಹೊಂದಿಕೆಯಾಗುವುದಿಲ್ಲ.
   - ಲಿನಕ್ಸ್‌ನೊಂದಿಗೆ ಸಾಂದರ್ಭಿಕ ಹೊಂದಾಣಿಕೆಯ ಸಮಸ್ಯೆಗಳು (ಎನ್‌ಟಿಎಫ್‌ಎಸ್ ಸ್ವಾಮ್ಯದ್ದು ಮತ್ತು ರಿವರ್ಸ್ ಎಂಜಿನಿಯರಿಂಗ್ ಆಗಿದೆ).

EXT4
   + ಆಧುನಿಕ, ಸ್ಥಿರ, ವೇಗದ, ಜರ್ನಲ್ಡ್.
   + ಲಿನಕ್ಸ್ ಕಡತ ಅನುಮತಿಗಳನ್ನು ಬೆಂಬಲಿಸುತ್ತದೆ.
   - ವಿಂಡೋಸ್, ಮ್ಯಾಕ್ ಮತ್ತು ಹೆಚ್ಚಿನ ಯಂತ್ರಾಂಶ ಸಾಧನಗಳೊಂದಿಗೆ ಹೊಂದಿಕೆಯಾಗುವುದಿಲ್ಲ.
 ರೂಪಿಸು ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯನ್ನು ರೂಪಿಸು ಸಜ್ಜುಶಕ್ತ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯನ್ನು ಮಾಡು ಸಜ್ಜುಶಕ್ತ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯನ್ನು ಮಾಡು ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯಲ್ಲಿ ಸಾಕಷ್ಟು ಸ್ಥಳಾವಕಾಶವಿಲ್ಲ. ಪ್ರತಿಕೃತಿಯನ್ನು ಆರಿಸಿ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯನ್ನು ಆಯ್ಕೆಮಾಡಿ ಪ್ರತಿಕೃತಿಯನ್ನು ಆರಿಸಿ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯನ್ನು ಯಶಸ್ವಿಯಾಗಿ ರೂಪಿಸಲಾಗಿದೆ. ಪ್ರತಿಕೃತಿಯನ್ನು ಯಶಸ್ವಿಯಾಗಿ ಬರೆಯಲಾಗಿದೆ. ಇದು ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿಯಲ್ಲಿರುವ ಎಲ್ಲಾ ದತ್ತಾಂಶಗಳನ್ನು ನಾಶಪಡಿಸುತ್ತದೆ, ನೀವು ಮುಂದುವರಿಯಲು ಬಯಸುತ್ತೀರಾ? ಯುಎಸ್‌ಬಿ ಪ್ರತಿಕೃತಿ ಬರಹಗಾರ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿ ರೂಪಿಸುವವ ಯುಎಸ್‌ಬಿ ಕಡ್ಡಿ: ಸಂಪುಟ ಶೀರ್ಷಿಕೆ: ಬರೆ ಅಜ್ಞಾತ 