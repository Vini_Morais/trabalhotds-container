��    :      �  O   �      �     �     �            %   !     G     \     e     {     �     �     �     �  	   �     �  	   �     �     �            -   0     ^     e     z     �     �     �     �     �     �     �     �     �     �     �                      $   +     P     W     m     �     �     �     �     �     �  '        0     A     H     Q     X  	   ^     h  �  }  !   
  
   0
  !   ;
     ]
  T   r
  :   �
       !        7  R   W  ?   �     �  "   �          5     S     j     ~     �  "   �  \   �     2  (   C     l  N   �     �     �                 '     /  &   <     c     z     �     �     �  
   �  %   �  G   �     >  )   K  M   u  7   �     �  6     .   R  :   �  -   �  j   �  %   U     {     �     �  
   �     �  0   �                
   7                                     !   1   ,         8   2   5   4   *   -          "   6          #   0   9   &   /                         '             %       	             (          +                                :              )           3   .         $    About All All applications Applications Browse and install available software Browse deleted files Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Places Preferences Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-05-04 23:18+0000
Last-Translator: SimpleLeon <Unknown>
Language-Team: Kirghiz <ky@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Программа жөнүндө Баары Бардык тиркемелер Тиркемелер Жеткиликтүү программа жабдыгын көрүп орнотуу Өчүрүлгөн файлдарды серепчилөө Компьютер Системаны ырастоо Башкаруу борбору Плагинди инициализациялоо оңунан чыккан жок Плагинди жүктөө оңунан чыккан жок: Иш столу Иш столунун темасы Менюну оңдоо Себетти тазалоо Тандалмалар Үй папкасы Бөлгүчтү коюу Боштукту коюу '%s' пакетин орнотуу Орнотуу, өчүрүү жана программа пакеттерин жаңылоо Жүргүзүү Мен киргенде жүргүзүү Экранды бөгөттөө Сеансты аяктоо же колдонуучуну алмаштыруу Сеансты аяктоо Меню Меню ырастоолору Аты Аты: Тармак Жеке папкаңызды ачуу Параметрлер Пакет менеджери Жолу Орундар Ырастоолор Чыгуу Плагиндерди жаңылоо Соңку категорияны же издөө эске сактоо Өчүрүү Тандалмалардан өчүрүү Бөгөттөөнү алуу үчүн сырсөз талап кылынат Орнотуу үчүн пакеттерди издөө Папканы тандаңыз Бардык тиркемелерди көрсөтүү Кнопка значогун көрсөтүү Категория значокторун көрсөтүү Тандалмалардан көрсөтүү Өчүрүү, кайтадан жүктөө, бир азга токтотуу же уктоо режими Программа менеджери Система Терминал Темасы: Себет Өчүрүү Командалык сабын колдонуу 