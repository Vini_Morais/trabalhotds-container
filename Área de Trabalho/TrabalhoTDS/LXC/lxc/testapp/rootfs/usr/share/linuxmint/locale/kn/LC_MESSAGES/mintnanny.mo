��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  <        >  L   �  +     "   7  �  Z  +   �  %     .   E  �   t                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-09-29 11:08+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: Kannada <kn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ಮಾನ್ಯ ಡೊಮೇನ್ ಹೆಸರಲ್ಲ. ಆಯ್ದ ಡೊಮೇನ್ ಹೆಸರುಗಳಿಗೆ ಪ್ರವೇಶವನ್ನು ನಿರ್ಬಂಧಿಸು ನಿರ್ಬಂಧಿಸಲ್ಪಟ್ಟ ಡೊಮೇನ್‌ಗಳು ಡೊಮೇನ್ ನಿರ್ಬಂಧಕ ಡೊಮೇನ್ ಹೆಸರು ಡೊಮೇನ್ ಹೆಸರುಗಳು ಅಕ್ಷರ ಅಥವಾ ಅಂಕಿಯೊಂದಿಗೆ ಪ್ರಾರಂಭವಾಗಬೇಕು ಮತ್ತು ಕೊನೆಗೊಳ್ಳಬೇಕು ಮತ್ತು ಅಕ್ಷರಗಳು, ಅಂಕೆಗಳು, ಚುಕ್ಕೆಗಳು ಮತ್ತು ಹೈಫನ್‌ಗಳನ್ನು ಮಾತ್ರ ಒಳಗೊಂಡಿರಬಹುದು. ಉದಾಹರಣೆ: my.number1domain.com ಅಮಾನ್ಯ ಡೊಮೇನ್ ಪೋಷಕರ ನಿಯಂತ್ರಣ ದಯವಿಟ್ಟು ನೀವು ನಿರ್ಬಂಧಿಸಲು ಬಯಸುವ ಡೊಮೇನ್ ಹೆಸರನ್ನು ಟೈಪ್ ಮಾಡಿ 