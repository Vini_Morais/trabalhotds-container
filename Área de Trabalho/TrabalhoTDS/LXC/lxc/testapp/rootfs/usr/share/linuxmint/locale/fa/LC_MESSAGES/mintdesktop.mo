��    2      �  C   <      H     I     Y     i  �   p  P        b     j     s     {     �  -   �     �  >   �     "  
   '     2  
   8  X   C  ^   �  Z   �  X   V  	   �     �  
   �     �     �  �   �     t  �   }     	     '	  ~   .	     �	      �	  0   �	     
     
     1
     7
     H
  	   Z
     d
     m
     �
     �
     �
     �
  �   �
     J  �  ]  #   �          7    D     X     �     �     �             ^   >  4   �  V   �     )     2     J  #   \  ~   �  �   �  }   �  y        }     �     �     �  
   �  �   �     �  �   �  #   �     �  �   �     Z  8   c  x   �  1     ,   G     t  -   }  /   �     �     �          )  @   =     ~     �  �   �  �   k     	            !   (               +             "   %       .      #                    &   '               2   0          1          ,   )   $                      *              
              /          -                               Buttons labels: Buttons layout: Compiz Compiz is a very impressive window manager, very configurable and full of amazing effects and animations. It is however the least reliable and the least stable. Compiz is installed by default in Linux Mint. Note: It is not available in LMDE. Compton Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only If Marco is not installed already, you can install it with <cmd>apt install marco</cmd>. If Metacity is not installed already, you can install it with <cmd>apt install metacity</cmd>. If Mutter is not installed already, you can install it with <cmd>apt install mutter</cmd>. If Xfwm4 is not installed already, you can install it with <cmd>apt install xfwm4</cmd>. Interface Large Linux Mint Mac style (Left) Marco Marco is the default window manager for the MATE desktop environment. It is very stable and very reliable. It can run with or without compositing. Metacity Metacity was the default window manager for the GNOME 2 desktop environment. It is very stable and very reliable. It can run with or without compositing. Mounted Volumes Mutter Mutter is the default window and compositing manager for the GNOME 3 desktop environment. It is very stable and very reliable. Network Overview of some window managers Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Windows Xfwm4 Xfwm4 is the default window manager for the Xfce desktop environment. It is very stable and very reliable. It can run with or without compositing. translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-04-27 14:35+0000
Last-Translator: MohammadSaleh Kamyab <Unknown>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 برچسب‌های دکمه‌ها آرایش دکمه‌ها کامپیز کامپیز یک مدیر پنجرهٔ بسیار چشمگیر است. بسیار قابل تنظیم و پر از جلوه‌ها و پویانمایی‌های شگفت‌انگیز است. با این حال کم‌ترین اطمینان و پایداری را دارد. کامپیز به طور پیش‌گزیده در لینوکس مینت نصب است. توجه: در LMDE موجود نیست. کامپتون رایانه میزکار تنظیمات میزکار آیکون‌های میزکار محتوای پنجره‌ها را در هنگام جابجایی آن‌ها نشان نده تنظیمات ریز-کوک‌کردن میزکار این‌جا توضیح بعضی از مدیر پنجره‌های موجود است. خانه اندازه آیکون آیکون‌ها فقط نمایش آیکون‌ها اگر مارکو هم‌اکنون نصب نیست، می‌توانید با <cmd>apt install marco</cmd> نصبش کنید. اگر متاسیتی هم‌اکنون نصب نیست، می‌توانید با <cmd>apt install metacity</cmd> نصبش کنید. اگر ماتر هم‌اکنون نصب نیست، می‌توانید با <cmd>apt install mutter</cmd> نصبش کنید. اگر Xfwm4 هم‌اکنون نصب نیست، می‌توانید با <cmd>apt install xfwm4</cmd> نصبش کنید. واسط بزرگ لینوکس مینت سبک مکینتاش (چپ) مارکو مارکو مدیر پنجرهٔ پیش‌گزیدهٔ میزکار ماته است. بسیار پایدار و قابل اعتماد است. می‌تواندهمراه یا بدون ترکیب کردن اجرا شود. متاسیتی متاسیتی مدیر پنجرهٔ پیش‌گزیدهٔ میزکار گنوم ۲ بود. بسیار پایدار و قابل اعتماد است. می‌تواند همراه یا بودن ترکیب کردن اجرا شود. درایو‌های نصب‌شده ماتر ماتر مدیر پنجرهٔ پیش‌گزیدهٔ میزکار گنوم ۳ است. بسیار پایدار و قابل اعتماد است. شبکه نمای کلی بعضی از مدیر پنجره‌ها گزینه هایی که می خواهید در میزکار مشاهده نمایید، را انتخاب نمایید: نمایش آیکون‌ها در دکمه‌ها نمایش آیکون‌ها در منوها کوچک نمایش متن در زیر دکمه‌ها نمایش متن در کنار دکمه‌ها فقط نمایش متن نوارهای ابزار سبک سنتی (راست) زباله‌دان استفاده از فونت سیستم در نوار عنوان پنجره‌ها Xfwm4 Xfwm4 مدیر پنجرهٔ پیش‌گزیدهٔ میزکار Xfce است. بسیار پایدار و قابل اعتماد است. می‌تواند همراه یا بدون ترکیب کردن اجرا شود. Launchpad Contributions:
  Masoud Abdar https://launchpad.net/~masoud.abdar
  MohammadSaleh Kamyab https://launchpad.net/~mskf1383
  Sari Galin https://launchpad.net/~mehdi-teymourlouie
  m.soleimani https://launchpad.net/~esf-m-sol 