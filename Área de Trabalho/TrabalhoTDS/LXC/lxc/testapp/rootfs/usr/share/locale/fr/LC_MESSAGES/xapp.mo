��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �     	   �     �     �     �  0   �  	   0     :     B  
   N     Y     o     �  	   �  	   �     �     �     �     �     �     �  
   �     �     �                    '     5     T     Z  "   t     �  "   �  &   �     	  !   	            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-25 11:37+0000
Last-Translator: Clement Lefebvre <root@linuxmint.com>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 À propos Actions Autoriser les chemins d'accès Applications Zone où les icônes d’état XApp apparaissent Parcourir Annuler Catégories Catégorie Choisissez une icône Valeurs par défaut Icône par défaut Appareils Emblèmes Emoji Favoris Icône Taille de l'icône Image Chargement... Types MIME Ouvrir Opération non supportée Autre Lieux Chercher Sélectionner Sélectionner un fichier image État La catégorie par défaut L’icône à utiliser par défaut La taille d'icône préférée. La chaîne représentant l'icône. Autoriser ou non les chemins d'accès. Applet d’état XApp Fabrique d'applets d’état XApp 