��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  %   {  2   �     �     �     �               (     ;     X     s  C   �     �  �  �     �     �     �     �     �               5     D  
   Q  
   \     g  1   l     �     �  *   �     �     �  &   �  &   "     I     [  $   z     �     �     �  
   �     �     �     �     
  
        %  (   (  :   Q  6   �  -   �     �  E     '   V  <   ~     �  9   �  O     4   e  6   �  -   �     �  F        d     h     z     �  	   �     �     �     �  #   �            ;        X     _     f     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-10-13 12:03+0000
Last-Translator: menom <Unknown>
Language-Team: Slovak <sk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Nastala chyba pri kopírovaní obrazu Vyskytla sa chyba pri vytváraní partície na %s. Nastala chyba Došlo k chybe. Chyba overovania. Prebieha výpočet... Skontrolovať Kontrolný súčet Súbory kontrolného súčtu Chyba kontrolného súčtu Zvoľte meno pre váš USB disk Stiahnite si ISO obraz znova. Jeho kontrolný súčet sa nezhoduje. Všetko vyzerá dobre! FAT32 
  +Kompatibilný zo všetkým.
  -Nezvláda súbory väčšie ako 4GB.

exFAT
  +Kompatibilný takmer zo všetkým.
  +Zvláda súbory väčšie než 4GB.
  -Nie je kompatibilný z FAT32.

NTFS 
  +Kompatibilný z Windowsom.
  -Nefunguje na Mac a vačšine hardvéru.
  -Linux s ním má občas problémy.

EXT4 
  +Moderný, stabilný, rýchly.
  +Podporuje Linuxové nastavenia súborov.
  -Nie je kompatibilný z Windowsom, Mac a vačšinou hardvéru.
 Súborový systém: Formátovanie Naformátovať USB disk GB GPG podpisy Podpísaný GPG súbor Podpísaný GPG súbor: Prejsť späť Overenie ISO ISO obraz: ISO obrazy ISO: Ak dôverujete podpisu, môžete dôverovať ISO. Kontrola integrity zlyhala KB Kľúč sa nenašiel na serveri kľúčov. Lokálne súbory MB Vytvoriť spustiteľný USB flash disk Vytvoriť spustiteľný USB flash disk Viac informácií Nenašlo sa žiadne ID zväzku Nedostatok miesta na USB flash disku Suma SHA256 Súbor súm SHA256 Súbor súm SHA256: SHA256sum: Zvoľte obraz Vyberte USB disk Zvoľte obraz Podpísaný: %s Veľkosť: TB Súčet SHA256 obrazu ISO je nesprávny. Súbor súm SHA256 neobsahuje súčet pre tento obraz ISO. Súbor kontrolného súčtu SHA256 nie je podpísaný. USB flash disk bol úspešne naformátovaný. Kontrolný súčet je správny Kontrolný súčet je správny, ale pravosť súčtu nebola overená. Súbor gpg sa nepodarilo skontrolovať. Súbor gpg sa nepodarilo stiahnuť. Skontrolujte adresu URL. Obraz bol úspešne zapísaný. Súbor kontrolných súčtov sa nepodarilo skontrolovať. Súbor s kontrolným súčtom sa nepodarilo stiahnuť. Skontrolujte adresu URL. Tento obraz ISO je overený dôveryhodným podpisom. Tento obraz ISO je overený nedôveryhodným podpisom. Tento ISO vyzerá ako obraz systému Windows. Toto je oficiálny obraz ISO. Všetky dáta na USB flash disku budú zničené, chcete pokračovať? URL Tvorca USB obrazu USB flash disk Formátovanie USB flash disku USB disk: Neznámy podpis Nedôveryhodný podpis Overiť Overiť pravosť a integritu obrazu Názov média: Zväzok: Obrazy systému Windows vyžadujú špeciálne spracovanie. Zápis bajtov neznáme 