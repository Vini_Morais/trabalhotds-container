��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  G     H   U  +   �  ,   �  %   �                -     9     >     T  C   q     �     �  k   �     J     ^  ;   c  0   �     �     �     �  	   �     �     
  
             +     C  f   Q  Q   �     
          $  	   -     7  
   T     _     x  F   �     �     �     �  $   �               &  #   A  W   e     �     �     �     �  	   �     �  =   �  1   =     o     }     �     �  	   �      �     �     �  D   �  
   +     6     ?     H  C   ]  9   �  6   �  $        7     J     ^     c     p  :   �  @   �           %     +     7         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-07 14:53+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s a discargar, %(localSize)s de spatio de disco liberate %(downloadSize)s a discargar, %(localSize)s de spatio de disco necessari %(localSize)s del spatio del disco liberate %(localSize)s del spatio del disco necessari %d labor currente %d labores currente 3D A  proposito Accessorios Toto Tote le applicationes Tote le operationes complete Un error occurreva al tentativa de adder le repositorio de Flatpak. Jocos de tabuliero Impossibile installar Impossibile processar iste file durante que il ha operationes active.
Per prova ancora post que illos fini. Impossibile remover Chat Clicca <a href='%s'>here</a> pro adder tu proprie revision. Actualmente on labora sur le pacchettos sequente Detalios Designo Picks del Editores Education Electronica Email Emulatores Cosas essential Compartimento del files Prime-persona Le supporto de flatpak non es actualmente disponibile. Prova a installar flatpak e gir1.2-flatpak-1.0. Le supporto de flatpak non es actualmente disponibile. Prova a installar flatpak. Typos de charattere Jocos Graphica Installar Installar nove applicationes Installate Applicationes installate Installation Installar iste pacchetto pote damnificar irreparabilemente tu systema. Internet Java Lancear Limitar le recerca al lista sequente Mathematica Codecs multimedial Codecs multimedial pro KDE Nulle pacchetto concordante trovate Nulle pacchettos a monstrar
Isto pote indicar un problema - prova a refrescar le cache. Non disponibile Non installate Officio PHP Pacchetto Photographia Porta patientia per favor. Illo pote prender alcun tempore... Per favor usa apt-get pro installar ce pacchetto. Programmation Publication Python Strategia de tempore real Refrescar Refrescar le lista de pacchettos Remover Remotion Remover iste pacchetto pote damnificar irreparabilemente tu systema. Revisiones Scansion Scientia Scientia e education Cercar in le description del pacchettos (recerca ancora plus lente) Cercar in le summario del pacchettos (recerca plus lente) In recerca tra le repositorios de software, un momento Monstrar le applicationes installate Simulation e cursa Gestor de  software Sono Sono e video Instrumentos de systema Le repositorio de Flatpak que tu tenta de adder existe ja. Actualmente il ha operationes active.
Desira tu vermente quitar? Strategia basate sur le rotation Video Spectatores Web 