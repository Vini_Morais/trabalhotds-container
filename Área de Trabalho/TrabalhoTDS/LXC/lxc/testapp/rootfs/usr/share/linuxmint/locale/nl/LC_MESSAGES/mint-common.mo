��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  ,      .   M  )   |  .   �  /   �  0     E   6  2   |  ,   �  /   �     	  L   &	     s	     	     �	     �	  ;   �	     �	  4   �	  $   &
     K
     Z
     f
  /   }
  _   �
  *        8                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 15:58+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Er zal %s meer schijfruimte worden gebruikt. Er zal %s aan schijfruimte worden vrijgemaakt. Er zal in totaal %s worden binnengehaald. Er moeten extra wijzigingen worden aangebracht Er zal extra programmatuur worden afgewaardeerd Er zal extra programmatuur worden geïnstalleerd Er zal extra programmatuur worden verwijderd (inclusief instellingen) Er zal extra programmatuur worden geherinstalleerd Er zal extra programmatuur worden verwijderd Er zal extra programmatuur worden opgewaardeerd Er is een fout opgetreden Kan pakket %s niet verwijderen aangezien het nodig is voor andere pakketten. Afwaarderen Flatpak Flatpaks Installeren Pakket %s is een afhankelijkheid van de volgende pakketten: Te verwijderen pakketten Bekijk a.u.b. de onderstaande lijst van wijzigingen. Verwijderen (inclusief instellingen) Herinstalleren Verwijderen Opwaardering overslaan De volgende pakketten zullen verwijderd worden: Dit menu-onderdeel is niet gekoppeld aan enig pakket. Wilt u het toch uit het menu verwijderen? Actualiseringen zullen worden overgeslagen Opwaarderen 