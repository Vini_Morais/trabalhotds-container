��    )      d  ;   �      �     �     �     �     �     �     �     �     �  -   �     &  >   A     �  
   �     �  
   �  	   �     �  
   �     �     �  �   �     f     o          �      �  0   �     �     �               "  	   4     >     G     a     g     �     �     �  �  �  )   H  )   r     �     �     �     �  .   �  +   &	  m   R	  Q   �	  u   
  	   �
     �
     �
  %   �
     �
     �
  "     +   /     [  �  q     �  >        Q     g  [   �  |   �  >   Z  A   �     �  2   �  8     (   T  $   }  =   �     �  b   �     S  2   i  �   �                               '                  "                       #            
   %              	                    !   $      (   &             )                                                     Buttons labels: Buttons layout: Compiz Compton Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Here's a description of some of the window managers available. Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Marco Marco is the default window manager for the MATE desktop environment. It is very stable and very reliable. It can run with or without compositing. Metacity Mounted Volumes Network Openbox Overview of some window managers Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Windows root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-02-03 12:57+0000
Last-Translator: Siddharth Belbase <Unknown>
Language-Team: Nepali <ne@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 बटनका लाबेलहरु: बटनहरुको लेआउट: कम्पिज कम्पटोन कम्प्युटर डेस्कटप डेस्कटप सेटिङहरु डेस्कटप आइकनहरु केहि सार्दा त्यसको सामग्री नदेखाउनुहोला डेस्कटपक सेटिङहरु राम्रो बनाउ यहाँ झ्याल उपलब्ध प्रबन्धकहरू केही विवरण छ। गृह आइकन आकार: आइकन्स आइकनहरु मात्र इन्टरफेस ठूलो लिनक्स मिन्ट माक शैली (देब्रे) म्याकरो म्यार्को को मेट डेस्कटप वातावरणको लागि पूर्वनिर्धारित सञ्झ्याल प्रबन्धक हो। यो धेरै स्थिर र धेरै विश्वसनीय छ। यो वा कोम्पोसिटिङ बिनानै चल्न सक्छ। मेटासिटी माउन्ट गरिएका भोलुमहरू नेटवर्क खुला बाकस केही विन्डो प्रबन्धकहरू को अवलोकन डेस्कटपमा देख्न चाहनुहुने सामग्रीहरु चुन्नुस बटनहरुमा आइकनहरु देखाऊ मेनुहरुमा आइकनहरु देखाऊ सानो सामग्रीका तल अक्षर सामग्रीका संगै अक्षर अक्षरहरु मात्र औजारपट्टीहरू परम्परागत शैली (दायिने) टोकरी शीर्षक पट्टीमा सिस्टम फन्ट प्रयोग गर विन्डोज रूट@लिनक्समिन्ट.कम Launchpad Contributions:
  Niroj Manandhar https://launchpad.net/~nirose
  Siddharth Belbase https://launchpad.net/~thatssid.-deactivatedaccount 