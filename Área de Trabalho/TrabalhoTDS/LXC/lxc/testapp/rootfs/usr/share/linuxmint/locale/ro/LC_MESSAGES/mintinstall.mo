��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  K   L  K   �  -   �  *     :   =     x     {  	   �     �     �     �  D   �     
       z   /     �     �  =   �  $        &     .     4  	   I     S     `  
   g  
   r     }     �  f   �  O        X     `     g     p     |     �     �     �  M   �            	      *   *     U     a     u  !   �  n   �     %  
   2     =     Q     U  
   \  ;   g  4   �  
   �     �     �               ,     K     T  M   `     �     �  
   �     �  :   �  3     )   R     |     �     �     �     �     �  H   �  D   9     ~     �     �     �         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-05-17 10:35+0000
Last-Translator: Daniel Slavu <Unknown>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1 ? 0: (((n % 100 > 19) || ((n % 100 == 0) && (n != 0))) ? 2: 1));
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s de descărcat, %(localSize)s eliberat din spațiul pe disc %(downloadSize)s de descărcat, %(localSize)s necesari din spațiul pe disc %(localSize)s eliberați din spațiul pe disc %(localSize)s necesar din spațiul pe disc %d task rulează %d task-uri rulează %d task-uri rulează 3D Despre Accesorii Toate Toate aplicaţiile Toate operațiile sunt complete A intervenit o eroare încercând să se adauge repertoriul Flatpak. Jocuri pe tablă Nu se poate instala Acest fișier nu poate fi procesat cât timp alte operații sunt active.
Te rog încearcați din nou după terminarea lor. Nu se poate elimina Chat Clic <a href='%s'>aici</a> pentru a adăuga propria evaluare. Se lucrează la următoarele pachete Detalii Desen Selecția editorului Educație Electronică E-mail Emulatoare Esențiale Partajare fișiere Persoana întâi Suportul Flatpak nu este disponibil momentan. Încercăi să instalezi flatpak și gir1.2-flatpak-1.0. Suportul Flatpak nu este disponibil momentan. Încearcă să instalezi flatpak. Fonturi Jocuri Grafică Instalează Instalează aplicații noi Instalat Aplicații instalate Se instalează Instalarea acestui pachet ar putea provoca daune ireparabile sistemului tău. Internet Java Lansează Limitează căutarea la listările curente Matematică Codecuri multimedia Codecuri multimedia pentru KDE Nu s-au găsit pachetele căutate Nu există pachete de afișat.
Acest lucru poate indica o problemă - încercă să actualizezi memoria cache. Indisponibil Neinstalat Aplicații de birou PHP Pachet Fotografie Te rog să ai răbdare. Acest lucru poate dura ceva timp... Utilizați apt-get pentru instalarea acestui pachet. Programare Editare de publicații Python Strategie în timp real Reîmprospătează Actualizează lista de pachete Elimină Se elimină Eliminarea acestui pachet ar putea provoca daune ireparabile sistemului tău. Recenzii Scanare Știință Știință și educație Caută în descrierea pachetelor (căutare și mai lentă) Caută în sumarul pachetelor (căutare mai lentă) Se caută depozite de software, un moment Arată aplicațiile instalate Simulare și curse Administrator software Sunet Audio și video Instrumente de sistem Repertoriul Flatpak pe care încercați să îl adăugați există deja. Sunt acum operații în derulare.
Ești sigur că vrei să renunți? Strategie cu rândul Video Vizualizatoare Web 