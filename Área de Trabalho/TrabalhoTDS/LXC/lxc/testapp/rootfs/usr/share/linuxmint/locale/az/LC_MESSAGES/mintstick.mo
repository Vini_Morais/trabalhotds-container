��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  4     9   R     �  !   �  '   �  H  �     4  "   B  ,   e  /   �  "   �     �     �           $     E  Y   d     �     �     �               $     (                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2018-08-15 19:55+0000
Last-Translator: Arif <mdyos@inbox.ru>
Language-Team: Azerbaijani <az@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Görünüş yazılışında yanlışlıq baş verdi. %s'da bir bölüm yaradılarkən yanlışlıq baş verdi. Yanlış baş verdi. Kimlik Doğrulanması Yanlışı. USB yaddaşınız üçün bir ad seçin FAT32 
  + Bütün yerdə uyumludur.
  -  4GB'dan çox düzümləri götürmür.

exFAT
  + Demək olar, bütün yerdə uyumludur.
  + 4GB'dan çox düzümləri götürür.
  -  FAT32 kimi uyumlu deyil.

NTFS 
  + "Windows" ilə uyumludur.
  -  "Mac" ilə qurğuların bir çoxu ilə uyumsuzdur.
  -  Arabir Linuks ilə uyumluqda çətinliklər yarana bilər (NTFS özəldir, qarşı işlətməçi işidir).

EXT4 
  + Çağdaş, durğun, sürətli, dərgiləndirilmişdir.
  + Linuks düzüm yolverimlərini dəstəkləyir.
  -  "Windows", "Mac", daha çox qurğularla uyumlu deyil.
 Biçimləndir Bir USB yaddaşını biçimləndir Ön-başlana bilən bir USB yaddaş düzəlt Ön-başlana bilən bilən USB yaddaş düzəlt Bu USB yaddaşda yer yetərsizdir. Görünüşü Seç Bir USB yaddaşı seç Bir görünüş seç USB yaddaş uğurla biçimləndi Görünüş uğurla yazıldı. Bu, USB yaddaşın bütün verilənini yox edəcək, bunu  sürdürmək istəyirsiniz mi? USB Görünüş Yazıcısı USB Yaddaş USB Yaddaş Biçimləndiricisi USB yaddaş: Tutum yarlığı: Yaz Bilinməyən 