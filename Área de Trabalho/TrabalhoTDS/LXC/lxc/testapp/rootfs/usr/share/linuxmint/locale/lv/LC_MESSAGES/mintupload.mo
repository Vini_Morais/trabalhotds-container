��    '      T  5   �      `     a  6   o  4   �     �     �     �     �     �  #        %  $   <     a     d     h  !   n     �     �     �     �  	   �     �     �  7   �     �  	   �  !     (   &  
   O     Z     `     e     t     �     �     �  )   �     �     �  �  �     �  E   �  K        f     j     l     s     �  /   �  &   �  6   �     "	     %	     )	  6   9	     p	     s	     w	     z	     ~	     �	     �	  L   �	     �	     �	  !   
  5   (
     ^
     m
     s
     x
      �
     �
     �
     �
  C   �
     7     >            $                 &   	                                             !                             
         "            '          %      #                                  %s Properties <b>Please enter a name for the new upload service:</b> <i>Try to avoid spaces and special characters...</i> About B Cancel Check connection Close Could not save configuration change Define upload services Drag &amp; Drop here to upload to %s GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password: Path: Port: Remote port, default is 21 for FTP, 22 for SFTP and SCP Service name: Services: This service requires a password. Timestamp format (strftime). By default: Timestamp: Type: URL: Upload Manager Upload manager... Upload services Uploading the file... User: Username, defaults to your local username _File _Help Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-07-27 03:34+0000
Last-Translator: tuxmaniack <Unknown>
Language-Team: Latvian <lv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s īpašības <b>Lūdzu, ievadiet jaunā augšupielādes pakalpojuma nosaukumu:</b> <i>Centieties izvairīties no atstarpēm un īpašajām rakstzīmēm...</i> Par B Atcelt Pārbaudīt savienojumu Aizvērt Nevar saglabāt konfigurācijas faila izmaiņas Kontrolē augšupielāžu pakalpojumus Velciet un nometiet šeit, lai augšupielādētu uz %s GB GiB Saimniekdators: Saimniekdatora nosaukums vai IP adrese, noklusējums:  KB KiB MB MiB Parole: Ceļš: Ports: Attālinātais ports, noklusējums ir 21 priekš FTP, 22 priekš SFTP un SCP Pakalpojuma nosaukums: Pakalpojumi: Šis pakalpojums pieprasa paroli. Laika zīmoga formāts (strftime). Pēc noklusējuma: Laika zīmogs: Tips: URL: Augšupielāžu pārvaldnieks Augšupielāžu pārvaldnieks... Augšupielāžu pakalpojumi Augšuplādē failu Lietotājs: Lietotājvārds, pēc noklusējuma jūsu vietējais lietotājvārds _Fails _Palīdzība 