��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
  	   �     �     �     �     �     �  d   �  4   F  7   {     �  1   �  U     	   Y     c     {     �     �     �     �     �     �               -     C     U  <   s     �     �     �     �                    /  	   8     B      J  
   k     v     �     �     �     �  M   �  D   
     O     V  -   r     �     �  #   �  &   �       !   #     E  !   b     �  0   �     �  .   �               #     )     2     >     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-03-05 15:15+0000
Last-Translator: Ueliton <Unknown>
Language-Team: Tagalog <tl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <not set> Tungkol dito MATE Menu Buong Lahat Lahat ng mga Application Mga Aplikasyon Tingnan ang lahat ng local at remote na mga disk at mga folder na kayang i-access ng computer na ito Maghanap at mag-install ng mga available na software Tingnan ang mga nai-bookmark na local network locations Tingnan ang mga binurang file Tingnan ang mga item na inilagay sa pook ng likha I-click upang magtakda ng isang bagong shortcut key upang buksan at isara ang menu.   Kompyuter Ayusin ang iyong system Kontrol Sentro Hindi ma-initialize ang plugin Hindi mai-load ang plugin: Pook ng likha Tema ng pook ng likha Baguhin ang menu Linisin ang basura Mga Paborito Pangsariling Polder Maglagay ng separator Maglagay ng space I-install ang package na '%s' I-install, tanggalin at i-upgrade ang mga pakete ng software Ibunsod Simulan kapag nag-log in ako I-lock ang Screen Mag-logout o magpalit ng user Mag-sign Out Menu Kagustuhan para sa menu Pangalan Pangalan: Network Buksan ang iyong personal folder Mga Opsyon Manager ng Pakete Landas Pumili ng aselerador Mga Pook Mga Kagustuhan Pindutin ang Backspace key upang i-clear ang umiiral na shortcut sa keyboard. Pindutin ang Esc o i-click muli upang kanselahin ang pagpapatakbo.   Umalis I-karga muli ang mga plugin Tandaan huling kategorya o gawin pananaliksik Alisin Tanggalin sa mga paborito Kailangan ng password para mabuksan Hanapin ang mga pakete upang i-install Pumili ng isang folder Ipakita lahat ang mga application Ipakita ang icon ng pindutan Ipakita ang mga icon ng kategorya Ipakita sa mga paborito I-shutdown, i-restart, i-suspend o mag-hibernate Tagapamahala ng Pakete Pagpalitin ang pangalan at generic na pangalan System Terminal Tema: Buntunan I-uninstall Gamitin ang command line 