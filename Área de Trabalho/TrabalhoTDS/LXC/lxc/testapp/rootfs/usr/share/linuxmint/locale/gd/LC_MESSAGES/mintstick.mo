��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  K   �  I   *     t     �  %   �     �     �  )   �  )     +   9     e     t     �  !   �  "   �  s   �     L  
   k     v     �     �     �     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-11-07 01:38+0000
Last-Translator: Akerbeltz <fios@akerbeltz.org>
Language-Team: Gaelic; Scottish <gd@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Thachair mearachd fhad 's a bha sinn a' dèanamh lethbhreac dhen ìomhaigh. Thachair mearachd fhad 's a bha sinn a' cruthachadh pàirteachadh air %s. Thachair mearachd. Mearachd dearbhaidh. Tagh ainm airson a' bhiorain USB agad Fòrmataich Fòrmataich bioran USB Cruthaich bioran USB a ghabhas a bhùtadh Cruthaich bioran USB a ghabhas a bhùtadh Chan eil àite gu leòr air a' bhioran USB. Tagh ìomhaigh Tagh bioran USB Tagh ìomhaigh Chaidh am bioran USB fhòrmatadh. Chaidh an ìomhaigh a sgrìobhadh. Sgriosaidh seo an dàta air fad air a' bhioran USB, a bheil thu cinnteach gu bheil thu airson leantainn air adhart? Sgrìobhadair ìomhaighean USB Bioran USB Fòrmataiche bioran USB Bioran USB: Leubail an draibh: Sgrìobh 