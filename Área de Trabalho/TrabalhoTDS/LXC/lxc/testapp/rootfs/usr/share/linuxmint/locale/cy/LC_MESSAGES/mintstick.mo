��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  '   �  )   �     �  	   �     �     	          #     ,     >  $   [  C   �     �  '  �     	  	         *     A     D     S     q     �     �     �     �     �  B   �            -   "     P     _     b     �     �     �  &   �  
   �     �       
   !     ,     :     O     a     t     {  *   ~  I   �  (   �  4        Q  E   i  !   �  :   �  )     !   6  :   X  >   �  @   �  ,     "   @  ]   c     �     �     �     �     �               -  +   4     `     o  1   w  
   �     �     �     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-29 10:00+0000
Last-Translator: Rhoslyn Prys <Unknown>
Language-Team: Welsh <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: cy
 Digwyddod gwall wrth gopïo'r ddelwedd. Digwyddodd gwall wrth greu rhaniad ar %s. Digwyddodd gwall Bu gwall. Gwall Dilysu. Wrthi'n cyfrifo... Gwirio Checksum Ffeiliau checksum Diffyg cyfatebiaeth checksum Dewis enw ar gyfer eich cof bach USB Llwythwch y ddelwedd ISO i lawr eto. Nid yw ei checksum yn cyfateb. Mae popeth yn edrych yn dda! FAT32 
  + Cydweddus ymhob man.
  -  Methu trin ffeiliau sy'n fwy na 4GB.

exFAT
  + Cydweddus bron ymhob man.
  + Yn gallu trin ffeiliau sy'n fwy na 4GB.
  -  Ddim mor gydweddus a FAT32.

NTFS 
  + Cydweddus â Windows.
  -  Ddim yn gydweddu â'r Mac a'r rhan fwyaf o ddyfeisiau caledwedd.
  -  Ambell anhawster cydweddu gyda Linux (mae NTFS yn berchnogol ac wedi ei ail beiriannu).

EXT4 
  + Modern, sefydlog, cyflym, siwrnaleiddio.
  + Yn cynnal caniatâd ffeil Linux.
  -  Ddim yn gydweddus â Windows, Mac a'r rhan fwyaf o ddyfeisiau caledwedd.
 Gwall system ffeiliau. Fformatio Fformatio cof bach USB GB Llofnodion GPG Ffeil wedi'i harwyddo gan GPG Ffeil wedi'i llofnodi gan GPG: Nôl Gwiriad ISO Delwedd ISO: Delweddau ISO ISO: Os ydych chi'n ymddiried yn y llofnod gallwch ymddiried yn yr ISO. Methodd y gwiriad cywirdeb KB Heb ganfod yr allwedd ar y gweinydd allweddi. Ffeiliau lleol MB Creu cof bach USB cychwynadwy Crëwch gof bach cychwynadwy Rhagor o wybodaeth Heb ganfod enw cyfrol Does dim digon o le ar y cof bach USB. Swm SHA256 Ffeil symiau SHA256 Ffeil symiau SHA256: SHA256sum: Dewis Delwedd Dewis y cof bach USB Dewiswch ddelwedd Llofnodwyd gan: %s Maint: TB Mae swm SHA256 y ddelwedd ISO yn anghywir. Nid yw ffeil symiau SHA256 yn cynnwys symiau ar gyfer y ddelwedd ISO hon. Nid yw ffeil swm SHA256 wedi'i llofnodi. Mae'r cof bach USB wedi ei fformatio'n llwyddiannus. Mae'r checksum yn gywir Mae'r checksum yn gywir ond nid yw dilysrwydd y swm wedi'i gadarnhau. Nid oedd modd gwirio'r ffeil gpg. Nid oedd modd llwytho'r ffeil gpg i lawr. Gwiriwch yr URL. Ysgrifennwyd y ddelwedd yn llwyddiannus . Nid oedd modd gwirio'r ffeil swm. Nid oedd modd llwytho'r ffeil swm i lawr. Gwiriwch yr URL. Mae'r ddelwedd ISO hon yn cael ei gwirio gan lofnod dibynadwy. Mae'r ddelwedd ISO hon yn cael ei gwirio gan lofnod annibynadwy. Mae'r ISO hwn yn edrych fel delwedd Windows. Mae hon yn ddelwedd ISO swyddogol. Bydd hyn yn chwalu'r holl ddata sydd ar y cof bach USB, ydych chi'n siŵr eich bod am barhau? URLau Awdur Delwedd USB Cof Bach Fformatiwr Cof Bach USB Cof bach USB: Llofnod anhysbys Llofnod annibynadwy Gwirio Gwiriwch ddilysrwydd a chywirdeb y ddelwedd Label y cyfrol Cyfrol: Mae angen prosesu arbennig ar ddelweddau Windows. Ysgrifennu beit anhysbys 