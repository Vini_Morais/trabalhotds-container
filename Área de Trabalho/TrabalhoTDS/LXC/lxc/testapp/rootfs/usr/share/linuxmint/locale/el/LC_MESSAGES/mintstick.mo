��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  B   �  `   �      6  (   W  &   �     �     �     �  2   �  8   #  6   \  {   �  #     �  3     �       '   3     [     ^  0   u  1   �     �     �               0  s   5  9   �     �  W   �     >     X  6   ]  6   �  /   �  ;   �  F   7     ~  *   �  +   �     �          $     D     b     ~     �  U   �  u   �  S   ]  >   �  :   �  �   +  K   �  o     2   �  ^   �  �      h   �   m   !  B   u!  ;   �!  �   �!     �"  %   �"     �"  )   �"     #     #  '   5#     ]#  k   r#     �#     �#  X   $     ^$     m$     s$     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-27 09:16+0000
Last-Translator: Vasilis Kosmidis <Unknown>
Language-Team: Greek <el@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: el
 Σφάλμα κατά την εγγραφή της εικόνας. Προέκυψε σφάλμα κατά τη δημιουργία κατάτμησης στο %s. Συνέβη ένα σφάλμα Παρουσιάστηκε σφάλμα. Σφάλμα Ταυτοποίησης. Υπολογισμός... Έλεγχος Άθροισμα ελέγχου Αρχεία αθροίσματος ελέγχου Ασυμφωνία αθροίσματος ελέγχου Επιλέξτε όνομα για τη μνήμη USB Κατεβάστε ξανά την εικόνα ISO. Το άθροισμα ελέγχου του δεν ταιριάζει. Όλα φαίνονται καλά! FAT32 
  + Συμβατό παντού.
  -  Δεν μπορεί να χειριστεί αρχεία μεγαλύτερα από 4GB.

exFAT
  + Συμβατό σχεδόν παντού.
  + Μπορεί να χειριστεί αρχεία μεγαλύτερα από 4GB.
  -  Όχι τόσο συμβατό όσο το FAT32.

NTFS 
  + Συμβατό με τα Windows.
  -  Μη συμβατό με Mac και τις περισσότερες συσκευές υλικού.
  -  Περιστασιακά προβλήματα συμβατότητας με το Linux (το NTFS είναι ιδιόκτητο και έχει σχεδιαστεί με αντίστροφη μηχανική).

EXT4 
  + Σύγχρονο, σταθερό, γρήγορο, journalized.
  + Υποστηρίζει δικαιώματα αρχείων Linux.
  -  Δεν είναι συμβατό με τα Windows, Mac και τις περισσότερες συσκευές υλικού.
 Σύστημα αρχείων: Μορφοποίηση Μορφοποίηση μνήμης USB GB Υπογραφές GPG Αρχείο υπογεγραμμένο με GPG Αρχείο υπογεγραμμένο με GPG: Μετάβαση πίσω Επαλήθευση ISO Εικόνα ISO: Εικόνες ISO ISO: Εάν εμπιστεύεστε την υπογραφή, μπορείτε να εμπιστευτείτε το ISO. Ο έλεγχος ακεραιότητας απέτυχε KB Το κλειδί δεν βρέθηκε στον διακομιστή κλειδιών. Τοπικά αρχεία ΜΒ Να είναι εκκινήσιμη η μνήμη USB Να είναι εκκινήσιμη η μνήμη USB Περισσότερες πληροφορίες Δεν βρέθηκε αναγνωριστικό τόμου Δεν υπάρχει επαρκής χώρος στη μνήμη USB. Άθροισμα SHA256 Αρχείο αθροισμάτων SHA256 Αρχείο αθροισμάτων SHA256: Άθροισμα SHA256: Επιλογή Εικόνας Επιλογή μνήμης USB Επιλογή εικόνας Υπογραφή από: %s Μέγεθος: TB Το άθροισμα SHA256 της εικόνας ISO είναι εσφαλμένο. Το αρχείο SHA256 sums δεν περιέχει αθροίσματα για αυτήν την εικόνα ISO. Το αρχείο αθροισμάτων SHA256 δεν έχει υπογραφεί. Η μνήμη USB μορφοποιήθηκε επιτυχώς. Το άθροισμα ελέγχου είναι σωστό Το άθροισμα ελέγχου είναι σωστό, αλλά η αυθεντικότητα του αθροίσματος δεν επαληθεύτηκε. Δεν ήταν δυνατός ο έλεγχος του αρχείου gpg. Δεν ήταν δυνατή η λήψη του αρχείου gpg. Ελέγξτε τη διεύθυνση URL. Η εικόνα γράφτηκε επιτυχώς. Δεν ήταν δυνατός ο έλεγχος του αρχείου αθροισμάτων. Δεν ήταν δυνατή η λήψη του αρχείου αθροισμάτων. Ελέγξτε τη διεύθυνση URL. Αυτή η εικόνα ISO επαληθεύεται από μια αξιόπιστη υπογραφή. Αυτή η εικόνα ISO επαληθεύεται από μια μη αξιόπιστη υπογραφή. Αυτό το ISO μοιάζει με εικόνα των Windows. Αυτή είναι μια επίσημη εικόνα ISO. Αυτό θα καταστράψει όλα τα δεδομένα πάνω στη μνήμη USB, είστε σίγουροι ότι θέλετε να συνεχίσετε; URLs Εγγραφέας εικόνας USB Μνήμη USB Μορφοποιητής μνήμης USB Μνήμη USB: Άγνωστη υπογραφή Αναξιόπιστη υπογραφή Επαλήθευση Επαληθεύστε τη γνησιότητα και την ακεραιότητα της εικόνας Ετικέτα τόμου: Τόμος: Οι εικόνες των Windows απαιτούν ειδική επεξεργασία. Εγγραφή bytes άγνωστο 