��          t      �              %   0     V     f     u  q   �     �             -   1  �  _       %        C     T  
   d  u   o     �            ,   *                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-11-14 15:13+0000
Last-Translator: Martin Srebotnjak <miles@filmsi.net>
Language-Team: Slovenian <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s ni veljavno ime domene. Blokira dostop do izbranih imen domen Blokirane domene Blokirnik domen Ime domene Imena domen se morajo začeti in končati s črko ali števko in lahko vsebujejo le črke, številke, pike in vezaje. Primer: moja.domenastevilka1.si Neveljavna domena Starševski nadzor Vpišite ime domene, ki jo želite blokirati 