��    ,      |  ;   �      �  ?   �  B   	  !   L  $   n     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �       	        &     /     =     K     R     Z  +   f     �  
   �     �     �     �     �     �  3   �  *        I     _     p     �     �     �     �  �  �  M   e  R   �  *   	  /   1	     a	     d	     l	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  %   �	     "
     /
     8
     L
     _
     g
     n
  B   z
  
   �
     �
     �
     �
     �
  
          ?   !  9   a     �     �     �     �     �                      #         "                     )          !               ,   (                &       +                               $       '                 	          %                *   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not available Not installed Office Package Photography Please use apt-get to install this package. Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-03-11 14:00+0000
Last-Translator: Ueliton <Unknown>
Language-Team: Filipino <fil@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s ang ida-download, %(localSize)s na laman ng disk ay nablanko %(downloadSize)s ang ida-download, %(localSize)s na laman ng disk ay kinakailangan %(localSize)s na laman ng disk ay nablanko %(localSize)s na laman ng disk ay kinakailangan 3D Tungkol Mga Kagamitan Mga Larong Lupon Chat Mga detalye Pagguhit Email Upang pumaris Pagbabahagi ng talaksan Pagmumulan ng Mga laro Mga Grapiko Iluklok Mag-install ng mga bagong application Naka install Internet Hindi magagamit ang Hindi naka-install Opisina Pakete Potograpiya Mangyaring gamitin ang apt-get upang i-install ang package na ito. Pagprogram Sa pag-ilathala Diskarte sa real-time Alisin Mga Pagsusuri Nagsa-scan Agham at Edukasyon Hanapin sa kahulugan ng mga package (mas mabagal na paghahanap) Hanapin sa sumaryo ng mga package (mabagal na paghahanap) Simulation at lahi Tagapamahala ng Software Tunog at video Mga tool ng system Diskarte na nakabatay I Mga Tumitingin Web 