��    $      <  5   \      0  
   1     <     L     \     e     m     ~     �  -   �     �     �  
   �     �  
   �  	          
             0     @  0   H     y     �     �     �     �  	   �     �     �     �     �          *     2     8  �  K     �     �          *     3     @     U  	   e  1   o  (   �     �     �     �     �             
             0     G  J   P  !   �     �     �     �     �     	     	     %	  
   B	  *   M	     x	     �	     �	  �  �	         #                                             	      
                                                       "                  !                 $                              Advantages Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Mounted Volumes Network Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows Xfwm4 translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-19 20:45+0000
Last-Translator: Stefan <Stefan.stumpf@hotmail.de>
Language-Team: German, Low <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Vorteile Schaltflächenbeschriftungen: Schaltflächen-Stil: Computer Schreibtisch Desktopeinstellungen Desktop-Symbole Nachteile Fensterinhalt während des Ziehens nicht anzeigen Feinabstimmung der Desktop-Einstellungen Benutzerverzeichnis Symbolgröße Symbole Nur Symbole Schnittstelle Groß Linux Mint Mac-Stil (Links) Eingehängte Laufwerke Netzwerk Wählen Sie die Elemente aus, die Sie auf dem Schreibtisch sehen möchten: Symbole auf Schaltflächen zeigen Symbole in Menüs anzeigen Klein Text unter den Elementen Text neben Elementen Nur Text Werkzeugleisten Traditioneller Stil (Rechts) Papierkorb System-Schriftart in Titelleiste verwenden Fenstermanager Fenster Xfwm4 Launchpad Contributions:
  Cedrik Heckenbach https://launchpad.net/~cedrik-heckenbach
  Christian Oelte https://launchpad.net/~ch-oelte
  Filip https://launchpad.net/~f-muellers-deactivatedaccount
  Jorge Hinz https://launchpad.net/~jh02777
  Lucky Starr https://launchpad.net/~tuxmaster
  Stefan https://launchpad.net/~stefan-stumpf
  Thonixx https://launchpad.net/~c-admin-white-tiger-ch
  tbds https://launchpad.net/~tbds-deactivatedaccount 