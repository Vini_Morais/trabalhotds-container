��    D      <  a   \      �  	   �     �     �                 K   &  %   r  -   �     �  "   �  F   �     E     N     d     s     �     �     �  	   �     �  	   �     �     �     �       -        G     N     c     o     �     �     �     �     �     �     �     �     �     �     �     	     	  1   	  6   F	     }	     �	  $   �	     �	     �	     �	     �	     
     
     4
     E
     Y
  '   n
     �
     �
     �
     �
     �
     �
  	   �
     �
  �  �
  (   �     �  )   �       "        5  �   Q  \   �  x   Z  2   �  U     �   \     !  <   7  "   t  M   �  H   �     .     D     d  #   {     �     �  (   �     �  1     }   A     �  J   �  #     D   ;     �     �     �  	   �  
   �     �  ?   �     &  "   6  	   Y  ,   c     �     �  �   �  �   ^     �  &     P   3     �  ,   �  M   �  =        J  /   g  &   �  2   �  3   �  v   %  +   �  1   �     �     
  
        (     5  ,   O     2                   >   C   A   +             !      (   &             "           :                 ?                   *         $   B       -       7      .          6   8           9                 ;   4   /       5   D   )           0   <   '                        3           1      #   @                              	      =      
       %   ,    <not set> About Advanced MATE Menu All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2015-07-25 17:10+0000
Last-Translator: Gurdit Singh Bedi <gurditsbedi@gmail.com>
Language-Team: Punjabi <pa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <ਸੈੱਟ ਨਹੀਂ ਹੋਇਆ> ਇਸ ਬਾਰੇ ਉੱਨਤ ਮੈਟ ਮੈੱਨਯੂ ਸਭ ਸਭ ਐਪਲੀਕੇਸ਼ਨ ਐਪਲੀਕੇਸ਼ਨ ਸਭ ਲੋਕਲ ਅਤੇ ਰਿਮੋਟ ਡਿਸਕਾਂ ਅਤੇ ਇਸ ਕੰਪਿਊਟਰ ਤੋਂ ਵਰਤੋਂਯੋਗ ਫੋਲਡਰ ਦੀ ਝਲਕ ਉਪਲੱਬਧ ਸਾਫਟਵੇਅਰ ਵੇਖੋ ਤੇ ਇੰਸਟਾਲ ਕਰੋ ਬੁੱਕਮਾਰਕ ਕੀਤੇ ਅਤੇ ਲੋਕਲ ਨੈੱਟਵਰਕ ਟਿਕਾਣੇ ਖੋਲ੍ਹੋ ਹਟਾਈਆਂ ਫਾਇਲਾਂ ਵੇਖੋ ਡੈਸਕਟਾਪ ਉੱਤੇ ਪਈਆਂ ਆਈਟਮਾਂ ਖੋਲ੍ਹੋ ਖੋਲ੍ਹਣ ਅਤੇ ਮੇਨੂ ਬੰਦ ਕਰਨ ਲਈ ਇੱਕ ਨਵੇ ਐਕਸਲੇਟਰ ਕੁੰਜੀ ਨੂੰ ਸੈੱਟ ਕਰਨ ਲਈ ਕਲਿੱਕ ਕਰੋ |   ਕੰਪਿਊਟਰ ਤੁਹਾਡੇ ਸਿਸਟਮ ਦੀ ਸੰਰਚਨਾ ਕੰਟਰੋਲ ਸੈਂਟਰ ਪਲੱਗਇਨ ਸ਼ੁਰੂ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ ਪਲੱਗਇਨ ਲੋਡ ਨਹੀਂ ਕੀਤੀ ਜਾ ਸਕੀ: ਡੈਸਕਟਾਪ ਡੈਸਕਟਾਪ ਥੀਮ ਮੇਨੂ ਸੋਧ ਰੱਦੀ ਖਾਲੀ ਕਰੋ ਪਸੰਦੀਦਾ ਘਰ ਫੋਲਡਰ ਵੱਖਰੇਵਾਂ ਸ਼ਾਮਲ ਥਾਂ ਸ਼ਾਮਲ '%s' ਪੈਕੇਜ ਇੰਸਟਾਲ ਕਰੋ ਸਾਫਟਵੇਅਰ ਪੈਕੇਜ ਇੰਸਟਾਲ ਕਰੋ, ਹਟਾਓ ਅਤੇ ਅੱਪਗਰੇਡ ਕਰੋ ਚਲਾਓ ਜਦੋਂ ਮੈਂ ਲਾਗਇਨ ਕਰਾਂ ਤਾਂ ਚਲਾਓ ਸਕਰੀਨ ਲਾਕ ਕਰੋ ਲਾਗ ਆਉਟ ਕਰੋ ਜਾਂ ਯੂਜ਼ਰ ਬਦਲੋ ਲਾਗ ਆਉਟ ਮੇਨੂ ਮੇਨੂ ਪਸੰਦ ਨਾਂ ਨਾਂ: ਨੈੱਟਵਰਕ ਆਪਣਾ ਨਿੱਜੀ ਫੋਲਡਰ ਖੋਲ੍ਹੋ ਚੋਣਾਂ ਪੈਕੇਜ ਮੈਨੇਜਰ ਪਾਥ ਇੱਕ ਐਕਸਲੇਟਰ ਚੁਣੋ ਥਾਵਾਂ ਮੇਰੀ-ਪਸੰਦ ਮੌਜੂਦਾ ਸਵਿੱਚ ਜਿਲਦਬੰਦੀ ਨੂੰ ਸਾਫ ਕਰਨ ਲਈ ਬੈਕ-ਸਪੇਇਸ ਨੂੰ ਪ੍ਰੈੱਸ ਕਰੋ | ਕਾਰਵਾਈ ਨੂੰ ਰੱਦ ਕਰਨ ਲਈ ਇਸਕੇਇਪ ਪ੍ਰੈੱਸ ਕਰੋ ਜਾਂ ਫਿਰ ਕਲਿੱਕ ਕਰੋ |   ਬੰਦ ਕਰੋ ਪਲੱਗਇਨ ਮੁੜ-ਲੋਡ ਪਿਛਲੇ ਸ਼੍ਰੇਣੀ ਯਾਦ ਰੱਖੋ ਜਾਂ ਖੋਜ ਹਟਾਓ ਪਸੰਦ ਵਿੱਚੋਂ ਹਟਾਓ ਅਣਲਾਕ ਕਰਨ ਲਈ ਪਾਸਵਰਡ ਚਾਹੀਦਾ ਹੈ ਇੰਸਟਾਲ ਕਰਨ ਲਈ ਪੈਕੇਜ ਲਭੋ ਫੋਲਡਰ ਚੁਣੋ ਸਭ ਐਪਲੀਕੇਸ਼ਨ ਵੇਖੋ ਬਟਨ ਆਈਕਾਨ ਵੇਖੋ ਕੈਟਾਗਰੀ ਆਈਕਾਨ ਵੇਖੋ ਮੇਰੀ ਪਸੰਦ ਵਿੱਚ ਵੇਖੋ ਬੰਦ ਕਰੋ, ਮੁੜ-ਚਾਲੂ ਕਰੋ, ਸਸਪੈਂਡ ਕਰੋ ਜਾਂ ਹਾਈਬਰਨੇਟ ਸਾਫਟਵੇਅਰ ਮੈਨੇਜਰ ਸਵੈਪ ਨਾਂ ਅਤੇ ਆਮ ਨਾਂ ਸਿਸਟਮ ਟਰਮੀਨਲ ਥੀਮ: ਰੱਦੀ ਅਣ-ਇੰਸਟਾਲ ਕਮਾਂਡ ਲਾਈਨ ਵਰਤੋਂ 