��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  ,   �                    *     7  	   H     R     c  	   j  	   t     ~  	   �     �  	   �     �     �     �  #   �                               8     ?      ]  '   ~  -   �     �     �     	            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-14 23:02+0000
Last-Translator: Changmin Jang <Unknown>
Language-Team: Korean <ko@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 정보 동작 경로 허용 애플리케이션 XApp 상태 아이콘이 나타나는 영역 탐색 취소 카테고리 카테고리 아이콘 선택 기본값 기본 아이콘 장치 엠블럼 이모지 즐겨찾기 아이콘 아이콘 크기 이미지 불러오고 있습니다... Mime 형식 열기 지원되지 않는 작업입니다 기타 위치 검색 선택 이미지 파일 선택 상태 기본 카테고리입니다. 기본으로 사용할 아이콘 좋아하는 아이콘 크기입니다. 아이콘을 표시하는 문자열입니다. 경로 허용 여부 결정. XApp 상태 애플릿 XApp 상태 애플릿 팩토리 