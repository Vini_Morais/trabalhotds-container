��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �  T   -  /   �  3   �  ;   �  b   "  ^   �  V   �  f   ;	  X   �	  X   �	     T
  w   r
     �
     �
            J   #  0   n  R   �     �             '   1  2   Y  �   �  .   *     Y                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-07 16:45+0000
Last-Translator: Rostyslav Haitkulov <Unknown>
Language-Team: Ukrainian <uk@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s більше дискового простору буде використано. %s вільного місця на диску. %s буде завантажено в цілому. Потрібно внести додаткові зміни Додатково програмне забезпечення буде даунгрейднуто Додатково програмне забезпечення буде встановлене Додатково програмне забезпечення буде очищено Додатково програмне забезпечення буде перевстановлено Додатково програмне забезпечення буде видалено Додатково програмне забезпечення буде оновлено Сталася помилка Неможливо видалити пакунок %s, так як він потрібен іншим пакункам. Даунгрейд Flatpak Flatpaks Встановити Від пакунку %s залежать наступні пакунки: Пакунки, які буде вилучено Будь ласка, подивіться на перелік змін нижче. Очищення Перевстановити Вилучити Пропустити оновлення Буде вилучено такі пакунки: Цей пункт меню не пов'язаний із жодним пакунком. Все одно бажаєте вилучити його з меню? Оновлення буде пропущено Оновити 