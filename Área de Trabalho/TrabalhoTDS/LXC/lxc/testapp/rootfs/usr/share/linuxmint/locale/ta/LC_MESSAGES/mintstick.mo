��          �   %   �      0  )   1  2   [     �     �      �     �     �     �       "   $     G     T     g  )   w  #   �  N   �       	   %     /  
   C     N     \     b  �  j  n     w   t  3   �  &      x   G     �  `   �  v   @  f   �  V   	  7   u	  K   �	  A   �	  m   ;
  T   �
  �   �
  ,   �       ;   .     j  3   �     �     �                	   
                                                                                                            An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-03-27 22:15+0000
Last-Translator: Sudhakaran Pooriyaalar <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 படத்தை நகலெடுக்கும் போது பிழை ஏற்பட்டது. %sல் பகிர்வை உருவாக்கும் போது பிழை ஏற்பட்டது. ஒரு பிழை நேர்ந்தது. அங்கீகார பிழை. உங்கள் USB குச்சிக்கு ஒரு பெயரைத் தேர்வு செய்க வடிவமைப்பு ஒரு யுஎஸ்பி குச்சியை வடிவமைக்கவும் துவங்கக்கூடிய ஒரு யுஎஸ்பி குச்சியைச் செய்க துவங்கக்கூடிய யுஎஸ்பி குச்சியை செய்க USB குச்சியில் போதுமான இடம் இல்லை. படத்தைத் தேர்ந்தெடு ஒரு USB குச்சியைத் தேர்ந்தெடு ஒரு படத்தைத் தேர்ந்தெடு USB குச்சி வெற்றிகரமாக வடிவமைக்கப்பட்டது. படம் வெற்றிகரமாக எழுதப்பட்டது. இது USB குச்சியிலுள்ள அனைத்து தரவுகளையும் அழித்துவிடும், இதனைத் தொடர விரும்புகிறீர்களா? யுஎஸ்பி பட எழுதி USB குச்சி USB குச்சி வடிவமைப்பான் USB குச்சி வன்வட்டு முத்திரை: எழுது தெரியாதவை 