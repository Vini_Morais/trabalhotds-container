��    #      4  /   L        w   	     �     �     �     �     �     �  �   �     �     �     �  ^   �     &     =     [     d     k     t     �     �  	   �     �     �  ,   �     �     �          "     5     U     c     j     o  
   �    �  �   �     3	     G	     [	  $   g	     �	     �	  �   �	  	   i
  #   s
  $   �
  �   �
     =     \  	   |  
   �     �     �     �     �     �  (   �     �  0        <     K  %   [     �  &   �     �     �     �     �     �                               
         !                            	       "      #                                                                            <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-03-10 17:40+0000
Last-Translator: GunChleoc <Unknown>
Language-Team: Fòram na Gàidhlig
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: gd
 <b><small>An aire: Ma stàlaicheas tu pacaidean cànain no ma nì thu an ùrachadh, dh'fhaoidte gun dèid cànain a bharrachd a stàladh 'nan lùib</small></b> Cuir cànan ùr ris Cuir cànan ùr ris Cuir ris... Cuir an sàs air feadh an t-siostaim Air a làn-stàladh Dòigh ion-chuir Cleachdar dòighean ion-chuir gus samhlaidhean is caractaran a sgrìobhaidh nach eil air a' mheur-chlàr. Tha iad feumail airson cànain mar Shìnis, Seapanais, Coirèanais is msaa... Stàlaich Stàlaich / Thoir air falbh cànain Stàlaich/Thoir air falbh cànain... Stàlaich pacaidean cànain, faidhlichean eadar-theangachaidh no faclairean sam bith a tha a dhìth airson an cànan a thagh thu Stàlaich na pacaidean cànain Stàlaich an cànan a thagh thu Seapanais Coireanais Cànan Roghainnean cànain Roghainnean cànain Taic chànan Cànain Cha deach sgeama ionadail a shònrachadh Chan eil gin Àireamhan, airgeadra, seòlaidhean, tomhasan... Roinn-dùthcha Thoir air falbh Thoir air falbh an cànan a thagh thu Sìnis shimplichte Tha cuid a phacaidean cànain a dhìth Sgeama ionadail an t-siostaim Telugu Tàidh Sìnis thradaiseanta Bhiet-Namais 