��          �   %   �      @  )   A  2   k     �     �      �    �     �     �          )  "   A     d     q     �  )   �  #   �  N   �     1  	   B     L  
   `     k     y       �  �  :   "  E   ]     �     �  D   �  n  	     �  '   �  @   �  ;     F   G     �  %   �     �  ?   �  -   %  �   S  /   �     #  %   7     ]     q  
   �     �                
                                                          	                                                    An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-05-02 10:15+0000
Last-Translator: MohammadSaleh Kamyab <Unknown>
Language-Team: Persian <fa@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 هنگام رونوشت تصویر خطایی رخ داد. هنگام ایجاد افراز روی ‎%s خطایی رخ داد. خطایی رخ داد. خطای اعتبارسنجی. نامی برای فلش‌مموری‌تان انتخاب کنید ‏FAT32 
  + سازگار در همه‌جا.
  - ناتوان در رسیدگی به پرونده‌های بزرگ‌تر از ۴GB.

‏exFAT  
  +تقریباً سازگار در همه‌جا.
  + توانایی رسیدگی به پرونده‌های بزرگ‌تر از ۴GB.
  - به اندازهٔ  FAT32 سازگاری ندارد.

‏NTFS 
  + سازگار با ویندوز.
  - با مک و بیش‌تر افزاره‌های سخت‌افزاری سازگاری ندارد.
  - گاهاً مشکلات سازگاری با لینوکس (NTFS انحصاری است که مهندسی معکوس شده است).

‏EXT4 
  + نوین، پایدار و سریع.
  + از مجوز پرونده‌های لینوکس پشتیبانی می‌کند.
  - با ویندوز، مک و بیش‌تر افزاره‌های سخت‌افزاری سازگاری ندارد.
 قالب‌بندی قالب‌بندی فلش‌مموری ساخت یک فلش‌مموری قابل راه‌اندازی ساخت فلش‌مموری قابل راه‌اندازی فضای کافی بر روی فلش‌مموری وجود ندارد. گزینش تصویر انتخاب یک فلش‌مموری گزینش یک تصویر فلش‌مموری با موفقیت قالب‌بندی شد. تصویر با موفقیت نوشته شد. تمامی داده‌های موجود روی فلش‌مموری نابود خواهند شد، مطمئنید که می‌خواهید ادامه دهید؟ نویسندهٔ تصویر فلش‌مموری فلش‌مموری قالب‌بند فلش‌مموری فلش‌مموری برچسب حجم نوشتن ناشناخته 