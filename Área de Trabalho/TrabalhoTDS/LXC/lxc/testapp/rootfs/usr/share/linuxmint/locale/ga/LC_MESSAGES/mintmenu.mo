��    C      4  Y   L      �  	   �     �     �     �     �  K   �  %   /  -   U     �  "   �  F   �               !     0     K     a     i  	   w     �  	   �     �     �     �     �  -   �                     ,     C     J     O     `     e     k     s     �     �     �     �     �     �  1   �  6   	     :	     ?	  $   N	     s	     z	     �	     �	     �	     �	     �	     
     
  '   +
     S
     d
     
     �
     �
     �
  	   �
     �
  �  �
  	   k  
   u     �     �     �  Z   �  0     <   2     o  $   �  Z   �  
             +  $   ?  &   d     �     �     �     �     �     �     �             8   -  
   f  $   q     �  &   �     �  
   �     �                          >     G     ^     e  	   x     �  7   �  D   �            4   (     ]     b  +   w  %   �     �      �     �  %        @  ,   _     �  #   �     �     �     �     �     �  #   �     )      -   &       (          ?   =                           %         .                0             !   *       ,       B   :   2          +              #                >   $   <   3      
   '          @              C              9       7      	   A   1   ;      5      8                  "   6         4   /        <not set> About All All applications Applications Browse all local and remote disks and folders accessible from this computer Browse and install available software Browse bookmarked and local network locations Browse deleted files Browse items placed on the desktop Click to set a new accelerator key for opening and closing the menu.   Computer Configure your system Control Center Couldn't initialize plugin Couldn't load plugin: Desktop Desktop theme Edit menu Empty trash Favorites Home Folder Insert separator Insert space Install package '%s' Install, remove and upgrade software packages Launch Launch when I log in Lock Screen Log out or switch user Logout Menu Menu preferences Name Name: Network Open your personal folder Options Package Manager Path Pick an accelerator Places Preferences Press Backspace to clear the existing keybinding. Press Escape or click again to cancel the operation.   Quit Reload plugins Remember the last category or search Remove Remove from favorites Requires password to unlock Search for packages to install Select a folder Show all applications Show button icon Show category icons Show in my favorites Shutdown, restart, suspend or hibernate Software Manager Swap name and generic name System Terminal Theme: Trash Uninstall Use the command line Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2013-04-28 17:17+0000
Last-Translator: Gearóid  Ó Maelearcaidh <gearoid@omaelearcaidh.com>
Language-Team: Irish <ga@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <not set> Eolas Faoi Uile Feidhmchláir uile Feidhmchláir Brábhsáil gach diosca logánta agus cianda agus fillteáin inrochtana ón ríomhaire seo Brabhsáil is suiteáil bogearraí atá ar fáil Brábhsáil suíomh líonra leabharmharcáilte agus logánta Brabhsáil na comhaid bainte Brabhsáil rudaí curtha ar an deasc Cliceáil chun eochair luasaire nua a shocrú chun an roghchlár a oscailt agus dúnadh.   Ríomhaire Cumraigh do chóras Lárionad Rialaithe Níorbh fhéidir breiseán a thúsú Níorbh fhéidir bhreiseán a luchtú: Deasc Téama deisce Roghchlár Cuir in Eagar Folmhaigh an bruscar Ceanáin Fillteán Baile Cuir isteach deighilteoir Cuir isteach spás Suiteáil pácáiste '%s' Suiteáil, bain agus uasghrádaigh pacáistí bogearraí Lainseáil Lainseáil nuair a logáilim isteach Cuir an Scáileán Faoi Ghlas Logáil amach nó athraigh úsáideoir Logáil Amach Roghchlár Sainroghanna roghchláir Ainm Ainm: Líonra Oscail d'fhillteán pearsanta Roghanna Bainisteoir Pacáistí Cosán Roghnaigh luasaire Áiteanna Sainroghanna Brú ar Chúlspás chun an eochair-cheangail a ghlanadh Brú ar Éalaigh nó cliceáil arís chun an oibríócht a chealú   Scoir Athluchtú breiseáin Cuimhnigh ar an gcatagóír is déanaí nó cuardach Bain Bain ón na ceanáin Focal Faire ag teastáil chun díghleasáil Cuardaigh ar phacáistí le suiteáil Roghnaigh fillteán Taispeáin na feidhmchláir uile Taispeáin cnaipe-deilbhín Taispeáin deilbhíní na gcatagóiri Taispeáin imeasc mo cheanáin Dún síos, atosaigh, fionraigh nó suanaigh Bainisteoir bogearraí Ainm babhtála agus ainm cineálach Córas Teirminéal Téama: Bruscar Díshuiteáil Bain úsáid as líne na n-orduithe 