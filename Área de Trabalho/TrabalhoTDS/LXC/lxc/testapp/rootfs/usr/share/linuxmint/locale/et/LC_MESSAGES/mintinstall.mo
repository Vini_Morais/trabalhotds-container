��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  G     B   X      �  #   �  &   �          
                    0  $   N     s       t   �          '  8   /  ,   h     �     �     �     �     �     �     �  
   �     �     �  W     9   o     �     �     �     �     �     �     �       Q        k     t     y  #   �     �     �     �     �  ^        `     n     {     �     �     �  (   �  ;   �     �  
             !  
   5     @     `     h  P   t  
   �     �     �     �  5   �  .   4  )   c     �     �     �     �     �     �  3   �  B   2     u     �     �     �         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-06-23 06:12+0000
Last-Translator: mahfiaz <mahfiaz@gmail.com>
Language-Team: Estonian <et@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s laaditakse alla, %(localSize)s kettaruumi vabastatakse %(downloadSize)s laaditakse alla, %(localSize)s kettaruumi vajalik %(localSize)s kettaruumi vabaneb %(localSize)s kettaruumi on vajalik %d tegevus pooleli %d tegevust pooleli 3D Teave Tarvikud Kõik Kõik rakendused Kõik tegevused on lõpetatud Flatpaki repo lisamisel esines viga. Lauamängud Pole võimalik paigaldada Seda faili pole võimalik töödelda, kui teised tegevused on pooleli.
Palun proovi uuesti pärast nende lõppemist. Pole võimalik eemaldada Vestlus Enda arvustuse lisamiseks <a href='%s'>klõpsa siia</a>. Praegu tehakse tööd järgmiste pakettidega Üksikasjad Joonistamine Toimetaja valik Haridus Elektroonika E-mail Emulaatorid Põhilised Failijagamine Esimeses isikus mängud Flatpaki tugi ei ole saadaval. Proovi paigaldada paketid flatpak ja gir1.2-flatpak-1.0. Flatpaki tugi ei ole saadaval. Paigaldada pakett flatpak. Kirjatüübid Mängud Graafika Paigalda Paigalda uusi rakendusi Paigaldatud Paigaldatud rakendused Paigaldamine Selle paketi paigaldamine võib põhjustada pöördumatut kahju sinu süsteemile. Internet Java Käivita Otsingu piiramine praegu kuvatavale Matemaatika Multimeedia koodekid Multimeedia koodekid KDE jaoks Sobivaid pakette ei leitud Ei ole pakette, mida näidata.
See võib olla probleemi tunnus - proovi puhvri värskendamist. Pole saadaval Paigaldamata Kontor PHP Pakett Fotograafia Varu kannatust. See võib aega võtta... Palun kasuta selle paketi paigaldamiseks programmi apt-get. Programmeerimine Avaldamine Python Reaalaja strateegia Värskenda Pakettide loendi värskendamine Eemalda Eemaldamine Selle paketi eemaldamine võib põhjustada pöördumatut kahju sinu süsteemile. Arvustused Skaneerimine Loodusteadused Teadus ja haridus Otsi pakettide kirjeldustest (veelgi aeglasem otsing) Otsi pakettide kokkuvõtetest (aeglane otsing) Tarkvaraallikate otsimine, oota üks hetk Kuva paigaldatud rakendused Simulatsioon ja võidusõit Tarkvarahaldur Heli Heli ja video Süsteemi tööriistad Flatpaki repo, mida püüad lisada, on juba olemas. Praegu on mõned tegevused pooleli.
Kas tahad kindlasti lõpetada? Käigupõhine strateegia Video Vaaturid Veeb 