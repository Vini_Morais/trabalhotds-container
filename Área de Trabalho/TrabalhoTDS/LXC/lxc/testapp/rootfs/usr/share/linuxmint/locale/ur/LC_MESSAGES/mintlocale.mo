��    #      4  /   L        w   	     �     �     �     �     �     �  �   �     �     �     �  ^   �     &     =     [     d     k     t     �     �  	   �     �     �  ,   �     �     �          "     5     U     c     j     o  
   �  �  �  �   R  !   �  !   	     :	  (   T	  !   }	     �	  3  �	     �
  5   �
  ;   -  �   i  %   �  0   #     T  
   a     l     u     �     �     �  8   �       0      
   Q     \  /   i     �  3   �  "   �       
             0                               
         !                            	       "      #                                                                            <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Japanese Korean Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Simplified Chinese Some language packs are missing System locale Telugu Thai Traditional Chinese Vietnamese Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-11-28 13:33+0000
Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small> نوٹ: کسی زبان پیک کو انسٹال یا اپگریڈ کرنے سے اضافی زبانیں انسٹال ہو سکتی ہیں۔</small></b> نئی زبان شامل کریں نئی زبان شامل کریں شامل کریں۔۔۔۔ پورے سسٹم پہ لاگو کریں مکمل طور پر انسٹال لکھنے کی طرز 'لکھنے کی طرز' کا استعمال ایسے سمبل(symbol) اور کیریکٹر (charachter)  لکھنے کے لیے کیا جاتا ہے جو کی بورڈ پہ موجود نہیں۔ مثال کے طور پر آپ اردو لکھنے کے لیے اس کا استعمال کر سکتے ہیں۔ انسٹال زبان انسٹال یا ان انسٹال کریں زبان انسٹال یا ان انسٹال کریں۔۔۔ منتخب کردہ زبان کے لیے غیر موجود 'زبان پیک'، ترجمے کی فائلیں اور لغات انسٹال کریں۔ زبان پیک انسٹال کریں منتخب شدہ زبان انسٹال کریں جاپانی کورین زبان زبان کی ترتیبات زبان کی ترتیبات زبان کی معاونت زبانیں کوئی مقامیت(locale) بتائی نہیں گئی کچھ نہیں نمبر، کرنسی، پتے، پیمائش... علاقہ ہٹائیں منتخب کردہ زبان کو ہٹا دیں سادہ چینی کچھ ربانوں کے پیک موجود نہیں سسٹم کی مقامیت(locale) تیلوگو تھائی روایتی چینی ویتنامی 