��    k      t  �   �       	  ?   !	  B   a	  !   �	  $   �	     �	       
     !
     $
     *
     6
     :
     >
     O
  5   g
     �
     �
     �
  _   �
           .  3   3  +   g     �     �     �     �  	   �     �     �  	   �  
   �     �     �       Z     C   s     �     �     �     �     �     �     �  	          
   4  F   ?     �     �     �     �     �     �     �     �     �  L        [     t     �     �     �     �     �     �  -   �  +   �       
   )     4     ;     N     V     s     {     �  D   �     �     �     �     �  3   �  *   3  +   ^     �     �     �     �  )   �     �               "  6   /  E   f     �     �  	   �     �  Q   �     O     [     d     j     r     v  �  �  K   Y  L   �  *   �  +        I  .   `     �  	   �  
   �     �     �     �  '   �  G   �     <     K     Q  i   f     �     �  @   �  .   +     Z     c     r     y  
   �     �     �  
   �  
   �     �     �     �  w      `   x     �     �     �  	             !     *  	   G     Q  
   i  E   t     �     �     �  &   �     �               4  /   <  D   l  '   �     �     �     �     �                 -   +  0   Y     �     �     �     �     �     �     �     �  
   �  E   	     O     X     a     i  4   ~  +   �  4   �  #        8     O     X  4   k     �     �     �     �  B   �  3   '     [     y     �     �  b   �       
   -     8     ?     G  '   K     0   [   M       ]       3   A                       c       (             B   9   #   ^   .   /   e       g   Y   N   U      
   2       -          *                   ;           K   &      k   7   6         `   d   ?   Q              R   O   J                      +   :   >   '   E   @   )           <   $       I   1   %          H   	   \                      a   Z              j      8                     V   T              P       5   F       X      =   !      f   D       S      L   4   G   C   b   "   _                           i   ,   W          h    %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d Review %d Reviews %d task running %d tasks running 3D About Accessories Add All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Branch: Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Documentation Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak (%s) Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Generating cache, one moment Graphics Homepage Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE Name: No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. No screenshots available Not available Not installed Office Optional components PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remote: Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Size: Software Manager Something went wrong. Click to try again. Sound Sound and video System Package System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? This is a system package This package is a Flatpak Try again Turn-based strategy Unable to communicate with servers. Check your Internet connection and try again. Unavailable Version: Video Viewers Web Your system's package manager Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-12-01 10:37+0000
Last-Translator: Isidro Pisa <isidro@utils.info>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s para descargar, %(localSize)s de espacio de disco liberado %(downloadSize)s para descargar, %(localSize)s de espacio de disco requerido %(localSize)s de espacio de disco liberado %(localSize)s de espacio de disco requerido %d Reseña %d Reseñas %d tarea en ejecución %d tareas en ejecución 3D Acerca de Accesorios Añadir Todas Todas las aplicaciones Se han completado todas las operaciones Se ha producido un error al intentar añadir el repositorio de Flatpak. Juegos de mesa Rama: No se puede instalar No se puede procesar este archivo mientras haya operaciones activas.
Inténtelo de nuevo cuando terminen. No se puede eliminar Chat Haga clic <a href='%s'>aquí</a> para añadir su propia reseña. Se está trabajando en los paquetes siguientes Detalles Documentación Dibujo Recomendaciones Educación Electrónica Correo electrónico Emuladores Esenciales Intercambio de archivos Primera persona Flatpak (%s) Las aplicaciones en formato Flatpak no están disponibles. Pruebe a instalar los paquetes flatpak y gir1.2-flatpak-1.0. Las aplicaciones en formato Flatpak no están disponibles. Pruebe a instalar el paquete flatpak. Tipografías Juegos Generando caché, un momento Gráficos Página web Instalar Instalar aplicaciones nuevas Instalado Aplicaciones instaladas Instalando Instalar este paquete podría causar un daño irreparable al sistema. Internet Java Ejecutar Limitar la búsqueda a la lista actual Matemáticas Códecs multimedia Códecs multimedia para KDE Nombre: No se ha encontrado ningún paquete coincidente No hay paquetes que mostrar.
Pruebe a recargar la lista de paquetes. No hay capturas de pantalla disponibles No disponible No instalado Oficina Componentes opcionales PHP Paquete Fotografía Tenga paciencia, esto puede tardar un rato... Use la orden apt-get para instalar este paquete. Programación Maquetación Python Estrategia en tiempo real Recargar Recargar la lista de paquetes Remoto: Eliminar Eliminando Eliminar este paquete podría causar un daño irreparable al sistema. Reseñas Escáner Ciencia Ciencia y educación Buscar en la descripción (búsqueda aún mas lenta) Buscar en el resumen (búsqueda más lenta) Buscando en los repositorios de software, un momento Mostrar las aplicaciones instaladas Simulación y carreras Tamaño: Gestor de software Algo salió mal. Haga clic para volver a intentarlo. Sonido Sonido y vídeo Paquete del sistema Herramientas del sistema El repositorio de Flatpak que está tratando de añadir ya existe. Hay operaciones activas.
¿Seguro que quiere salir? Este es un paquete de sistema Este es un paquete Flatpak Intentar de nuevo Estrategia por turnos No se puede comunicar con los servidores. Compruebe su conexión a Internet y vuelva a intentarlo. No disponible Versión: Vídeo Visores Web Administrador de paquetes de su sistema 