��    t      �  �   \      �	     �	     �	     �	     �	  	   �	     
     
     ,
  	   ;
     E
     T
  
   c
     n
     t
  ;   �
     �
     �
     �
  	   �
     �
     �
          !     8     =     O  5   \  +   �  /   �     �  -        1  /   9     i     �     �     �  .   �     �      �                 	   <     F     T     j     r     u     �     �     �     �     �     �  
   �               &     6  
   >  
   I     T     `     y     |  	   �     �     �     �     �     �     �     �     �  C   �  	   *     4     M     V     r     x     �     �  
   �     �     �  #   �     �  
   �     �          !     3     <     N  J   \  /   �  A   �               9     P     b  	   h     r     �     �     �     �     �     �  <   �     #     ,  �  ?     �     �     �     �               >     \     v     �     �     �     �     �  A   �          /     6  	   N  	   X     b  %   x  "   �  	   �      �     �  <     9   >  :   x     �  M   �       >   ,     k     �     �     �  C   �  	               0     Q     X     r     �     �     �     �     �     �     �     �          -      D     e     �     �     �     �  	   �     �     �     �     �  
   �               -  
   4     ?     L     g     v       G   �     �     �       #        B  !   [     }     �     �     �     �  2   �               4     H     b  	   |     �     �  L   �  0     H   3     |     �     �     �     �     �  !   �     �  
             +  $   4     Y  _   q     �     �     S             3               X   9   n               L   '   c           -   j          :   5   [   f            Z           s       .   ,      &      Y                  o      0   D   T                 ?           #   >   i   t   R   W   C   1           O   7             g   K       ]      ;      @   E   B       "   N   H   !   =          p           I   <   8       %   4      V   F          q       2                      (   
       a   e      U   )   +   	   _   `   A   b       l       ^   G   h       $      6   M          d   P   k          \       Q       m   r   J           *   /    About Tilix Active Add Bookmark... Add Down Add Right Add terminal down Add terminal right Advanced Paste All Files All JSON Files All Text Files Assistants Badge Badges enabled=%b Be sure you understand what each part of this command does. Bookmark... Cancel Change Session Name Clipboard Close Close session Convert CRLF and CR to LF Convert spaces to tabs Copy Copy Link Address Copy as HTML Copying commands from the internet can be dangerous.  Could not check file '%s' due to error '%s' Could not load session due to unexpected error. Create a new session Custom link regex '%s' has an error, ignoring Default Disable input synchronization for this terminal Do not show this again Don't Paste Edit Encodings Edit Profile Enable input synchronization for this terminal Encoding Enter a new name for the session Error Loading Session Error:  Filename '%s' does not exist Find next Find previous Find text in terminal Find… GC GTK Version: %d.%d.%d Keyboard Shortcuts Layout Options Layout Options… Library Not Loaded Load Session Match as regular expression Match case Match entire word only Maximize Monitor Silence Name… New Window New output Not Enabled Notifications enabled=%b OK Open Open Link Open… Other Password... Paste Paste Anyway Preferences Profiles Quake Mode Not Supported Quake mode is not supported under Wayland, running as normal window Read-Only Regular Expression Error Relaunch Remote File URI Unsupported Reset Reset and Clear Restore Save Save As… Save Output… Save Session Search '%s' is not a valid regex
%s Search Options Select All Show File Browser... Synchronize Input Synchronize input Terminal Terminal Activity Terminal bell The library %s could not be loaded, password functionality is unavailable. There are multiple sessions open, close anyway? This command is asking for Administrative access to your computer Tilix Tilix Custom Notification Tilix Special Features Tilix version: %s Title Transform Triggers enabled=%b Unexpected exception occurred Unknown VTE version: %s Versions View session sidebar Wrap around Your GTK version is too old, you need at least GTK %d.%d.%d! disabled translator-credits Project-Id-Version: tilix
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-04-13 23:11+0000
Last-Translator: Péter Varga <pcobbler9@gmail.com>
Language-Team: Hungarian <https://hosted.weblate.org/projects/tilix/translations/hu/>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.0-dev
 A Tilix-ről Aktív Új könyvjelző… Hozzáadás lefelé Hozzáadás jobbra Terminál hozzáadása lefelé Terminál hozzáadása jobbra Irányított beillesztés Minden Fájl Minden JSON fájl Minden szöveges fájl Asszisztensek Jelvény Jelvények bekapcsolva=%b Kérem, ellenőrizze az alábbi parancs részletes működését. Könyvjelző… Mégse Munkamenet átnevezése Vágólap Bezárás Munkamenet bezárása CRLF átalakítása CR-ré és LF-fé Szóköz tabulátorrá alakítása Másolás Hivatkozás címének másolása Másolás HTML-ként Az internetről származó parancsok veszélyesek lehetnek.  A '%s' fájl ellenőrzése nem sikerült, '%s' hiba miatt Váratlan hiba miatt meghiúsult a munkamenet betöltése. Új munkamenet indítása A hivatkozás reguláris kifejezés „%s” hibás, figyelmen kívül hagyva Alapértelmezés Bevitel szinkronizációjának letiltása ehhez a terminálhoz Ne jelenjen meg többet Ne illessze be Kódolások szerkesztése Profil szerkesztése Bevitel szinkronizációjának engedélyezése ehhez a terminálhoz Kódolás Adja meg a munkamenet új nevét Hiba a munkamenet betöltésekor Hiba:  A fájl (%s) nem létezik Következő keresése Előző keresése Keresés a terminálban Keresés… GC GTK verzió: %d.%d.%d Gyorsbillentyűk Elrendezés Elrendezés beállításai… Könyvtár nincs betöltve Munkamenet betöltése Reguláris kifejezésként keres Kis- és nagybetűérzékeny Egész szóra keres Maximizálás Csend figyelése Név… Új ablak Új kimenet Tiltott Értesítések bekapcsolva=%b OK Megnyitás Hivatkozás megnyitása Megnyitás… Egyéb Jelszó… Beillesztés Beillesztés mindenképpen Beállítások Profilok A Quake mód nem támogatott Wayland alatt a Quake mód nem támogatott, folytatás normál ablakkal Csak olvasható Reguláris kifejezés hiba Újraindítás A távoli fájl URI nem támogatott Alaphelyzetbe állítás Alaphelyzet és kimenet törlése Visszaállítás Mentés Mentés másként… Kimenet mentése… Munkamenet mentése A „%s” nem érvényes reguláris kifejezés
%s Keresési beállítások Összes kijelölése Fájlböngésző… Bevitel szinkronizálása Bevitel szinkronizálása Terminál Terminál-tevékenység Terminál jelzőhangja A %s könyvtár nem tölthető be, a jelszó funkcionalitás nem elérhető. Több munkamenet van folyamatban, biztos kilép? Ez a parancs adminisztrátori hozzáférést igényel a számítógépen Tilix Egyedi Tilix értesítés Tilix Extrák Tilix verzió: %s Cím Átalakítások Indító triggerek bekapcsolva=%b Nem várt kivétel történt Ismeretlen VTE verzió: %s Verziók Munkamenet oldalsáv megjelenítése Folytatólagos keresés A számítógépén található GTK verzió túl régi, legalább %d.%d.%d verzió szükséges! kikapcsolva Gergely Gombos 