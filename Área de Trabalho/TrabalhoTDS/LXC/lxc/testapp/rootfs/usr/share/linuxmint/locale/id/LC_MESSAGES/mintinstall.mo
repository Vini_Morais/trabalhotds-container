��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  Q     S   Y  /   �  /   �          *     -     5     >     D     S  @   i     �     �  h   �     8     N  >   V  +   �     �  
   �     �  
   �     �     �                    ,  U   9  >   �     �  	   �     �     �     �  	             $  Y   -     �     �  	   �  $   �  
   �     �     �  %   �  ^   !     �     �     �     �     �  	   �  ,   �  3   �  
   )     4     @     G     X     a  	   w  	   �  Z   �     �     �     �     �  1     .   N  3   }  !   �     �     �                  5   '  =   ]     �     �     �     �         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-07 07:43+0000
Last-Translator: Bayu Satiyo <Unknown>
Language-Team: Indonesian <id@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s untuk didownload, %(localSize)s dari kapasitas harddisk terbebas %(downloadSize)s untuk didownload, %(localSize)s dari kapasitas harddisk diperlukan %(localSize)s kapasitas hardisk yang dibebaskan %(localSize)s dari kapasitas hardisk diperlukan Tugas-tugas yang berjalan %d 3D Tentang Tambahan Semua Semua Aplikasi Semua operasi lengkap Sebuah kesalahan terjadi ketika mencoba menambahkan repo Flatpak Permainan Papan Tidak dapat memasang Tidak dapat memproses berkas ketika operasi sedang aktif.
Mohon untuk mencoba lagi ketika sudah selesai. Tidak dapat menghapus Obrolan Klik <a href='%s'>disini</a> untuk tambah review anda sendiri. Saat ini sedang bekerja dipaket dibawah ini Rincian Menggambar Pilihan Editor Pendidikan Elektronika Surat Elektronik Emulator Esensial Berbagi Berkas First-person Dukungan Flatpak saat ini tidak tersedia. Coba pasang flatpak dan gir1.2-flatpak-1.0. Dukungan Flatpak saat ini tidak tersedia. Coba pasang flatpak. Font Permainan Gambar Pasang Memasang aplikasi baru Terpasang Aplikasi yang terpasang Memasang Memasang paket ini bisa menyebabkan kerusakan yang tidak dapat diperbaiki ke sistem anda. Internet Java Luncurkan Batasi pencarian pada daftar terkini Matematika Kodek Multimedia Codec Multimedia untuk KDE Tidak ada paket sesuai yang ditemukan Tidak ada paket untuk ditunjukkan.
Ini mungkin tanda adanya masalah - coba segarkan singgahan. Tidak tersedia Tidak terinstal Aplikasi Perkantoran PHP Paket Fotografi Mohon bersabar. Hal ini membutuhkan waktu... Silakan gunakan apt-get untuk menginstal paket ini. Pemrogaman Menerbitkan Python Taktik real-time Segarkan Segarkan daftar paket Hilangkan Menghapus Menghapus paket ini bisa menyebabkan kerusakan yang tidak dapat diperbaiki ke sistem anda. Ulasan Memindai Sains Ilmu Pengetahuan dan Pendidikan Cari didalam uraian paket (Pencarian lebih pelan) Cari didalam ringkasan paket (pencarian pelan) Mencari repositori perangkat lunak, tunggu sebentar Tampilkan aplikasi yang terpasang Simulasi dan balap Pengelola Perangkat Lunak Suara Suara dan Video Perangkat Sistem Repo Flatpak yang ingin anda coba tambahkan sudah ada Ada operasi aktif saat ini.
Apakah anda yakin ingin berhenti? Turn-based strategy Video Penampil Web 