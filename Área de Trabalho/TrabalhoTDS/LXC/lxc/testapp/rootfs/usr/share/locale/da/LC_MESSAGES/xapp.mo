��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �  
   �     �  
   �  $   �       	     
   %     0     9     G     P     ]     e     n  
   t          �     �     �  	   �     �      �     �     �     �     �     �            &        C  #   b     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-25 17:04+0000
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Om Handlinger Tillad stier Programmer Område hvor XApp-statusikoner vises Gennemse Annullér Kategorier Kategori Vælg et ikon Standard Standardikon Enheder Emblemer Emoji Favoritter Ikon Ikonstørrelse Billede Indlæser … Mimetyper Åbn Handlingen er ikke understøttet Andet Steder Søg Vælg Vælg billedfil Status Standardkategorien. Det ikon som skal bruges som standard. Den foretrukne ikonstørrelse. Strengen som repræsenterer ikonet. Hvorvidt stier skal tillades. XApp status-panelprogram XApp status-panelprogramfabrik 