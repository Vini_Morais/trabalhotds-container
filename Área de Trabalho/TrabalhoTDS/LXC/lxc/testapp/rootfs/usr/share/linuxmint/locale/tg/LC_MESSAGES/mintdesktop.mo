��            )         �     �     �     �     �     �     �  -        /     J  
   O     Z  
   `  	   k     u     {     �     �  0   �     �     �     �            	   (     2     ;     U     [     w       �  �  $   ]  $   �     �     �  $   �  $   �  f     U   {     �     �     
          7  
   J     U     m     �  m   �  5   	  3   >	     r	  +   {	  +   �	     �	     �	  &   
  
   *
  Q   5
     �
  S   �
        
                                                                           	                                                     Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Mac style (Left) Mounted Volumes Network Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Windows translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: Victor Ibragimov <victor.ibragimov@gmail.com>
PO-Revision-Date: 2015-09-14 15:06+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Tajik <tg@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: tg
 Барчаспҳои тугмаҳо: Тарҳбандии тугмаҳо: Компютер Мизи корӣ Танзимоти мизи корӣ Нишонаҳои мизи корӣ Намоиш надодани мӯҳтавои равзана ҳангоми кашидани онҳо Интихобҳои беҳтарин барои танзимоти мизи корӣ Феҳристи асосӣ Андозаи нишона: Нишонаҳо Танҳо нишонаҳо Интерфейс Калон Сабки Mac (Чап) Ҳаҷмҳои васлшуда Шабака Объектҳоеро, ки мехоҳед дар мизи корӣ бинед, интихоб намоед: Намоиши нишонаҳо дар тугмаҳо Намоиши нишонаҳо дар менюҳо Хурд Матн дар болои объектҳо Матн аз паҳлӯи объектҳо Танҳо матн Наворҳои абзор Сабки анъанавӣ (Рост) Сабад Истифодаи шрифти системавӣ дар навори унвон Равзанаҳо Launchpad Contributions:
  Victor Ibragimov https://launchpad.net/~victor-ibragimov 