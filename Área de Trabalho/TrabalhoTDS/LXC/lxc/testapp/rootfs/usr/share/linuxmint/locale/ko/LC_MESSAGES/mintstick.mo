Þ    O        k         ¸  )   ¹  2   ã          (     ;     Q     `     f     o     ~        :   ±     ì         
     
     #
     6
     9
     H
     X
     i
     q
  
   
  
   
     
  1   
     Ï
     æ
     é
                    .     F     W  "   j  
             ©  
   »     Æ     Ó     æ     ö          
  -     >   ;  #   z  )        È  I   à  "   *  4   M  #     #   ¦  5   Ê  2      5   3  $   i       N   ­     ü       	          
   0     ;     M     a  2   h          ©  *   ±     Ü     â     è    ð  A     >   Ä                >     M     [  	   b     l     }  +     S   º       _  ,               ¥     ¶  
   ¹     Ä     Ö     é  
   ÷                 D   #     h       2        ¶     Ä     Ç     Ü     ñ  $     (   '     P     a     y          £  !   ´     Ö     ó            8     b   L  8   ¯  4   è       H   =  )     L   °  (   ý  /   &  Q   V  M   ¨  M   ö  1   D     v  g        ý                    /     ;     Q     m  (   t          ¬  9   ´  	   î  	   ø          K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-02 07:50+0000
Last-Translator: ChanUng Park <Unknown>
Language-Team: Korean <ko@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 ì´ë¯¸ì§ë¥¼ ë³µì¬íë ëì ìë¬ê° ë°ìíììµëë¤. %sì íí°ìì ë§ëë ì¤ ìë¬ê° ë°ìíìµëë¤. ì¤ë¥ê° ë°ìíìµëë¤ ì¤ë¥ê° ë°ìíìµëë¤. ì¸ì¦ ì¤ë¥. ê³ì° ì¤... ê²ì¬ ì²´í¬ì¬ ì²´í¬ì¬ íì¼ ì²´í¬ì¬ ì¤í¨ USB ì¤í±ì ì´ë¦ì ì íí´ ì£¼ì¸ì ì²´í¬ì¬ì ì¤í¨íìµëë¤. ISO ì´ë¯¸ì§ë¥¼ ë¤ì ë¤ì´ë¡ëí´ ì£¼ì¸ì. ëª¨ë ê² ì¢ìë³´ì´ë¤ì! FAT32 
  + ëª¨ë í¸í ê°ë¥.
  -  4GBë¥¼ ëë íì¼ì ì²ë¦¬í  ì ìì.

exFAT
  + ê±°ì ëª¨ë í¸í ë¨.
  + 4GBë¥¼ ëë íì¼ì ì²ë¦¬í  ì ìì.
  -  FAT32ë§í¼ í¸ííì§ë ëª»í¨.

NTFS 
  + Windowsì í¸í ë¨.
  -  Mac ë° ëë¶ë¶ì íëì¨ì´ ì¥ì¹ì í¸íëì§ ìì.
  -  ê°ëì© ë¦¬ëì¤ì í¸íì± ë¬¸ì  ìê¹(NTFSë ëì ì ì´ë©° ë¦¬ë²ì¤ ìì§ëì´ë§ ì).

EXT4 
  + ìµì , ìì ì ì, ë¹ ë¦, ì ëë¼ì´ì§ ì§ì.
  + ë¦¬ëì¤ íì¼ í¼ë¯¸ì ì§ì.
  -  Windows, Mac ë° ëë¶ë¶ì íëì¨ì´ ì¥ì¹ì í¸íëì§ ìì.
 íì¼ ìì¤í: í¬ë§· USB í¬ë§·íê¸° GB GPG ìëª GPG ìëª íì¼ GPG ìëª íì¼: ë¤ë¡ ê°ê¸° ISO ê²ì¦ ISO ì´ë¯¸ì§ ISO ì´ë¯¸ì§ ISO: ìëªì ì ë¢°í  ì ìë¤ë©´ ISOë ì ë¢°í  ì ììµëë¤. ë¬´ê²°ì± ê²ì¬ ì¤í¨ KB í¤ìë²ë¡ë¶í° í¤ë¥¼ ì°¾ì§ ëª» íìµëë¤. ë¡ì»¬ íì¼ MB ë¶í USB ë§ë¤ê¸° ë¶í USB ë§ë¤ê¸° ìì¸í ì ë³´ ë³¼ë¥¨ IDë¥¼ ì°¾ì§ ëª» íìµëë¤ USB ì¤í±ì ê³µê°ì´ ëª¨ìëëë¤. SHA256 ì²´í¬ì¬ SHA256 ì²´í¬ì¬ íì¼ SHA256 ì²´í¬ì¬ íì¼: SHA256ì²´í¬ì¬: ì´ë¯¸ì§ ì í USB ì¤í±ì ì íí´ ì£¼ì¸ì ì´ë¯¸ì§ë¥¼ ì ííì¸ì %sì´(ê°) ìëªí¨ í¬ê¸°: TB ISO ì´ë¯¸ì§ì SHA256 ì²´í¬ì¬ì´ ì¤í¨íìµëë¤. SHA256 ì²´í¬ì¬ íì¼ì´ ì´ ISO ì´ë¯¸ì§ë¥¼ ìí ì²´í¬ì¬ì ê°ì§ê³  ìì§ ììµëë¤. SHA256 ì²´í¬ì¬ íì¼ì´ ìëªëì§ ìììµëë¤. USB ì¤í±ì´ ì±ê³µì ì¼ë¡ í¬ë§·ëììµëë¤. ì²´í¬ì¬ì ì±ê³µíìµëë¤ ì²´í¬ì¬ì ì±ê³µíì§ë§ ì§ì ì¬ë¶ë¥¼ ê²ì¦í  ì ììµëë¤ gpg íì¼ì ê²ì¦í  ì ììµëë¤. gpg íì¼ì ë¤ì´ë¡ë í  ì ììµëë¤. URLì íì¸í´ ë³´ì¸ì. ë¶í USB ë§ë¤ê¸°ë¥¼ ë§ì³¤ìµëë¤. ì²´í¬ì¬ íì¼ì ê²ì¦í  ì ììµëë¤. ì²´í¬ì¬ íì¼ì ë¤ì´ë¡ëí  ì ììµëë¤. URLì íì¸í´ ë³´ì¸ì. ì´ ISO ì´ë¯¸ì§ë ì ë¢°í  ì ìë ìëªì¼ë¡ ê²ì¦ëììµëë¤. ì´ ISO ì´ë¯¸ì§ë ì ë¢°í  ì ìë ìëªì¼ë¡ ê²ì¦ëììµëë¤. ì´ ISOë Windows ì´ë¯¸ì§ì¸ ê² ê°ìµëë¤. ê³µì ISO ì´ë¯¸ì§ìëë¤. ì´ ììì USB ì¤í±ìì ëª¨ë  ë°ì´í°ë¥¼ ì­ì í©ëë¤. ì ë§ë¡ ì§ííìê² ìµëê¹? URL USB ì´ë¯¸ì§ ë¼ì´í° USB USB í¬ë§· ì¥ì¹ USB ì¤í±: ì ì ìë ìëª ì ë¢°í  ì ìë ìëª ê²ì¦ ì´ë¯¸ì§ì ì§ì ë° ë¬´ê²°ì± ê²ì¦ ë³¼ë¥¨ ì´ë¦: ë³¼ë¥¨: Windows ì´ë¯¸ì§ë í¹ë³í ì²ë¦¬ê° íìí©ëë¤. ë§ë¤ê¸° ë°ì´í¸ ì ì ìì 