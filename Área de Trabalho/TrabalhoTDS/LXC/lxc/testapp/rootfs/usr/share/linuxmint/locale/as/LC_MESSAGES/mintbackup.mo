��          �      �        )   	  4   3     h     t     �     �  !   �     �     �  "   �  
     2        R     j  4   �     �  g   �  \   C  \   �  B   �  &   @  )   g  �  �  ,   %  <   R  	   �     �     �     �  $   �  '   �       3   .     b  8   o     �     �  8   �     	  x   6	  d   �	  S   
  =   h
  /   �
  -   �
                                   
   	                                                                  %s is not located in your home directory. An error occurred while opening the backup file: %s. Backing up: Backup Tool Backups Calculating... No packages need to be installed. Please choose a backup file. Please choose a directory. Please select packages to install. Restoring: Skipping %s because named pipes are not supported. The backup was aborted. The following errors occurred: The list below shows the applications you installed. The restoration was aborted. This backup file is either too old or it was created with a different tool. Please extract it manually. Warning: Some files were not saved. Only %(archived)d files were backed up out of %(total)d. Warning: The meta file could not be saved. This backup will not be accepted for restoration. You do not have the permission to write in the selected directory. Your files were successfully restored. Your files were successfully saved in %s. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-03-16 01:28+0000
Last-Translator: Jeann Wilson <Unknown>
Language-Team: Assamese <as@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s não foi localizado na sua pasta pessoal. Ocorreu um erro durante a abertura do arquivo de backup: %s. Copiando: Ferramenta de Backup Backups Calculando... Nenhum pacote precisa ser instalado. Por favor escolha um arquivo de backup. Por favor escolha uma pasta. Por favor, selecione os pacotes a serem instalados. Restaurando: Ignorando %s porque pipes nomeados não são suportados. O backup foi cancelado. Ocorreram os seguintes erros: A lista abaixo exibe as aplicações que você instalou. A restauração foi cancelada. Este arquivo de backup é muito antigo ou ele foi criado com uma ferramenta diferente. Por favor, extraia-o manualmente. Aviso: Alguns arquivos não foram salvos. Somente %(archived)d arquivos foram copiados de %(total)d. Aviso: O arquivo meta não pode ser salvo. Este backup não poderá ser restaurado. Você não tem permissão para escrever na pasta selecionada. Os seus arquivos foram restaurados com sucesso. Seus arquivos foram salvos com sucesso em %s. 