��    O      �  k         �  )   �  2   �          (     ;     Q     `     f     o     ~      �  :   �     �         
     
     #
     6
     9
     H
     X
     i
     q
  
   �
  
   �
     �
  1   �
     �
     �
     �
                    .     F     W  "   j  
   �     �     �  
   �     �     �     �     �          
  -     >   ;  #   z  )   �     �  I   �  "   *  4   M  #   �  #   �  5   �  2      5   3  $   i     �  N   �     �       	          
   0     ;     M     a  2   h     �     �  *   �     �     �     �  �  �  0   �  ;   �               +     C     P  
   U     `     q      �  3   �     �    �  
     	        $     <     ?     N     _     q     }     �  
   �     �  C   �     �     
  -        ;     H  )   K  &   u     �     �  ,   �  
   �            
        )     6     J     Z     j     v  %   y  <   �  !   �     �       C   4     x  )   �  )   �  $   �  0     :   @  ;   {  )   �  !   �  W        [     b  
   u  '   �     �     �     �     �  )   �       
     )   )     S     Y     ^     K   2              +             &          6          9   -                               .   N       A   L   '          %   5   
   ,       8   C   :         B       "       )   0          !           3       /   D              J   7          	   M          1                      H   $         >   <       ;       ?   4   F         *       (   =   #   @         G       E       I   O           An error occured while copying the image. An error occured while creating a partition on %s. An error occurred An error occurred. Authentication Error. Calculating... Check Checksum Checksum files Checksum mismatch Choose a name for your USB Stick Download the ISO image again. Its checksum does not match. Everything looks good! FAT32 
  + Compatible everywhere.
  -  Cannot handle files larger than 4GB.

exFAT
  + Compatible almost everywhere.
  + Can handle files larger than 4GB.
  -  Not as compatible as FAT32.

NTFS 
  + Compatible with Windows.
  -  Not compatible with Mac and most hardware devices.
  -  Occasional compatibility problems with Linux (NTFS is proprietary and reverse engineered).

EXT4 
  + Modern, stable, fast, journalized.
  + Supports Linux file permissions.
  -  Not compatible with Windows, Mac and most hardware devices.
 Filesystem: Format Format a USB stick GB GPG signatures GPG signed file GPG signed file: Go back ISO Verification ISO image: ISO images ISO: If you trust the signature you can trust the ISO. Integrity check failed KB Key not found on keyserver. Local files MB Make a bootable USB stick Make bootable USB stick More information No volume ID found Not enough space on the USB stick. SHA256 sum SHA256 sums file SHA256 sums file: SHA256sum: Select Image Select a USB stick Select an image Signed by: %s Size: TB The SHA256 sum of the ISO image is incorrect. The SHA256 sums file does not contain sums for this ISO image. The SHA256 sums file is not signed. The USB stick was formatted successfully. The checksum is correct The checksum is correct but the authenticity of the sum was not verified. The gpg file could not be checked. The gpg file could not be downloaded. Check the URL. The image was successfully written. The sums file could not be checked. The sums file could not be downloaded. Check the URL. This ISO image is verified by a trusted signature. This ISO image is verified by an untrusted signature. This ISO looks like a Windows image. This is an official ISO image. This will destroy all data on the USB stick, are you sure you want to proceed? URLs USB Image Writer USB Stick USB Stick Formatter USB stick: Unknown signature Untrusted signature Verify Verify the authenticity and integrity of the image Volume label: Volume: Windows images require special processing. Write bytes unknown Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-09-26 17:39+0000
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Der opstod en fejl under kopiering af aftrykket. Der opstod en fejl under oprettelse af en partition på %s. Der opstod en fejl Der opstod en fejl. Fejl under godkendelse. Beregner … Tjek Kontrolsum Kontrolsumsfiler Uoverensstemmelse i kontrolsum Vælg et navn til din USB-nøgle Hent ISO-aftrykket igen. Kontrolsummen passer ikke. Alt ser fint ud! FAT32 
  + Kompatibelt med alt.
  - Kan ikke håndtere filer større end 4 GB.

exFAT
  + Kompatibelt med alt.
  + Kan håndtere filer større end 4 GB.
  - Ikke så kompatibel som FAT32.

NTFS 
  + Kompatibel med Windows.
  - Ikke kompatibel med Mac og de fleste hardwareenheder.
  - Af og til kompatibilitetsproblemer med Linux (NTFS er proprietært og reverse-engineered).

EXT4 
  + Moderne, stabilt, hurtigt, journaliseret.
  + Understøtter Linux-filrettigheder.
  - Ikke kompatibel med Windows, Mac og de fleste hardwareenheder.
 Filsystem: Formatér Formatér en USB-nøgle GB GPG-signaturer GPG-signeret fil GPG-signeret fil: Gå tilbage ISO-bekræftelse ISO-aftryk: ISO-aftryk ISO: Har du tillid til signaturen, kan du have tillid til ISO-aftrykket. Integritetstjek mislykkedes kB Nøglen kunne ikke findes på nøgleserveren. Lokale filer MB Lav en USB-nøgle, som der kan bootes fra Lav USB-nøgle, som der kan bootes fra Flere oplysninger Intet diskenheds-ID fundet Der er ikke nok ledig plads på USB-nøglen. SHA256-sum SHA256-sumfil SHA256-sumfil: SHA256sum: Vælg aftryk Vælg en USB-nøgle Vælg et aftryk Signeret af: %s Størrelse: TB ISO-aftrykkets SHA256-sum er forkert. SHA256-sumfilen indeholder ikke summer for dette ISO-aftryk. SHA256-sumfilen er ikke signeret. USB-nøglen blev formateret. Kontrolsummen er korrekt Kontrolsummen er korrekt, men summens ægthed blev ikke bekræftet. GPG-filen kunne ikke tjekkes. GPG-filen kunne ikke hentes. Tjek URL'en. Aftrykket blev overført til USB-nøglen. Kontrolsumsfilen kunne ikke tjekkes. Kontrolsumsfilen kunne ikke hentes. Tjek URL'en. Dette ISO-aftryk er bekræftet med en pålidelig signatur. Dette ISO-aftryk er bekræftet med en upålidelig signatur. Dette ISO-aftryk ligner et Windowsaftryk. Dette er et officielt ISO-aftryk. Dette vil slette alle data på USB-nøglen. Er du sikker på, du ønsker at fortsætte? URL'er USB-aftryksskriver USB-nøgle Værktøj til formatering af USB-nøgle USB-nøgle: Ukendt signatur Upålidelig signatur Bekræft Bekræft aftrykkets ægthed og integritet Enhedens mærkat: Diskenhed: Windowsaftryk kræver speciel behandling. Skriv byte ukendt 