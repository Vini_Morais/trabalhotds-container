��          t      �              %   0     V     f     u  q   �     �             -   1  �  _        +        H     \  
   o  �   z     	     %     4  ,   E                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-05-03 10:38+0000
Last-Translator: Umidjon Almasov <Unknown>
Language-Team: Uzbek <uz@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: uz
 %s yaroqli domen nomi emas. Tanlangan domen nomlariga kirishni bloklash Bloklangan domenlar Domen bloklovchisi Domen nomi Domen nomlari harf yoki raqam bilan boshlanishi va tugashi kerak va faqat harflar, raqamlar, nuqtalar va defislardan iborat bo‘lishi mumkin. Misol: my.number1domain.com Yaroqsiz domen Ota-ona nazorati Bloklamoqchi bo‘lgan domen nomini kiriting 