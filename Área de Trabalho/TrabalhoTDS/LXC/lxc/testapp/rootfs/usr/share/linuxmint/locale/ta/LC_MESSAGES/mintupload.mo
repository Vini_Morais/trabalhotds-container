��    +      t  ;   �      �     �  6   �  4   �     3     9     ;     B     Q     b     h  #   �     �  "   �  "   �          #     &     *  !   0     R     U     Y     \  	   `     j     p     v     �  	   �  !   �  
   �     �     �     �     �     �          !     7     =     C  #   I  �  m     (  x   D  �   �     �	     �	     �	  J   �	  4   
     9
  ]   F
  l   �
  J     �   \  �   �  i   �     �     �       w        �     �     �     �     �     �     �  4   �  )   +     U  j   l      �  
   �       /     1   8  7   j  .   �  C   �          &     :  _   H                	      !                              $       &          +   #         "   '   %      
                                                      (                )                *                  %s Properties <b>Please enter a name for the new upload service:</b> <i>Try to avoid spaces and special characters...</i> About B Cancel Cancel upload? Check connection Close Could not get available space Could not save configuration change Define upload services Do you want to cancel this upload? File larger than service's maximum File uploaded successfully. GB GiB Host: Hostname or IP address, default:  KB KiB MB MiB Password: Path: Port: Run in the background Service name: Services: This service requires a password. Timestamp: Type: URL: Unknown service: %s Upload Manager Upload manager... Upload services Uploading the file... User: _File _Help connection successfully established Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-02 09:08+0000
Last-Translator: Kamala Kannan <Unknown>
Language-Team: Tamil <ta@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:35+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s பண்புகள் <b>புதிய பதிவேற்ற சேவையின் பெயரை உள்ளிடவும்:</b> <i>இடைவெளிகள் மற்றும் சிறப்பு எழுத்துக்களை தவிர்க்க முயற்சி செய்யுங்கள்...</i> பற்றி B இரத்துசெய் பதிவேற்றத்தை ரத்துசெய்யவா? இணைப்பை சரிபார்க்க மூடு கிடைக்கும் இடத்தை பெற முடியவில்லை கட்டமைப்பு மாற்றத்தை சேமிக்க இயலவில்லை பதிவேற்ற சேவைகளை வரையறுக்க நீங்கள் இந்த பதிவேற்றத்தை ரத்து செய்ய விரும்புகிறீர்களா? சேவையின் அதிகப்பட்சத்தை விட கோப்பு பெரிதாக உள்ளது கோப்பு வெற்றிகரமாக பதிவேற்றப்பட்டது. ஜிபி GiB புரவலன்: புரவலன் பெயர் அல்லது ஐபி முகவரி, இயல்புநிலை:  கேபி KiB எம்பி MiB கடவுச்சொல்: பாதை: நுழைவு பின்னணியில் இயக்கு சேவையின் பெயர்: சேவைகள்: இந்த சேவைக்கு கடவுச்சொல் தேவையாகிறது. நேர அமைப்பு: வகை: URL: அறியப்படாத சேவை: %s பதிவேற்ற நிர்வாகி பதிவேற்று நிர்வாகி... சேவைகளை பதிவேற்ற கோப்பை பதிவேற்றுகிறது... பயனர்: _கோப்பு _உதவி இணைப்பு வெற்றிகரமாக நிறுவப்பட்டது 