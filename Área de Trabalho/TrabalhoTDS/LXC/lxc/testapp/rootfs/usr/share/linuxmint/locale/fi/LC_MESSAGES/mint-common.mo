��          �   %   �      p      q     �     �     �  &   �  %     "   ?  (   b  #   �  $   �     �  =   �  	   $     .     6     ?  5   G     }  0   �     �  	   �     �     �  '   �  _        q     �  �  �     3     S     j  $   �  %   �     �     �  %        .      J     k  >   z     �     �  	   �     �  2   �     	     +	  	   K	     U	     f	     m	     }	  Y   �	     �	  	   
                       	                                  
                                                                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required Additional software will be downgraded Additional software will be installed Additional software will be purged Additional software will be re-installed Additional software will be removed Additional software will be upgraded An error occurred Cannot remove package %s as it is required by other packages. Downgrade Flatpak Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Purge Reinstall Remove Skip upgrade The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Updates will be skipped Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-11-29 17:09+0000
Last-Translator: Kimmo Kujansuu <Unknown>
Language-Team: Finnish <fi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: fi
 Levytilaa tarvitaan lisää %s. Levytilaa vapautuu %s. %s ladataan yhteensä. Ylimääräisiä muutoksia vaaditaan Lisäohjelmistot palautetaan aiempaan Lisäohjelmistot asennetaan Lisäohjelmistot tyhjennetään Lisäohjelmistot asennetaan uudelleen Lisäohjelmistot poistetaan Lisäohjelmistoja päivitetään Tapahtui virhe Pakettia %s ei voi poistaa, koska muut paketit vaativat sitä. Palauta aiempaan Flatpak Flatpakit Asenna Paketti %s on riippuvainen seuraavista paketeista: Poistettavat paketit Tarkista alla olevat muutokset. Puhdistus Asenna uudelleen Poista Ohita päivitys Seuraavat paketit poistetaan: Tämä valikkokohta ei liity mihinkään pakettiin. Haluatko silti poistaa sen valikosta? Päivitykset ohitetaan Päivitä 