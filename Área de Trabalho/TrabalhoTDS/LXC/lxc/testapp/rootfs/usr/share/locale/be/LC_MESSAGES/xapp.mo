��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  L        U     h     {     �     �     �  )   �               $     1     @     M  
   e     p     �     �  4   �  
   �  
   �  
   �     �     	     &	  +   /	  E   [	  +   �	  7   �	  #   
     )
     <
            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-05-23 11:49+0000
Last-Translator: Anton Hryb <Unknown>
Language-Team: Belarusian <be@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Аб праграме Дзеянні Дазволіць шляхі Праграмы Вобласць, у якой з'яўляецца XApp стан значка Аглядзець Скасаваць Катэгорыі Катэгорыя Выберыце значок Перадвызначана Перадвызначаны значок Прылады Эмблемы Эмодзі Абраныя Значок Памер значка Выява Загрузка... Тыпы міма Адчыніць Аперацыя не падтрымліваецца Іншае Месцы Пошук Выбраць Выбраць выяву Стан Катэгорыя па змаўчанні. Значок выкарыстоўваецца па змаўчанні Пераважны памер значка. Радок, які прадстаўляе значок. Ці дазваляць шляхі. XApp Status Applet XApp Status Applet Factory 