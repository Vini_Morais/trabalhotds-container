��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �     h     �     �  #   �     �     �  	          1   ,     ^  !   d  Y   �  	   �                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-07-09 12:28+0000
Last-Translator: André Savik <Unknown>
Language-Team: Norwegian Bokmal <nb@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s diskplass vil bli brukt. %s diskplass vil bli frigjort. %s vil lastes ned totalt. Ytterligere endringer er nødvendig Det oppstod en feil Flatpak-instanser Installer Pakker som vil bli fjernet Vennligst se over listen over endringer nedenfor. Fjern Følgende pakker vil bli fjernet: Dette menyelementet er ikke assosiert med en pakke. Vil du fjerne det fra menyen uansett? Oppgrader 