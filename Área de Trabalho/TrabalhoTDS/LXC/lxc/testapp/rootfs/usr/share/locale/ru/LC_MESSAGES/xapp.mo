��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  L     
   [     f     s     �     �     �  $   �     �     	          %     8     E     _     v     �     �  2   �     �  
   �  
   �     �  0   	     =	  +   J	  >   v	  ;   �	  :   �	  !   ,
      N
  H   o
            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-01-10 06:20+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 О программе Действия Разрешать пути Приложения Область, где появляется значок статуса XApp Обзор Отмена Категории Категория Выберите значок По умолчанию Значок по умолчанию Устройства Эмблемы Эмодзи Избранное Значок Размер значка Изображение Загрузка… Типы MIME Открыть Действие не поддерживается Другое Места Поиск Выбрать Выберите файл изображения Статус Категория по умолчанию. Значок, используемый по умолчанию Предпочтительный размер значка. Строка, соответствующая значку. Разрешать ли пути. Апплет статуса XApp Заводские настройки апплета статуса XApp 