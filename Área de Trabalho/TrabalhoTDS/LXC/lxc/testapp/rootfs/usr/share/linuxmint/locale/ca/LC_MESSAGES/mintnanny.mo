��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  "   �  1   "     T     g     ~  �   �          -     >  -   M                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-08-17 20:54+0000
Last-Translator: Robert Antoni Buj Gelonch <Unknown>
Language-Team: Catalan <ca@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s no és un nom de domini vàlid. Bloqueja l'accés als noms de domini seleccionats Dominis bloquejats Bloquejador de dominis Nom de domini Els noms de domini han de començar i acabar amb una lletra o un dígit, i només poden contenir lletres, dígits, punts i guions. Exemple: my.number1domain.com Domini no vàlid Control patern Teclegeu el nom de domini que voleu bloquejar 