��    -      �  =   �      �     �     �                          )     :     B     S     a  -   o     �     �  
   �     �  
   �  	   �     �  
   �     �                    #     5     E     M  0   U     �     �     �     �     �  	   �     �     �               (     7     ?     E     X  �  k          %     ?  +   R     ~     �  2   �     �  %   �  (   	     ?	  o   L	  W   �	     
  '   
     C
     S
     p
     �
     �
  ;   �
     �
  +        1  1   J     |     �     �  �   �  6   N  <   �  	   �  ;   �  >        G     d  G   �     �  �   �  1   ~     �     �     �  e  �               '   !   *   	       (   $                                                    ,      #                "                    +       
   )      -                                            &           %    Buttons labels: Buttons layout: Compiz Compiz settings Compton Computer Configure Compiz Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Marco Marco settings Metacity Metacity settings Mounted Volumes Network Openbox Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only Toolbars Traditional style (Right) Trash Use system font in titlebar Window Manager Windows Xfwm4 root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-01-06 15:23+0000
Last-Translator: Vikram Kulkarni <kul.vikram@gmail.com>
Language-Team: Marathi <mr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 बटन लेबल कळ संरचना कोंपिज कोंपिज सेटिंग्ज कॉम्पटन संगणक कोंपिज संयोजीत करा डेस्कटॉप डेस्कटॉप रचना डेस्कटॉप चिन्ह तोटे विंडो सरकवताना (drag) करताना मजकूर दर्शवू नका डेस्कटॉपचे गुणधर्म अनुकुलीत करा घर चिन्ह आकारमान : चिन्ह फक्त चिन्ह इंटरफेस विशाल लिनक्स मिंट मॅकची पद्धत (डावी बाजू) मार्को मार्को सेटिंग्ज मेटासिटी मेटासिटी सेटिंग्ज स्थापित खंड नेटवर्क ओपनबाक्स आपण डेस्कटॉपवर पाहू इच्छित असलेल्या वस्तू निवडा: मेनू मध्ये बटन दाखवा मेनू मध्ये चिन्ह दाखवा लघु घटकांच्या खालील मजकूर घटकांच्या बाजूचा मजकूर फक्त मजकूर साधनपट्टी पारंपारिक पद्धत (उजवी बाजू) कचरापेटी शीर्षकपट्टीमध्ये संगणक प्रणालीच्या मुख्य फाँटचा वापर करा विण्डो व्यवस्थापक विंडोज Xfwm4 root@linuxmint.com Launchpad Contributions:
  Abhijeet Shrikant Torane https://launchpad.net/~abhijeet-torane
  Ajinkya Bhosale https://launchpad.net/~ajax7877
  Dhruv https://launchpad.net/~meetsangvikar
  Digvijay Patankar https://launchpad.net/~dbpatankar
  Vikram Kulkarni https://launchpad.net/~kul-vikram
  सुजित जाधव https://launchpad.net/~sujitjadhav 