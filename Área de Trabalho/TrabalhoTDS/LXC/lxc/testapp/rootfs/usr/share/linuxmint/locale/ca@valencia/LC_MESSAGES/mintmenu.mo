��          �      L      �     �     �     �     �       	        "     '     8     =     C     K     W  $   f     �     �     �     �  �  �     �     �     �  $   �      �     
               5     9     >     F     T  $   g      �     �     �     �                                            	                 
                                       About Advanced MATE Menu Applications Couldn't initialize plugin Couldn't load plugin: Edit menu Menu Menu preferences Name Name: Options Preferences Reload plugins Remember the last category or search Search for packages to install Show button icon Show category icons Swap name and generic name Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-02-15 19:00+0000
Last-Translator: Juan Hidalgo-Saavedra <Unknown>
Language-Team: Catalan (Valencian) <ca@valencia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Sobre Menú avançat de MATE Aplicacions No s'ha pogut inicialitzar el plugin No s'ha pogut carregar el plugin Editar menú Menú Preferències del menú Nom Nom: Opcions Preferències Recarregar plugins Recordar l'última categoria o busca Buscar paquets per a instal·lar Mostrar la icona del botó Mostrar icones en categories Alternar nom i nom genèric 