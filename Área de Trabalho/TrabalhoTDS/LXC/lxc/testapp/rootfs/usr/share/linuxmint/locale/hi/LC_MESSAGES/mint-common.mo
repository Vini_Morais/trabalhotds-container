��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p  [   �  B   W  2   �  B   �       �   -     �  %   �  t     )   �  ^   �       C   %  �   i  "   +	                                       	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-18 15:39+0000
Last-Translator: Panwar <Unknown>
Language-Team: Hindi <hi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s अतिरिक्त डिस्क स्पेस उपयोग होगा। %s डिस्क स्पेस मुक्त होगा। कुल %s डाउनलोड होगा। अतिरिक्त बदलाव आवश्यक है त्रुटि हुई %s पैकेज हटाना संभव नहीं है क्योंकि यह दूसरे पैकेज हेतु आवश्यक है। फ्लैटपैक इंस्‍टॉल करें %s पैकेज निम्नलिखित पैकेज का आश्रित पैकेज है : हटने वाले पैकेज कृपया निम्नलिखित बदलाव सूची देखें। हटाएँ निम्नलिखित पैकेज हटेंगे : यह मेन्यू आइटम किसी पैकेज से संबंधित नहीं है। क्या आप इसे हटाना चाहते हैं ? अपग्रेड करें 