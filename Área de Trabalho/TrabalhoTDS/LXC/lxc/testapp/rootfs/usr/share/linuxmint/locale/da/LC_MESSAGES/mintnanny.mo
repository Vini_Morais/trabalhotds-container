��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  "     &   %     L     _     p  r   |     �             0   1                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-01-29 09:21+0000
Last-Translator: scootergrisen <scootergrisen@gmail.com>
Language-Team: Danish <da@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s er ikke et gyldigt domænenavn. Blokér adgang til valgte domænenavne Blokerede domæner Domæneblokering Domænenavn Domænenavne skal begynde med et bogstav eller tal og kan kun indeholde bogstaver, tal, punktummer og bindesteger. Eksempel: mit.nummer1domæne.dk Ugyldigt domæne Forældrekontrol Indtast venligst det domænenavn, du vil blokere 