��          t      �              %   0     V     f     u  q   �     �             -   1  �  _      �  (        ;     N     a  �   p     (     H     Z  .   p                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2017-10-26 05:28+0000
Last-Translator: Pavel Borecki <Unknown>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s není platný název domény. Blokovat přístup k vybraným doménám Blokované domény Blokování domén Název domény Je třeba, aby název domény začínal a končil buď písmenem nebo číslicí a obsahoval pouze písmena (bez háčků a čárek), číslice, tečky a spojovníky („pomlčky“). Příklad: moje.domenacislo1.cz Neplatná doména Rodičovská kontrola Zadejte název domény, kterou chcete blokovat 