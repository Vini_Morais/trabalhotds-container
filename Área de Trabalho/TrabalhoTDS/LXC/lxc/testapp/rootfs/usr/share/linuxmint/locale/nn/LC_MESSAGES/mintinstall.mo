��    +      t  ;   �      �  ?   �  B   �  !   <  $   ^     �     �     �     �     �     �     �  	   �     �  	   �     �     �     �     �     �     �  	              )     7     >     F     R  
   ^     i     |     �     �     �  3   �  *   �     	          0     @     M     a     i  �  m  K   1  B   }  )   �  !   �     	     	  	   	  	   	     &	     +	     4	  	   =	     G	  
   N	  	   Y	     c	     o	     t	  	   {	     �	  
   �	  	   �	     �	     �	     �	  	   �	     �	  	   �	     �	     
     
     
     
  (   1
  !   Z
     |
     �
     �
     �
     �
     �
     �
            "         !                    (                          +   '                %       *                                #       &                	          $                 )   
                   %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required 3D About Accessories Board games Chat Details Drawing Education Email Emulators File sharing Fonts Games Graphics Install Install new applications Installed Internet Not installed Office Package Photography Programming Publishing Real-time strategy Remove Reviews Scanning Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Simulation and racing Software Manager Sound and video System tools Turn-based strategy Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2012-11-27 17:09+0000
Last-Translator: Andreas N. <Unknown>
Language-Team: Norwegian Nynorsk <nn@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %(downloadSize)s må lastast ned, %(localSize)s lagringsplass vert frigjort %(downloadSize)s må lastast ned, krev %(localSize)s lagringsplass %(localSize)s lagringsplass vart frigjort Treng %(localSize)s lagringsplass 3D Om Tilbehør Brettspel Prat Detaljar Teikning Utdanning E-post Emulatorar Fildeling Skrifttypar Spel Bilete Installer Installer nye program Installert Internett Ikkje installert Kontor Pakke Fotografi Programmering Utgjeving Sanntidsstrategi Fjern Vurderingar Skanning Vitskap og utdanning Søk i pakkeskildringane (endå tregare) Søk i pakkesamandraget (tregare) Simulering og tevling Programvarebehandlar Lyd og video Systemverktøy Rundebasert strategi Framvisarar Vev 