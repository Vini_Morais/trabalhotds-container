��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  )   m  !   �     �  +   �            	   $     .  .   B     q  (   z  f   �     
                            	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-11-05 20:07+0000
Last-Translator: Quentin PAGÈS <Unknown>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s d’espaci disc serà utilizat en mai. %s d’espaci disc serà liberat. %s serà telecargar en total. De cambiaments suplementaris son requesits. Una error s'es produita Flatpaks Installar Paquets de suprimir Consultatz la lista dels cambiaments çaijós. Suprimir Los paquets seguents seràn suprimits : Aqueste element del menú es pas associat a un paquet. O volètz suprimir dins lo menú, malgrat tot ? Metre a nivèl 