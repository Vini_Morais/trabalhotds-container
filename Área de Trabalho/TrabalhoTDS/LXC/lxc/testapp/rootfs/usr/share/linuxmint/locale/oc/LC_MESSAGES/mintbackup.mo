��          �      L      �     �     �     �     �     �     �       $        D     a     |     �  
   �  
   �  
   �     �  B   �  &     �  )     �     �  
   �               ,     C  8   X  &   �     �     �     �     �     �            A   2  5   t     	                                   
                                                            Backing up: Backup Tool Backups Calculating... Deselect all Exclude directories Exclude files Make a backup of your home directory Please choose a backup file. Please choose a directory. Refresh Remove Restore... Restoring: Select all Software selection You do not have the permission to write in the selected directory. Your files were successfully restored. Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-04-20 18:32+0000
Last-Translator: Quentin PAGÈS <Unknown>
Language-Team: Occitan (post 1500) <oc@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Salvament en cors : Aisina de còpia de seguretat Salvaments Calcul en cors... Deseleccionar tot Exclure de repertòris Exclure de fichièrs Far una còpia de seguretat de vòstre dorsièr personal Causissètz un fichièr de salvagarda. Causissètz un repertòri. Metre a jorn Levar Restablir... Restabliment en cors : Seleccionar tot Seleccion de logicials Avètz pas la permission d’escriure sul repertòri seleccionat. Vòstres fichièrs son estats restablits amb succès. 