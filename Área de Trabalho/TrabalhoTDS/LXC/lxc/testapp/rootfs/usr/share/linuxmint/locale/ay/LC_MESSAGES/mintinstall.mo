��    V      �     |      x  ?   y  B   �  !   �  $         C     d     g     m     y     }     �  5   �     �     �  _   �     W	     e	  3   j	  +   �	     �	     �	     �	  	   �	     �	     �	  	   
  
   
     
     '
  Z   4
  C   �
     �
     �
     �
     �
     �
  	   	       
   *  F   5     |     �     �     �     �     �     �     �  L   �     K     Y     g     n     r     z  -   �  +   �     �  
   �     �     �               6     =  D   F     �     �     �     �  3   �  *   �  +        E     a     w     �     �     �  6   �  E   �     (     <     B     J  �  N  T     O   Z  4   �  0   �  )        :     =     D     S     \     u      �     �     �  \   �     ;  
   S  9   ^     �     �     �     �  	   �     �               )     7  
   M  h   X  G   �     	       
   '  
   2  !   =  	   _     i     �  =   �     �     �  	   �     �          (  $   B  (   g  p   �               *     A     E  	   N     X  -   x     �     �     �     �     �      �          "  C   2     v       	   �     �  6   �  8   �  2   "  %   U     {     �     �     �     �  (   �  -        6     P     W     e         4   "       /   %   K              	   R   :      <   0          H              ;               1         ?          *                  Q   D                    L   G      A           T   (       !               $   '   @   
               V               F   -          C           M   8      S   7      5          J         U   .       N   B   6       2      &       9   I   O   +   E              =       ,   P          >                        )   #            3       %(downloadSize)s to download, %(localSize)s of disk space freed %(downloadSize)s to download, %(localSize)s of disk space required %(localSize)s of disk space freed %(localSize)s of disk space required %d task running %d tasks running 3D About Accessories All All Applications All operations complete An error occurred attempting to add the Flatpak repo. Board games Cannot install Cannot process this file while there are active operations.
Please try again after they finish. Cannot remove Chat Click <a href='%s'>here</a> to add your own review. Currently working on the following packages Details Drawing Editors' Picks Education Electronics Email Emulators Essentials File sharing First-person Flatpak support is not currently available. Try installing flatpak and gir1.2-flatpak-1.0. Flatpak support is not currently available. Try installing flatpak. Fonts Games Graphics Install Install new applications Installed Installed Applications Installing Installing this package could cause irreparable damage to your system. Internet Java Launch Limit search to current listing Maths Multimedia Codecs Multimedia Codecs for KDE No matching packages found No packages to show.
This may indicate a problem - try refreshing the cache. Not available Not installed Office PHP Package Photography Please be patient. This can take some time... Please use apt-get to install this package. Programming Publishing Python Real-time strategy Refresh Refresh the list of packages Remove Removing Removing this package could cause irreparable damage to your system. Reviews Scanning Science Science and Education Search in packages description (even slower search) Search in packages summary (slower search) Searching software repositories, one moment Show installed applications Simulation and racing Software Manager Sound Sound and video System tools The Flatpak repo you are trying to add already exists. There are currently active operations.
Are you sure you want to quit? Turn-based strategy Video Viewers Web Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-15 01:54+0000
Last-Translator: amosbatto <amosbatto@yahoo.com>
Language-Team: Aymara <aym@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
Language: ay
 %(downloadSize)s apaqañataki,
 %(localSize)s ch'usawxa tisku (disco) ukat antutjata %(downloadSize)s apaqañataki, %(localSize)s ch'usawxa tisku(disco) ukat munasi %(localSize)s ch'usawxa tisku (disco) ukat antutjata %(localSize)s ch'usawxa tisku(disco) ukat munasi %d luraw lurasinxa %d lurawinak lurasinxa 3D Akxata Phuqhayirinaka Taqpacha Taqpach aplikasiyunanaka Taqpach lurawinakax phuqhasiwa Flatpak pirwa yapasinx pantjiwa. Misan anatañanaka Janiw uchantaskaspati Naktat lurawinak utjkipanxa, aka wayaqx janiw sarachaskaspati.
Tukt'ayasinxa mayamp yant'am. Janiw chhaqtayaskaspati Aruskipawi Klik luram <a href='%s'>akjaru</a> arst'awim yapañataki. Jutir q'ipichanakan irnaqasiski Juk'amp yatiyawi Jamuqa Amuyt'awinaka Yatichawi Illapata yänaka (Electrónica) E-chaski Tukurinaka (emuladores) Wakiskirinaka Wayaqanak turkakipawi Mayir jaqi Flatpak aplikasiyunanakax janiw utjkiti. flatpak ukhamaraki gir1.2-flatpak-1.0 q'ipichanak uchantasmawa. Flatpak aplikasiyunanakax janiw utjkiti. flatpak q'ipicha uchantasmawa. Qillqa kastanaka Anatañanaka Jamuqanaka Uchantaña Machaq aplikasiyunanak uchantaña Uchantata Uchantat aplikasiyunanak Uchantasina Aka q'ipicha uchantasax, sistema ukarux janiw walt'aykaspati. Llikapura (Internet) Java Lurayaña Jichha tanta ukanaki thaqaña Jakhuchawi (matemáticas) Kirkimpi videompi códecs Kirkimp videompi códecs KDE ukataki Janiw ni mä puraptat q'ipicha jikiskiti No hay paquetes que mostrar Janiw q'ipichanak uñachayañatak utjkiti.
Q'ipichanak tanta ukat mayamp apaqasmawa. Janiw apnaqaskaspati Janiw uchantatakiti Irnaqañ uta (oficina) PHP Q'ipicha Phututaki Suyt'akim, akax qhipt'aspawa... apt-get apnaqam, aka q'ipicha uchantañataki. Wakichayawi Taqiru yatxatayaña Python Jichhpach luraw thakhi Jichhaptayaña Q'ipichanak tanta jichhaptayaña Chhaqhtayaña Chhaqhtayaskiwa Aka q'ipich chhaqhtayasax, sistema ukarux janipuniw walt'aykaspati. Arst'awi Ullakipaskiwa Yatxatawi Yatxatawimpi yatichäwimpi Q'ipichanaka yatiyawi ukan thaqhaña(juk'amp k'achaki) Juk'aptat q'ipichanaka ukan thaqhaña (juk'amp k'achaki) Software pirwanakan thaqhaskiwa, mä juk'a suyt'am Uchantat aplikasiyunanak uñachayaña Tukuwimpi t'ijuñanakampi Software irpiri Kirkiwi Kirkiwimpi videompi Sistema ukan irnaqañ yänaka Flatpak pirwa yapañ munasktaxa utjxiwa. Naktat lurawinak utji.
¿Mistuñ munapuntati? Turnut turnu luraw thakhi Vídeo Uñjiyirinaka Llika (web) 