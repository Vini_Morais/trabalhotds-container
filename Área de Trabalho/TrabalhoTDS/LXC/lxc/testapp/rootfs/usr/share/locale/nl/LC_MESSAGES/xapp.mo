��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  /   �  	               	   '     1  	   D     N  	   a     k     t  
   �  	   �     �  
   �     �     �     �     �     �                         4     ;  $   R  (   w  1   �  .   �     	  *    	            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-11-25 19:04+0000
Last-Translator: Pjotr12345 <Unknown>
Language-Team: Dutch <nl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 Over Acties Paden toestaan Toepassingen Gebied waar XApp-statuspictogrammen verschijnen Verkennen Afbreken Categorieën Categorie Kies een pictogram Standaard Standaardpictogram Apparaten Emblemen Emotiepictogrammen Favorieten Pictogram Pictogramgrootte Afbeelding Bezig met laden… Bestandssoorten Openen Bewerking niet ondersteund Overige Locaties Zoeken Kiezen Kies afbeeldingsbestand Status De standaardcategorie. Het standaard te gebruiken pictogram De voorkeursgrootte van de pictogrammen. De tekenreeks die het pictogram vertegenwoordigt. Of paden al dan niet moeten worden toegestaan. Status-werkbalkhulpje van XApp Fabriek van status-werkbalkhulpje van XApp 