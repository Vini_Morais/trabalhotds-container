��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  1        =  
   F     Q     b     q     �     �     �     �     �     �     �     �  
             )  
   7     B     ]     d  
   q  
   |     �     �  %   �  )   �     �  ,   	     J	     j	  %   �	            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-07-27 09:07+0000
Last-Translator: Avi Markovitz <avi.markovitz@gmail.com>
Language-Team: Hebrew <he@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 על אודות פעולות לאפשר נתיבים יישומונים אזור שבו יופיעו סמלי מצב XApp עיון ביטול קטגוריות קטגוריה בחירת סמל ברירת מחדל סמל ברירת מחדל התקנים סמלונים אימוג׳י מועדפים סמל גודל סמל תמונה בהליכי טעינה... סוגי MIME פתיחה פעולה לא נתמכת אחר מקומות חיפוש בחירה בחירת קובץ תמונה מצב קטגורית ברירת המחדל. סמל לשימוש כברירת מחדל גודל הסמל המועדף. המחרוזת שמייצגת את הסמל. האם לאפשר נתיבים. יישומון מצב XApp מצב מפעל יישומוני XApp 