��            )   �      �  w   �     	          /     6     H     X  �   e               1  ^   O     �     �     �     �     �       	   !     +     =  ,   B     o     v     }     �     �     �  �  �  �   �               7     @     ^     p  �   �     1	  #   =	  #   a	  n   �	     �	  #   
     5
     A
     \
     v
     �
  &   �
  
   �
  0   �
     �
  	     !        .     L     `                                                  
                                         	                                               <b><small>Note: Installing or upgrading language packs can trigger the installation of additional languages</small></b> Add a New Language Add a new language Add... Apply System-Wide Fully installed Input method Input methods are used to write symbols and characters which are not present on the keyboard. They are useful to write in Chinese, Japanese, Korean, Thai, Vietnamese... Install Install / Remove Languages Install / Remove Languages... Install any missing language packs, translations files, dictionaries for the selected language Install language packs Install the selected language Language Language Settings Language settings Language support Languages No locale defined None Numbers, currency, addresses, measurement... Region Remove Remove the selected language Some language packs are missing System locale XIM Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-09-24 14:47+0000
Last-Translator: MinhTran <Unknown>
Language-Team: Vietnamese <vi@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 <b><small>Lưu ý: cài đặt hay nâng cấp gói ngôn ngữ có thể kéo theo việc cài đặt thêm ngôn ngữ</small></b> Thêm Ngôn ngữ Mới Thêm ngôn ngữ mới Thêm... Áp dụng Toàn Hệ thống Cài đầy đủ Phương thức nhập Phương thức nhập dùng để viết những ký hiệu và con chữ không có trên bàn phím. Thường dùng với tiếng Hoa, Nhật, Hàn, Thái, Việt... Cài đặt Cài đặt / Gỡ bỏ Ngôn ngữ Cài đặt / Gỡ bỏ Ngôn ngữ Cài các bộ ngôn ngữ, tập tin dịch thuật và từ điển còn thiếu cho ngôn ngữ đã chọn Cài đặt gói ngôn ngữ Cài đặt ngôn ngữ đã chọn Ngôn ngữ Thiết đặt Ngôn ngữ Thiết lập ngôn ngữ Hỗ trợ ngôn ngữ Ngôn ngữ Chưa định nghĩa cài đặt vùng Không có Số, tiền tệ, địa chỉ, đo lường... Vùng Gỡ bỏ Gỡ bỏ ngôn ngữ đã chọn Thiếu vài bộ ngôn ngữ Locale hệ thống XIM 