��          �      �       H      I     j     �     �     �     �     �     �  0        5  '   <  _   d     �  �  �  0   a  )   �     �  &   �               $     0  -   D     r  &   {  c   �                                 	      
                               %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Flatpaks Install Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2019-11-14 20:30+0000
Last-Translator: Flaviu <flaviu@gmx.com>
Language-Team: Romanian <ro@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 cu %s mai mult spațiu de pe disc va fi folosit. %s din spațiu de pe disc va fi eliberat. %s se va descărca în total. Modificări suplimentare sunt necesare A apărut o eroare Pachete Flatpak Instalează Pachete de eliminat Consultați lista modificărilor, de mai jos. Elimină Următoarele pachete vor fi eliminate: Acestui element din meniu nu îi este asociat nici un pachet. Doriți să îl eliminați din meniu? Actualizează 