��    )      d  ;   �      �  
   �     �     �     �     �     �     �     �  -        0     K  
   P     [  
   a  	   l     v  
   |     �     �     �     �     �      �  0   �          '     ;     A     R  	   d  5   n     �     �     �     �     �                    0  �  C  
   �     �  !        $     3      C     d     �  N   �  2   �     	     "	     8	     G	     [	     h	     o	     	  
   �	     �	     �	     �	  .   �	  q   
  +   �
  4   �
  
   �
     �
  &        :  W   H     �  '   �     �  W   �  7   ?     w  
   �     �  �   �        #       &                '                                         !            
                                   	      "      (   $              )              %                                     Advantages Buttons labels: Buttons layout: Computer Desktop Desktop Settings Desktop icons Disadvantages Don't show window content while dragging them Fine-tune desktop settings Home Icon size: Icons Icons only Interface Large Linux Mint Mac style (Left) Marco Metacity Mounted Volumes Network Overview of some window managers Select the items you want to see on the desktop: Show icons on buttons Show icons on menus Small Text below items Text beside items Text only The Windows section lets you choose a window manager. Toolbars Traditional style (Right) Trash Use system font in titlebar Welcome to Desktop Settings. Window Manager Windows root@linuxmint.com translator-credits Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-11-29 19:03+0000
Last-Translator: Waqar Ahmed <waqar.17a@gmail.com>
Language-Team: Urdu <ur@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 فوائد بٹنوں کے لیبل بٹن دائیں یا بائیں کمپیوٹر ڈیسک ٹاپ سطحِ مکتب ترتیبات ڈیسک ٹاپ آئیکون نقصانات ونڈو کو ہلاتے ہوئے اس کے مشمولات نہ دکھائیں فائن ٹیون ڈیسکٹ ٹاپ ترتیبات صفحہ اول آئیکون سائز آئیکونز صرف آئیکون مواجہہ بڑا لنکس منٹ میک انداز ۔ بائیں میکرو میٹاسٹی ماؤنٹ شدہ والیمز نیٹ ورک کچھ ونڈو منتظمین کا جائزہ ان عناصر کو منتخب کیجئے جنہیں آپ سطحِ مکتب پر دیکھنا چاہتے ہیں: بٹنوں پر آئیکون دکھائیں مینیو میں آئیکون بھی دکھائیں چھوٹا متن عناصر کے نیچے متن عناصر کے ایک جانب صرف متن ّونڈوز سیکشن میں آپ ونڈو منتظم پسند کر سکتے ہیں۔ اوزاری پٹیاں روائیتی انداز ۔ دائیں ردی عنوان کی پٹی کیلئے نظام کا رسم الخط استعمال کریں ڈیسک ٹاپ ترتیبات میں خوش آمدید ونڈو منتظم ونڈوز root@linuxmint.com Launchpad Contributions:
  Tajammul Hussain https://launchpad.net/~tajammal2
  Waqar Ahmed https://launchpad.net/~waqar-17a
  abufaizan https://launchpad.net/~abofaizan 