��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  )   �       	   #  
   -  	   8     B     Q     ]     o     |     �  	   �     �     �     �     �     �     �     �     �     �          	          +     1     J  !   f  1   �     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2021-06-25 04:45+0000
Last-Translator: karm <melo@carmu.com>
Language-Team: Interlingua <ia@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 A  proposito Actiones Consentir le percursos Applicationes Area ubi appare le icone de stato de XApp Navigar Cancellar Categorias Categoria Elige un icone Predefinite Icone predefinite Dispositivos Emblemas Emoji Favoritos Icone Dimensiones del Icone Imagine Cargamento... Typos de mime Aperir Operation non supportate Altere Locos Cercar Seliger Eliger le file de imagine Stato Le categoria predefinite Le icone de uso predefinite Le dimension preferite del icone. Le catena de characteres que representa le icone. Si consentir le percursos. Applet de stato XApp Fabrica de applet XApp Status 