��    %      D  5   l      @     A     G     O     [  #   h     �     �  
   �     �     �     �     �     �     �     �  	   �     �  	   �       
     
             "     :     @     G     N     U     g     n     �     �  !   �     �     �       �        �     �     �     �  1   �     $     0  	   7  	   A     K  	   X     b     r     ~     �     �     �     �     �     �  	   �     �     �     �     �     �     �               $     9     S  %   p     �     �     �            	      
                                                 %   #                                                                         $   !                   "              About Actions Allow Paths Applications Area where XApp status icons appear Browse Cancel Categories Category Choose an icon Default Default Icon Devices Emblems Emoji Favorites Icon Icon size Image Loading... Mime types Open Operation not supported Other Places Search Select Select image file Status The default category. The icon to use by default The preferred icon size. The string representing the icon. Whether to allow paths. XApp Status Applet XApp Status Applet Factory Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2020-12-11 14:56+0000
Last-Translator: Paweł Pańczyk <pawelppanczyk@gmail.com>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-02 14:24+0000
X-Generator: Launchpad (build 51cef3d08dff1e227b5ada709bd67280c567d4be)
 O programie Akcje Zezwól ścieżki Programy Obszar, w którym pokazują się ikony stanu XApp Przeglądaj Anuluj Kategorie Kategoria Wybór ikony Domyślny Domyślna ikona Urządzenia Symbole Emoji Ulubione Ikona Rozmiar ikon Obraz Wczytywanie… Typy Mime Otwórz Operacja nieobsługiwana Inne Miejsca Wyszukaj Wybierz Wybierz plik graficzny Status Kategoria domyślna. Domyślnie używana ikona Preferowana wielkość ikon. Ciąg znaków reprezentujący ikonę. Czy zezwalać na ścieżki. Aplet stanu XApp XApp Status Applet Factory 