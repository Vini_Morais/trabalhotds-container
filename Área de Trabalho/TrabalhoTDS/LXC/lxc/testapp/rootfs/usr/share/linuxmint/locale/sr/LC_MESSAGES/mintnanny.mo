��          t      �              %   0     V     f     u  q   �     �             -   1  �  _  ;     G   X     �     �     �  �   �      �     �  !     T   )                       
      	                               %s is not a valid domain name. Block access to selected domain names Blocked domains Domain Blocker Domain name Domain names must start and end with a letter or a digit, and can only contain letters, digits, dots and hyphens. Example: my.number1domain.com Invalid Domain Parental Control Please type the domain name you want to block Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2016-03-30 19:15+0000
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <sr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:46+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 „%s“ није исправан назив домена. Блокирајте приступ изабраним доменима Блокирани домени Блокирање домена Назив домена Називи домена морају почети и завршити се словом или бројем, и могу да садрже само слова, бројеве, тачке и цртице. Пример: moj.broj1domen.com Неисправан домен Родитељски надзор Упишите назив домена који желите да блокирате 