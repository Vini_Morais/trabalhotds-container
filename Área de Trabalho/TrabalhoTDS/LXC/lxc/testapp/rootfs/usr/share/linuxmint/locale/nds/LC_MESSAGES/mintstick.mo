��          �      �        )   	  2   3     f     y      �     �     �     �     �  "   �          ,     ?  )   O  #   y  N   �     �  	   �       
        &     4  �  :  9   �  ?        U     q  %   �     �     �  %   �  #   �  6   "     Y     e     |  +   �  '   �  ]   �     >  	   S     ]  
   s     ~     �     	                                                                                  
                 An error occured while copying the image. An error occured while creating a partition on %s. An error occurred. Authentication Error. Choose a name for your USB Stick Format Format a USB stick Make a bootable USB stick Make bootable USB stick Not enough space on the USB stick. Select Image Select a USB stick Select an image The USB stick was formatted successfully. The image was successfully written. This will destroy all data on the USB stick, are you sure you want to proceed? USB Image Writer USB Stick USB Stick Formatter USB stick: Volume label: Write Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2014-04-29 18:40+0000
Last-Translator: Michael Ernst <michel777@gmx.de>
Language-Team: Low German <nds@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:45+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 Beim den Kopieren des Bildes, ist ein Fehler aufgetreten. Es ist ein Fehler aufgetreten, beim erstellen der Partition %s. Ein Fehler ist aufgetreten. Authentifizierungsfehler. Wähle einen Namen für den USB Stick Formatieren Formatiere einen USB-Stick Erstelle einen bootfähigen USB-Stick Erstellen einen bootbaren USB-Stick Auf den USB Stick ist nicht genügend freier Speicher. Wähle Bild Wähle einen USB Stick Wähle ein Bild Das USB Stick wurde erfolgreich formatiert. Das Bild wurde erfolgreich geschrieben. Dies wird alle Daten auf den USB Stick vernichten, sind sie sicher das sie fortfahren wollen? USB Abbild Schreiber USB Stick USB Stick Formatierer USB Stick: Partition Name: Schreibe 