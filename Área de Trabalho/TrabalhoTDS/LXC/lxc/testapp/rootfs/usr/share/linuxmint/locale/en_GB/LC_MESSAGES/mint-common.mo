��          �            x      y     �     �     �     �  =        J     S  5   [     �  0   �     �  '   �  _        h  �  p           9     Y     y     �  =   �     �     �  5   �     0  0   G     x  '     _   �                                            	                      
                  %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade Project-Id-Version: linuxmint
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
PO-Revision-Date: 2022-08-01 22:21+0000
Last-Translator: Andi Chandler <Unknown>
Language-Team: English (United Kingdom) <en_GB@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2022-12-16 11:47+0000
X-Generator: Launchpad (build 31c78762a8046acf7ab47372e5d588ebb3759d2e)
 %s more disk space will be used. %s of disk space will be freed. %s will be downloaded in total. Additional changes are required An error occurred Cannot remove package %s as it is required by other packages. Flatpaks Install Package %s is a dependency of the following packages: Packages to be removed Please take a look at the list of changes below. Remove The following packages will be removed: This menu item is not associated to any package. Do you want to remove it from the menu anyway? Upgrade 